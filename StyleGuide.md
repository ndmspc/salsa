SALSA Programming Style Guide
=============================

Disclaimer
----------
Maintainers require the right to deny merge until code is up to standard. Please understand that this is solely for the purpose of keeping the code clean, readable and understandable.

Programming style
-----------------
Every commit must be formatted with `clang-format` prior to commiting. All styles are defined in `.clang-format`.

Naming conventions
------------------

### CamelCase convention
All names throughout project use camelCase naming convention.  

### Classes, namespaces, structs
Name of every class, namespace or struct shall begin with capital letter.  
    e.g. `class Socket`, `namespace Salsa`, `struct MachineLoadInfo`, etc.

### Enumerations
Name of base enumeration shall begin with capital latin letter `E`.  
Enum members shall begin with lowercase character.

### Variables
All variables shall begin with lowercase character.  
  
First char of variable shall define scope, as following:
 - `m` - for class data members,
 - `g` - for global variables.
   
Following char should be:
 - `c` for const,
 - `s` for static,
 - `p` for pointer.
   
Direction shall be from left to right, e.g. `int const * const cpcVar = nullptr;`

Const correctness
-----------------
Const qualifier shall be on the right side, as per 'east const convention'.  
E.g.: `int const * const ...`.
