find_path(FMT_INCLUDE_DIR NAMES fmt/format.h)

if(FMT_INCLUDE_DIR)
  set(_FMT_version_file "${FMT_INCLUDE_DIR}/fmt/base.h")
  if(NOT EXISTS "${_FMT_version_file}")
    set(_FMT_version_file "${FMT_INCLUDE_DIR}/fmt/core.h")
  endif()
  if(NOT EXISTS "${_FMT_version_file}")
    set(_FMT_version_file "${FMT_INCLUDE_DIR}/fmt/format.h")
  endif()
  if(EXISTS "${_FMT_version_file}")
    # parse "#define FMT_VERSION 40100" to 4.1.0
    file(STRINGS "${_FMT_version_file}" FMT_VERSION_LINE
      REGEX "^#define[ \t]+FMT_VERSION[ \t]+[0-9]+$")
    string(REGEX REPLACE "^#define[ \t]+FMT_VERSION[ \t]+([0-9]+)$"
      "\\1" FMT_VERSION "${FMT_VERSION_LINE}")
    foreach(ver "FMT_VERSION_PATCH" "FMT_VERSION_MINOR" "FMT_VERSION_MAJOR")
      math(EXPR ${ver} "${FMT_VERSION} % 100")
      math(EXPR FMT_VERSION "(${FMT_VERSION} - ${${ver}}) / 100")
    endforeach()
    set(FMT_VERSION
      "${FMT_VERSION_MAJOR}.${FMT_VERSION_MINOR}.${FMT_VERSION_PATCH}")
  endif()
endif()

find_library(FMT_LIBRARY NAMES fmt)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(FMT
  REQUIRED_VARS FMT_INCLUDE_DIR FMT_LIBRARY
  VERSION_VAR FMT_VERSION)
mark_as_advanced(
  FMT_INCLUDE_DIR
  FMT_LIBRARY
  FMT_VERSION_MAJOR
  FMT_VERSION_MINOR
  FMT_VERSION_PATCH
  FMT_VERSION_STRING)

if(FMT_FOUND AND NOT (TARGET fmt::fmt))
  add_library(fmt::fmt UNKNOWN IMPORTED)
  set_target_properties(fmt::fmt PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${FMT_INCLUDE_DIR}"
    IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
    IMPORTED_LOCATION "${FMT_LIBRARY}")
endif()
