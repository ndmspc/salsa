# Find YAML
#
# Find the YAML includes and library
# 
# if you nee to add a custom library search path, do it via via CMAKE_PREFIX_PATH 
# 
# This module defines
#  YAML_INCLUDE_DIRS, where to find header, etc.
#  YAML_LIBRARIES, the libraries needed to use YAML.
#  YAML_FOUND, If false, do not try to use YAML.
#  YAML_INCLUDE_PREFIX, include prefix for YAML

# only look in default directories
find_path(
	YAML_INCLUDE_DIR 
	NAMES yaml.h
	DOC "YAML include dir"
	HINTS /usr/include/yaml-cpp
)

find_library(
	YAML_LIBRARY
	NAMES yaml-cpp
	DOC "YAML library"
)

set(YAML_INCLUDE_DIRS ${YAML_INCLUDE_DIR})
set(YAML_LIBRARIES ${YAML_LIBRARY})

# handle the QUIETLY and REQUIRED arguments and set YAML_FOUND to TRUE
# if all listed variables are TRUE, hide their existence from configuration view
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(YAML DEFAULT_MSG YAML_LIBRARY YAML_INCLUDE_DIR)
mark_as_advanced (YAML_FOUND YAML_INCLUDE_DIR YAML_LIBRARY)

