#include "salsa.hh"
#include "MainApp.hh"

namespace Salsa {

MainApp::MainApp() : Object()
{
    ///
    /// Constructor
    ///
}

MainApp::~MainApp()
{
    ///
    /// Destructor
    ///
}
int MainApp::run()
{
    ///
    /// Main app run engine
    ///

    // Global actor poller
    zpoller_t * pPoller = zpoller_new(nullptr);

    // Setting up signal handler
    std::signal(SIGINT, Salsa::Actor::signalHandler);
    std::signal(SIGTERM, Salsa::Actor::signalHandler);

    // Execute mActors
    zactor_t * pThreadActor = nullptr;
    for (auto pActor : mActors) {
        if (Salsa::Actor::interrupted()) {
            break;
        }

        SPD_TRACE("Creating actor [{}]", static_cast<void *>(pActor.get()));
        pThreadActor = zactor_new(Salsa::ActorZmq::SalsaActorFn, pActor.get());
        SPD_TRACE("Adding zactor : {} to poller {}", static_cast<void *>(pThreadActor), static_cast<void *>(pPoller));
        zpoller_add(pPoller, pThreadActor);
        mThreadActors.push_back(pThreadActor);
    }

    SPD_INFO("Main App started successfully ...");
    zactor_t * pActorLast = nullptr;
    while (!Salsa::Actor::interrupted()) {
        pThreadActor = static_cast<zactor_t *>(zpoller_wait(pPoller, -1));
        SPD_TRACE("Recieving signal from actor [{}] interupted [{}]", static_cast<void *>(pThreadActor),
                  Salsa::Actor::interrupted());

        // if (pThreadActor == nullptr) Salsa::Actor::interrupted(true);
        pActorLast = nullptr;
        for (zactor_t * pActor : mThreadActors) {
            SPD_TRACE("Trying to delete zactor [{}]  threadActor [{}]", static_cast<void *>(pActor),
                      static_cast<void *>(pThreadActor));
            if (pActor == pThreadActor) {
                SPD_TRACE("Deleting zactor : {}", static_cast<void *>(pActor));
                pActorLast = pActor;
                break;
            }
        }
        if (pActorLast) {
            mThreadActors.erase(std::remove(begin(mThreadActors), end(mThreadActors), pActorLast), end(mThreadActors));
            zpoller_remove(pPoller, pActorLast);
            zactor_destroy(&pActorLast);
        }
        // Salsa::Actor::interrupted(true);
    }

    SPD_TRACE("Salsa::Actor::interrupted() signal [{}]", Salsa::Actor::interrupted());

    SPD_TRACE("Reset Actors...");
    mActors.clear();
    // pRootNode = nullptr;

    // Cleaning thread mActors
    for (zactor_t * pActor : mThreadActors) {
        SPD_TRACE("Deleting zactor : {}", static_cast<void *>(pActor));
        zactor_destroy(&pActor);
    }

    // Clean poller
    zpoller_destroy(&pPoller);

    SPD_INFO("Exiting {} with success ...", salsa_NAME);

    return 0;
}
} // namespace Salsa
