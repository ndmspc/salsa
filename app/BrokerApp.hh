#pragma once
#include "salsa.hh"
#include "ActorZmq.hh"

namespace Salsa {
///
/// \class BrokerApp
///
/// \brief  BrokerApp class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Job;
class BrokerApp : public ActorZmq {
public:
    BrokerApp(std::string inUrl = "@tcp://*:5000", std::string outUrl = "@tcp://*:5001", uint32_t timeoutPublish = 1000,
              uint32_t timeoutClean = 86400000, uint32_t timeoutNoActivity = 60000);
    virtual ~BrokerApp();

    virtual int init();
    virtual int exec();
    virtual int finish();

    /// Sets timeout
    void timeoutPublish(int t) { mTimeoutPublish = t; }
    /// Returns timeout
    int timeoutPublish() const { return mTimeoutPublish; }
    /// Sets timeout (Clean)
    void timeoutClean(int t) { mTimeoutClean = t; }
    /// Returns timeout (Clean)
    int timeoutClean() const { return mTimeoutClean; }
    /// Sets timeout (Heartbeat)
    void timeoutNoActivity(int t) { mTimeoutNoActivity = t; }
    /// Returns timeout (Heartbeat)
    int timeoutNoActivity() const { return mTimeoutNoActivity; }

private:
    zsock_t *                          mpIn{nullptr};             ///< Input socket
    std::string                        mInUrl{};                  ///< Input url
    std::string                        mInSub{""};                ///< Input subscribe string
    zsock_t *                          mpOut{nullptr};            ///< Output socket
    std::string                        mOutUrl{};                 ///< Output url
    uint32_t                           mTimeoutPublish{1000};     ///< Publish timeout
    uint32_t                           mTimeoutClean{86400000};   // Cleanup timeout (24 hours)
    uint32_t                           mTimeoutNoActivity{10000}; // No activity timeout
    std::map<std::string, std::string> mState{};                  ///< State
    std::map<std::string, std::string> mIdGroupState{};           ///< State id and group
    std::map<std::string, int64_t>     mTimes{};                  ///< List of timeputs

    virtual void publish(std::string id);
};

} // namespace Salsa
