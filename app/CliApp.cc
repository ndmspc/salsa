#include <sstream>
#include <fstream>

#include <ndm/Point.pb.h>
#include <ndm/Space.hh>
#include <ndm/Utils.hh>

#include "salsa.hh"
#include "TaskInfo.pb.h"
#include "Job.hh"
#include "MainApp.hh"
#include "MonApp.hh"
#include "CliApp.hh"

namespace Salsa {

CliApp::CliApp(std::string submitterUrl)
    : ActorZmq()
{
    ///
    /// Constructor
    ///

    mCliInfo.mutable_server()->set_url(submitterUrl);
}

CliApp::~CliApp()
{
    ///
    /// Destructor
    ///

    if (mpSubmitter) zsock_destroy(&mpSubmitter);
}

int CliApp::init()
{
    ///
    /// Init cli
    ///

    SPD_TRACE("CliApp::init() <-");

    /// important for signal handling
    ActorZmq::init();

    mpSubmitter = zsock_new_dealer(mCliInfo.server().url().data());
    if (!mpSubmitter) {
        SPD_ERROR("Problem connecting to submitter at [{}]", mCliInfo.server().url());
        return 1;
    }
    SPD_INFO("Connected to submitter at [{}] ...", mCliInfo.server().url());

    if (!auth()) return 2;

    mLogged = true;
    SPD_TRACE("CliApp::init() ->");

    return 0;
}
int CliApp::exec()
{
    ///
    /// Main cli engine
    ///
    SPD_TRACE("CliApp::exec() ->");
    if (Salsa::Actor::interrupted()) return 0;

    zpoller_t * poller = zpoller_new(NULL);
    while (!Salsa::Actor::interrupted()) {
        zpoller_wait(poller, -1);
    }
    zpoller_destroy(&poller);
    SPD_TRACE("CliApp::exec() <-");
    return 0;
}

int CliApp::finish()
{
    ///
    /// Finish cli
    ///

    SPD_TRACE("CliApp::finish() ->");
    SPD_TRACE("CliApp::finish() <-");
    return 0;
}

bool CliApp::auth()
{
    ///
    /// Auth with server
    ///

    zmsg_t * msg = zmsg_new();
    zmsg_addstr(msg, "AUTH");
    zmsg_send(&msg, mpSubmitter);
    zmsg_destroy(&msg);

    msg = zmsg_recv(mpSubmitter);
    if (!msg) return false;

    // Remove empty string
    free(zmsg_popstr(msg));
    /// Remove AUTH cmd (we can check for it later)
    free(zmsg_popstr(msg));

    char * pRC = zmsg_popstr(msg);
    if (strcmp(pRC, "OK")) {
        SPD_ERROR("AUTH failed");
        return false;
    }
    free(pRC);

    char * pRdrID = zmsg_popstr(msg);
    mCliInfo.mutable_server()->set_id(pRdrID);
    free(pRdrID);

    char * pVersion = zmsg_popstr(msg);
    if (pVersion) {
        mCliInfo.mutable_server()->set_version(pVersion);
        SPD_INFO("AUTH OK with server [{}] at url [{}]  ...", mCliInfo.server().version(), mCliInfo.server().url());
    }
    free(pVersion);
    char * pBrokerUrl = zmsg_popstr(msg);

    if (pBrokerUrl) {
        std::string brokerUrl = pBrokerUrl;
        brokerUrl.erase(0, 1);
        mCliInfo.set_broker(brokerUrl);
    }
    free(pBrokerUrl);
    zmsg_destroy(&msg);

    return true;
}

bool CliApp::isLogged() const
{
    ///
    /// Cecker if logged in
    ///

    if (!mLogged) {
        SPD_ERROR("Not logged in!!! Login first ...");
        return false;
    }
    return true;
}

int CliApp::mon(Salsa::Job * job, bool display, std::string broker)
{
    ///
    /// Salsa job monitoring
    ///

    if (broker.empty()) broker = mCliInfo.broker();

    SPD_INFO("");
    SPD_INFO("Reconnect to job via 'salsa-mon -b {} -m {} -j {}'", broker, mCliInfo.server().id(), job->uuid());
    if (!mLastLogDir.empty()) {
        SPD_INFO("");
        SPD_INFO("Logs in '{}'", mLastLogDir);
    }
    SPD_INFO("");
    if (!display) return 0;

    Salsa::MainApp mainApp;
    mainApp.addActor(std::make_shared<Salsa::MonApp>(broker, fmt::format("salsa:{}", mCliInfo.server().id()),
                                                     mCliInfo.server().id(), job->uuid(), true));
    mainApp.run();

    return 0;
}

Salsa::Job * CliApp::newJob(std::string id)
{
    ///
    /// Generate new job
    ///

    if (id.empty()) {
        zuuid_t * uuid = zuuid_new();
        id             = zuuid_str(uuid);
        zuuid_destroy(&uuid);
    }
    Salsa::Job * job = new Salsa::Job(id);
    return job;
}

int CliApp::command(std::string cmd)
{
    ///
    /// Executes cmd on server
    ///

    if (!isLogged()) return 1;

    std::stringstream        ss(cmd);
    std::string              item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, ':')) {
        elems.push_back(std::move(item));
    }

    zmsg_t * msg = zmsg_new();
    zmsg_addstr(msg, elems[0].c_str());
    if (elems.size() > 1) zmsg_addstr(msg, elems[1].c_str());
    zmsg_send(&msg, mpSubmitter);
    zmsg_destroy(&msg);
    SPD_INFO("cmd [{}] was sent ...", cmd);

    msg = zmsg_recv(mpSubmitter);
    if (!msg) return 1;
    zmsg_print(msg);

    return 0;
}

void CliApp::importEnvs(const char * env_names)
{
    ///
    /// Import envs (list of env names are defined in $SALSA_ENV_NAMES)
    ///

    const char * env_names_val = std::getenv(env_names);
    if (!env_names_val) return;

    SPD_INFO("Importing envs '{}' from list defined in '${}' to each task ...", env_names_val, env_names);
    std::stringstream s(env_names_val);
    std::string       currentEnv;
    for (int i = 0; s >> currentEnv; i++) {
        const char * val = std::getenv(currentEnv.c_str());
        if (!val) continue;
        std::string valString = val;
        SPD_TRACE("env={} val={} ...", currentEnv, valString);
        mEnvs.insert(make_pair(currentEnv, valString));
    }
    SPD_TRACE("size={}", mEnvs.size());
}

Salsa::Job * CliApp::generateJobFromTemplate(std::string cmd, int uselogs)
{
    ///
    /// Generate job from template string
    ///

    if (!isLogged()) return nullptr;

    std::stringstream        ss(cmd);
    std::string              item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, ':')) {
        elems.push_back(std::move(item));
    }

    Salsa::Job * job = newJob();

    uint32_t                                     n = atoi(elems[1].c_str());
    std::string                                  c;
    Salsa::TaskInfo *                            ji;
    std::map<std::string, std::string>::iterator it;
    for (uint32_t i = 0; i < n; ++i) {
        c  = elems[0];
        ji = new Salsa::TaskInfo();
        ji->set_clientid(getuid());
        ji->set_groupid(getgid());
        ji->set_jobid(job->uuid());
        ji->set_taskid(i);
        ji->set_data(c);
        ji->clear_logtargets();
        if (uselogs != 0) ji->add_logtargets("default");

        for (it = mEnvs.begin(); it != mEnvs.end(); it++) {
            SPD_DEBUG("Importing env={} val={} ...", it->first, it->second);
            ji->add_envs(fmt::format("{}={}", it->first, it->second));
        }

        ji->add_envs(fmt::format("SALSA_CLUSTER_ID={}", mCliInfo.server().id()));
        ji->add_envs(fmt::format("SALSA_JOB_ID={}", job->uuid()));
        ji->add_envs(fmt::format("SALSA_TASK_ID={}", i));

        //
        job->addTask(i, ji, Salsa::Job::pending);
    }

    return job;
}
Salsa::Job * CliApp::generateJobFromFile(std::string filename, int uselogs)
{
    ///
    /// Generate job from file
    ///

    if (!isLogged()) return nullptr;

    Salsa::Job * job = newJob();

    std::ifstream                                ifs(filename);
    std::string                                  c;
    Salsa::TaskInfo *                            ji;
    std::map<std::string, std::string>::iterator it;
    uint32_t                                     i = 0;
    do {
        std::getline(ifs, c);
        if (c.empty()) {
            continue;
        }
        ji = new Salsa::TaskInfo();
        ji->set_clientid(getuid());
        ji->set_groupid(getgid());
        ji->set_jobid(job->uuid());
        ji->set_taskid(i);
        ji->set_data(c);
        ji->clear_logtargets();
        if (uselogs != 0) ji->add_logtargets("default");

        for (it = mEnvs.begin(); it != mEnvs.end(); it++) {
            SPD_DEBUG("Importing env={} val={} ...", it->first, it->second);
            ji->add_envs(fmt::format("{}={}", it->first, it->second));
        }

        ji->add_envs(fmt::format("SALSA_CLUSTER_ID={}", mCliInfo.server().id()));
        ji->add_envs(fmt::format("SALSA_JOB_ID={}", job->uuid()));
        ji->add_envs(fmt::format("SALSA_TASK_ID={}", i));

        job->addTask(i, ji, Salsa::Job::pending);
        i++;

    } while (!c.empty());

    return job;
}

Salsa::Job * CliApp::generateJobFromNdm(std::vector<NDM::Point> & points, NDM::Config & cfg, int uselogs)
{
    ///
    /// Generate job from NDM points
    ///

    Salsa::Job * job = newJob();

    std::string base = cfg.base();
    if (base.empty()) {
        SPD_CRIT("Please set full path to base directory in configuration file !!!");
        return nullptr;
    }
    std::string cmd = cfg.cmd();
    if (cmd.empty()) {
        SPD_CRIT("Please set 'cmd' in configuration file !!!");
        return nullptr;
    }
    std::string  pointsList = base;
    NDM::Space * s          = cfg.space();
    if (s == nullptr) {
        SPD_CRIT("Problem loading Space object!!!");
        return nullptr;
    }
    if (s->axes().size() <= 0) {
        SPD_ERROR("No axis was found ...", base);
        return nullptr;
    }
    s->points(cfg.levels(), points);

    if (!s) {
        SPD_ERROR("Problem creating ndm space !!!");
        return nullptr;
    }

    if (points.size() == 0) {
        SPD_ERROR("No ndm points found !!!");
        return nullptr;
    }
    SPD_INFO("points={}", points.size());

    int                                          taskId = 0;
    std::string                                  serialize, strasci;
    std::map<std::string, std::string>::iterator it;
    for (auto p : points) {
        //     p.PrintDebugString();
        Salsa::TaskInfo * ji = new Salsa::TaskInfo();
        ji->set_clientid(getuid());
        ji->set_groupid(getgid());
        ji->set_jobid(job->uuid());
        ji->set_taskid(taskId);
        ji->set_data(cmd);
        ji->clear_logtargets();
        if (uselogs) ji->add_logtargets("default");

        for (it = mEnvs.begin(); it != mEnvs.end(); it++) {
            SPD_DEBUG("Importing env={} val={} ...", it->first, it->second);
            ji->add_envs(fmt::format("{}={}", it->first, it->second));
        }

        p.SerializeToString(&serialize);
        strasci.clear();
        NDM::Utils::protobuf_to_asci(serialize, strasci);

        for (auto e : cfg.envs()) {
            ji->add_envs(e);
        }
        ji->add_envs(fmt::format("SALSA_CLUSTER_ID={}", mCliInfo.server().id()));
        ji->add_envs(fmt::format("SALSA_JOB_ID={}", job->uuid()));
        ji->add_envs(fmt::format("SALSA_TASK_ID={}", taskId));
        ji->add_envs(fmt::format("NDMBASE={}", base));
        ji->add_envs(fmt::format("NDMPATH={}", p.path()));
        ji->add_envs(fmt::format("NDMPOINT={}", strasci));

        job->addTask(taskId, ji, Salsa::Job::pending);

        taskId++;
    }

    return job;
}

int CliApp::sendJob(Salsa::Job * job)
{
    ///
    /// Send all pending tasks in Job
    ///

    if (!isLogged()) return 1;
    if (!job) return 1;
    if (job->sizeNotFinished() == 0) return 1;

    SPD_INFO("Sending jobs ...");
    job->print();

    zmsg_t * pMsg = zmsg_new();
    zmsg_addstr(pMsg, "TASK");

    uint32_t    id = 0;
    std::string payload;
    while (!zsys_interrupted) {
        auto ji = job->nextTask();
        if (!ji) break;
        SPD_DEBUG("[{}:{}] [{}]", ji->jobid(), ji->taskid(), ji->data());
        ji->SerializeToString(&payload);

        zmsg_addstr(pMsg, payload.c_str());
        id++;
    }
    zmsg_send(&pMsg, mpSubmitter);
    zmsg_destroy(&pMsg);

    recv(job);

    SPD_INFO("Total number of jobs [{}] sent", id);
    return 0;
}

int CliApp::recv(Salsa::Job * job)
{
    ///
    /// Handle recieve
    ///

    uint32_t taskid;
    int      rc;
    zmsg_t * msg = zmsg_recv(mpSubmitter);
    if (!msg) return -1;
    // zmsg_print(msg);

    uint32_t count = 0;

    free(zmsg_popstr(msg));
    char * pCmd = zmsg_popstr(msg);
    if (!pCmd) {
        SPD_WARN("Command is missing in message !!! Skiping");
        return -1;
    }
    if (!strcmp(pCmd, "TASK_RESULT")) {

        char * pJobID  = zmsg_popstr(msg);
        char * pTaskID = zmsg_popstr(msg);
        taskid         = atol(pTaskID);
        char * pRC     = zmsg_popstr(msg);
        rc             = atoi(pRC);

        SPD_INFO("jobid [{}] taskid [{}] rc [{}] ", pJobID, pTaskID, rc);
        if (rc == 0) {
            job->moveTask(taskid, Salsa::Job::assigned, Salsa::Job::done);
        }
        else {
            job->moveTask(taskid, Salsa::Job::assigned, Salsa::Job::failed);
        }

        free(pJobID);
        free(pTaskID);

        free(pRC);
    }
    else if (!strcmp(pCmd, "TASK_ADDED")) {

        char * pCount = zmsg_popstr(msg);
        count -= atoi(pCount);
        char * pLogDir = zmsg_popstr(msg);
        mLastLogDir    = pLogDir;
        free(pCount);
        free(pLogDir);
    }
    else {
        SPD_WARN("Unknown command [{}]!!! Skiping", pCmd);
        return -1;
    }
    free(pCmd);

    zmsg_destroy(&msg);

    return 0;
}

} // namespace Salsa
