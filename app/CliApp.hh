#pragma once
#include <ndm/Point.pb.h>
#include <ndm/Config.hh>

#include "salsa.hh"
#include "CliInfo.pb.h"

#include "ActorZmq.hh"

namespace Salsa {
///
/// \class CliApp
///
/// \brief  CliApp class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Job;
class CliApp : public ActorZmq {
public:
    CliApp(std::string submitterUrl = "");
    virtual ~CliApp();

    virtual int init();
    virtual int exec();
    virtual int finish();

    virtual bool         auth();
    bool                 isLogged() const;
    virtual Salsa::Job * newJob(std::string id = "");
    virtual void         importEnvs(const char * env_names = "SALSA_ENV_NAMES");
    virtual Salsa::Job * generateJobFromFile(std::string file, int uselogs = 0);
    virtual Salsa::Job * generateJobFromTemplate(std::string cmd, int uselogs = 0);
    virtual Salsa::Job * generateJobFromNdm(std::vector<NDM::Point> & points, NDM::Config & cfg, int uselogs = 0);

    virtual int sendJob(Salsa::Job * job);
    virtual int command(std::string cmd);
    virtual int recv(Salsa::Job * job);
    virtual int mon(Salsa::Job * job, bool display = true, std::string broker = "");

private:
    Salsa::CliInfo                     mCliInfo{};           ///< Cli info
    zsock_t *                          mpSubmitter{nullptr}; ///< Submitter socket
    bool                               mLogged{false};       ///< Flag if we are logged in
    std::string                        mLastLogDir{};        ///< Log dir from last job sent
    std::map<std::string, std::string> mEnvs;                ///< Common envs
};

} // namespace Salsa
