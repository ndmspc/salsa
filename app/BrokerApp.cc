
#include "BrokerApp.hh"

namespace Salsa {

BrokerApp::BrokerApp(std::string inUrl, std::string outUrl, uint32_t timeoutPublish, uint32_t timeoutClean,
                     uint32_t timeoutNoActivity)
    : ActorZmq()
    , mInUrl(inUrl)
    , mOutUrl(outUrl)
    , mTimeoutPublish(timeoutPublish)
    , mTimeoutClean(timeoutClean)
    , mTimeoutNoActivity(timeoutNoActivity)
{
    ///
    /// Constructor
    ///
}

BrokerApp::~BrokerApp()
{
    ///
    /// Destructor
    ///

    SPD_TRACE("### Destroy BrokerApp ###");
    if (mpIn) zsock_destroy(&mpIn);
    if (mpOut) zsock_destroy(&mpOut);
}

int BrokerApp::init()
{
    ///
    /// Init broker
    ///

    SPD_TRACE("BrokerApp::init() <-");

    /// important for signal handling
    ActorZmq::init();

    SPD_DEBUG("in={} out={} publish={} timeout={}", mInUrl, mOutUrl, mTimeoutPublish, mTimeout);

    // Creating poller
    SPD_INFO("Broker is starting : in={} (sub[{}]) out={}", mInUrl, mInSub, mOutUrl);

    // Subscriber
    mpIn = zsock_new_sub(mInUrl.data(), mInSub.data());
    if (!mpIn) {
        mTerminated = true;
        SPD_ERROR("BrokerApp::init() : Problem starting socket at url [{}] ", mInUrl);
        return 1;
    }

    // Publisher
    mpOut = zsock_new_pub(mOutUrl.data());
    if (!mpOut) {
        mTerminated = true;
        SPD_ERROR("BrokerApp::init() : Problem starting socket at url [{}] ", mOutUrl);
        zsock_destroy(&mpIn);
        return 1;
    }
    // // Adding subscriber to poller
    // zpoller_add(mpPoller->poller(), mpIn);
    // zpoller_add(mpPoller->poller(), mpOut);

    SPD_TRACE("BrokerApp::init() ->");
    return 0;
}
int BrokerApp::exec()
{
    ///
    /// Main broker engine
    ///

    SPD_TRACE("BrokerApp::exec() <-");

    // if (Salsa::Actor::interrupted()) return 0;

    zpoller_t * poller = zpoller_new(NULL);

    // Adding subscriber to poller
    zpoller_add(poller, mpIn);
    zpoller_add(poller, mpOut);

    void *   pPointer;
    int64_t  last_time = zclock_time();
    int64_t  cur_time;
    zmsg_t * pMsg;

    while (!mTerminated && !Salsa::Actor::interrupted()) {

        SPD_TRACE("BrokerApp::poller::wait()");
        pPointer = zpoller_wait(poller, 100);

        if (zpoller_terminated(poller)) {
            mTerminated = true;
            continue;
        }

        if (zpoller_expired(poller)) {
            cur_time = zclock_time();
            if (cur_time - last_time > mTimeoutNoActivity) {
                last_time = zclock_time();
                if (mTimes.size() > 0) {
                    SPD_INFO("No activity more then {} msec. Publishing ...", mTimeoutNoActivity);
                    for (auto const & x : mTimes) {
                        publish(x.first);
                        std::map<std::string, int64_t>::iterator it = mTimes.find(x.first);
                        if (it != mTimes.end()) it->second = last_time;
                    }
                }
            }
            continue;
        }

        if (!pPointer) {
            SPD_TRACE("Poller released socket={} !!! Exiting ...", static_cast<void *>(pPointer));
            mTerminated = true;
            continue;
        }
        SPD_TRACE("Poller released socket={} ...", static_cast<void *>(pPointer));

        if (pPointer == mpPipe) {
            SPD_TRACE("Signal from pipe={}", static_cast<void *>(mpPipe));
            // We are not reacting to pipe events for now
            continue;
        }

        // last_time_heartbeat = zclock_time();
        bool forcePublish = false;

        if (pPointer != nullptr) {
            pMsg = zmsg_recv(pPointer);
            if (!pMsg) continue;
            // zmsg_print(pMsg);
            // free(zmsg_popstr(pMsg));
            char *      id         = zmsg_popstr(pMsg);
            std::string id_str     = id;
            id_str                 = id_str.substr(6);
            char * group           = zmsg_popstr(pMsg);
            char * data            = zmsg_popstr(pMsg);
            char * forcePublishStr = zmsg_popstr(pMsg);
            if (std::strcmp(forcePublishStr, "true") == 0) forcePublish = true;
            SPD_TRACE("forcePublish[{}] forcePublishStr=[{}]", forcePublish, forcePublishStr);

            auto it_state    = mState.find(id_str);
            auto it_ig_state = mIdGroupState.find(id_str);
            auto it_times    = mTimes.find(id_str);
            if (it_state != mState.end()) {
                it_state->second = data;
                it_times->second = zclock_time();
                SPD_TRACE("Updating id[{}] with data [{}]", it_state->first, it_state->second);
            }
            else {

                // clean if we have group already
                std::vector<std::string> clean;
                for (auto ig : mIdGroupState) {
                    // SPD_DEBUG("CHecking id[{}] group[{}] ", ig.first, ig.second);
                    if (ig.second == group) {
                        // SPD_DEBUG("We have same group id[{}] group[{}] ", ig.first, ig.second);
                        clean.push_back(ig.first);
                    }
                }
                for (auto c : clean) {
                    SPD_WARN("Cleaning duplicate for group[{}] id[{}] ...", group, c);
                    mState.erase(c);
                    mIdGroupState.erase(c);
                    mTimes.erase(c);
                }
                // end cleaning

                mState.insert(it_state, std::pair<std::string, std::string>(id_str, data));
                mIdGroupState.insert(it_ig_state, std::pair<std::string, std::string>(id_str, group));
                mTimes.insert(it_times, std::pair<std::string, int64_t>(id_str, zclock_time()));
                SPD_TRACE("Inserting id[{}] with data [{}]", id_str, data);
                SPD_TRACE("mState size={}", mState.size());
            }
            free(id);
            free(group);
            free(data);
            free(forcePublishStr);
            zmsg_destroy(&pMsg);
        }
        cur_time = zclock_time();
        if ((cur_time - last_time) > mTimeoutPublish || forcePublish) {
            last_time = zclock_time();
            if (mTimes.size() > 0) {
                std::vector<std::string> ids_clean;
                for (auto const & x : mTimes) {
                    if (cur_time - x.second < mTimeoutPublish * 1.5 || forcePublish) {
                        publish(x.first);
                        std::map<std::string, int64_t>::iterator it = mTimes.find(x.first);
                        if (it != mTimes.end()) it->second = last_time;
                    }
                    else {
                        // removing
                        if (cur_time - x.second > mTimeoutClean) {
                            SPD_TRACE("Adding to remove id {}", x.first);
                            ids_clean.push_back(x.first);
                        }
                    }
                }
                if (ids_clean.size() > 0) {
                    /// clearing published ids
                    for (auto id : ids_clean) {
                        SPD_TRACE("Removing id {}", id);

                        mState.erase(id);
                        mIdGroupState.erase(id);
                        mTimes.erase(id);
                    }
                }
                // if (isPublished) last_time = zclock_time();
            }

            // forcePublish = false;
        }
    }

    zpoller_destroy(&poller);

    // Salsa::Actor::interrupted(true);

    SPD_TRACE("BrokerApp::exec() ->");
    return 0;
} // namespace Salsa

int BrokerApp::finish()
{
    ///
    /// Finish broker
    ///
    SPD_TRACE("BrokerApp::finish() <-");
    zsock_destroy(&mpIn);
    mpIn = nullptr;
    zsock_destroy(&mpOut);
    mpOut = nullptr;
    SPD_TRACE("BrokerApp::finish() ->");

    return 0;
}

void BrokerApp::publish(std::string id)
{

    auto it_state    = mState.find(id);
    auto it_ig_state = mIdGroupState.find(id);
    if (it_state != mState.end()) {
        std::string json = it_state->second;
        zmsg_t *    msg  = zmsg_new();
        zmsg_addstr(msg, fmt::format("salsa:{}", it_state->first).data());
        zmsg_addstr(msg, it_ig_state->second.data());
        zmsg_addstrf(msg, "%s", json.data());
        SPD_DEBUG("[{}] Publishing sub[{}] id[{}] JSON={}", id, fmt::format("salsa:{}", it_state->first),
                  it_ig_state->second, json);
        zmsg_send(&msg, mpOut);
        zmsg_destroy(&msg);
    }
}

} // namespace Salsa
