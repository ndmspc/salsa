
#include "TestApp.hh"

namespace Salsa {

TestApp::TestApp() : ActorZmq()
{
    ///
    /// Constructor
    ///
}
TestApp::~TestApp()
{
    ///
    /// Destructor
    ///
}

int TestApp::init()
{
    ///
    /// Init
    ///

    SPD_TRACE("TestApp::init() <-");

    /// important for signal handling
    ActorZmq::init();

    SPD_TRACE("TestApp::init() ->");
    return 0;
}

int TestApp::exec()
{
    ///
    /// Exec
    ///
    SPD_TRACE("TestApp::exec() <-");

    void * pPointer = nullptr;

    while (!mTerminated && !Salsa::Actor::interrupted()) {
        SPD_TRACE("Actor::wait()");
        pPointer = ActorZmq::wait();
        if (!pPointer) {
            break;
        }

        if (pPointer == mpPipe) {
            SPD_TRACE("Signal from pipe={}", static_cast<void *>(mpPipe));
            // We are not reacting to pipe events for now
            continue;
        }
    }
    SPD_TRACE("TestApp::exec() ->");

    return 0;
}
int TestApp::finish()
{
    ///
    /// Finish
    ///
    SPD_TRACE("TestApp::finish() ->");
    SPD_TRACE("TestApp::finish() <-");

    return 0;
}

} // namespace Salsa
