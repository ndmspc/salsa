#pragma once

#include "Object.hh"
#include "ActorZmq.hh"

namespace Salsa {
///
/// \class MainApp
///
/// \brief  Main app
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class MainApp : public Object {
public:
    MainApp();
    virtual ~MainApp();
    int run();

    void addActor(std::shared_ptr<Salsa::ActorZmq> a) { mActors.push_back(a); }

    /// Returns list of actors
    std::vector<std::shared_ptr<Salsa::ActorZmq>> * actors() { return &mActors; }

private:
    std::vector<std::shared_ptr<Salsa::ActorZmq>> mActors{};       ///< List of actors
    std::vector<zactor_t *>                       mThreadActors{}; ///< Container for actors
};

} // namespace Salsa
