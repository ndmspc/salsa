#pragma once

#include "ActorZmq.hh"

namespace Salsa {
///
/// \class TestApp
///
/// \brief  TestApp class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class TestApp : public ActorZmq {
public:
    TestApp();
    virtual ~TestApp();

    virtual int init();
    virtual int exec();
    virtual int finish();
};

} // namespace Salsa
