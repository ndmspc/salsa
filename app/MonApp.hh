#pragma once

#include "ActorZmq.hh"

namespace Salsa {
///
/// \class MonApp
///
/// \brief  MonApp class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class MonApp : public ActorZmq {
public:
    MonApp(bool is_tui = true);
    MonApp(std::string url, std::string sub, std::string mgrid, std::string jobid, bool is_tui = true);
    virtual ~MonApp();

    virtual int init();
    virtual int exec();
    virtual int finish();

private:
    std::string mUrl{"tcp ://localhost:5001};"}; ///< Url
    std::string mSub{};                          ///< Subscribe string
    std::string mJobMgr{};                       ///< Job Manager ID
    std::string mJobID{};                        ///< Job ID
    bool        mIsTui{true};                    ///< Flag if to use TUI
};

} // namespace Salsa
