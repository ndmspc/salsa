
#include "MonApp.hh"
#include "JobWindow.hh"

namespace Salsa {

MonApp::MonApp(bool is_tui) : ActorZmq(), mIsTui(is_tui)
{
    ///
    /// Constructor
    ///
}

MonApp::MonApp(std::string url, std::string sub, std::string mgrid, std::string jobid, bool is_tui)
    : ActorZmq(), mUrl(url), mSub(sub), mJobMgr(mgrid), mJobID(jobid), mIsTui(is_tui)
{
    ///
    /// Constructor
    ///
}
MonApp::~MonApp()
{
    ///
    /// Destructor
    ///
}

int MonApp::init()
{
    ///
    /// Init
    ///

    SPD_TRACE("MonApp::init() <-");

    /// important for signal handling
    ActorZmq::init();

    SPD_TRACE("MonApp::init() ->");
    return 0;
}

int MonApp::exec()
{
    // if (Salsa::Actor::interrupted()) return 0;

    if (Salsa::Object::getConsoleOutput()->level() < spdlog::level::info) {
        SPD_WARN("Debug level is not 'info'!!! Skipping tui ...");
        Salsa::Actor::interrupted(true);
        return 0;
    }

    SPD_INFO("Listening at '{}' with subscription '{}'", mUrl.data(), mSub.data());

    // MAIN
    Salsa::JobWindow * jw = nullptr;
    if (Salsa::TuiUtils::tui_init(false)) {
        mvprintw(LINES - 2, 1, "(F10 to Exit)");
        jw = new Salsa::JobWindow();
        if (!mJobID.empty()) jw->cur_job_id(mJobID);
        if (!mJobMgr.empty()) jw->cur_job_manager(mJobMgr);
    }
    else {
        SPD_ERROR("Problem initializing tui interface !!! Skiping ...");
    }
    // Creating socket
    zsock_t *   pSub   = zsock_new_sub(mUrl.data(), mSub.data());
    zpoller_t * poller = zpoller_new(pSub, NULL);
    zmsg_t *    pMsg;
    int         timeoutPoller = 100;
    void *      socket;
    while (!Salsa::Actor::interrupted()) {
        SPD_TRACE("Poller wait");
        socket = zpoller_wait(poller, timeoutPoller);
        if (zpoller_terminated(poller)) break;
        if (socket) {
            pMsg = zmsg_recv(socket);
            if (!pMsg) continue;
            // zmsg_print(pMsg);
            char *      sub     = zmsg_popstr(pMsg);
            std::string sub_str = sub;
            sub_str             = sub_str.substr(6);
            char * id           = zmsg_popstr(pMsg);
            char * data         = zmsg_popstr(pMsg);
            SPD_DEBUG("sub [{}] id [{}] data [{}]", sub, id, data);

            if (jw) {
                // Sets first manager it it was not provided by user
                if (jw->cur_job_manager().empty()) jw->cur_job_manager(id);

                // Update window when job data changes
                if (jw->update(sub_str, data)) jw->update("");
                // else
                //     jw->update("Job was not found");
            }
            else {
                SPD_INFO("sub [{}] id [{}] data [{}]", sub, id, data);
            }
            zmsg_destroy(&pMsg);
            free(sub);
            free(id);
            free(data);
        }
        if (jw) {
            if (getch() == KEY_F(10)) {
                Salsa::Actor::interrupted(true);
                break;
            }
        }
    }

    // Clean socket
    zsock_destroy(&pSub);

    zpoller_destroy(&poller);

    if (jw) {

        // nodelay(stdscr, 0);
        // int ch;
        // while ((ch = getch()) != KEY_F(10)) {
        // }

        delete jw;
        // Destroy tui
        Salsa::TuiUtils::tui_destroy();
    }

    return 0;
}

int MonApp::finish()
{
    ///
    /// Finish
    ///
    SPD_TRACE("MonApp::finish() ->");
    SPD_TRACE("MonApp::finish() <-");

    return 0;
}

} // namespace Salsa
