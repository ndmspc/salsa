#include <fstream>
#include <getopt.h>
#include <memory>
#include <sstream>
#include <zyre.h>
#include "salsa.hh"
#include "TaskInfo.pb.h"
#include "Job.hh"
#include "MainApp.hh"
#include "MonApp.hh"
#include "CliApp.hh"

using namespace fmt::literals;

// TODO
#define SALSA_SUBPROG "tui"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{}-{} v{}.{}.{}-{}", salsa_NAME, SALSA_SUBPROG, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{

    spdlog::set_pattern("%v");
    SPD_INFO("Tool to feed jobs to {}.", salsa_NAME);
    SPD_INFO("");
    SPD_INFO("Usage: {}-{} [OPTION]...", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("       -s, --submit[=VALUE]     url for salsa submitter");
    SPD_INFO("       -f, --file[=VALUE]       input file");
    SPD_INFO("       -c, --command[=VALUE]    command to salsa");
    SPD_INFO("       -t, --template[=VALUE]   template for multiple commands");
    SPD_INFO("       -d, --discovery[=VALUE]  discovery");
    SPD_INFO("       -e, --endpoint[=VALUE]   endpoint for discovery");
    SPD_INFO("       -b, --broker[=VALUE]     broker url");
    SPD_INFO("           --batch              job is submited in batch mode (no tui)");
    SPD_INFO("           --rc                 flag if results of taks handled (experimental)");
    SPD_INFO("           --logs               flag if logs are produced");
    SPD_INFO("           --silent             no logs printed");
    SPD_INFO("           --debug              debug logs printed");
    SPD_INFO("           --trace              trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help               display this help and exit");
    SPD_INFO("       -v, --version            output version information and exit");
    SPD_INFO("");
    SPD_INFO("Examples:");
    SPD_INFO("       salsa-feeder -s tcp://localhost:41000 -f input.txt");
    SPD_INFO("                                sends command for each line in input.txt to localhost:41000");
    SPD_INFO("");
    SPD_INFO("       salsa-feeder -s tcp://localhost:41000 -t \"sleep 1:100\"");
    SPD_INFO("                                generate 100 jobs each 'sleep 1' to localhost:41000");
    SPD_INFO("");
    SPD_INFO("       salsa-feeder -s tcp://localhost:41000 -c \"JOB_DEL_ALL\"");
    SPD_INFO("                                remove all jobs from job manager at localhost:41000");
    SPD_INFO("");
    SPD_INFO("       salsa-feeder -s tcp://localhost:41000 -c \"JOB_DEL_FINISHED\"");
    SPD_INFO("                                remove all finished jobs from job manager at localhost:41000");
    SPD_INFO("");
    SPD_INFO("       salsa-feeder -s tcp://localhost:41000 -c \"WORKER_COUNT\"");
    SPD_INFO("                                returns number of workers at localhost:41000");
    SPD_INFO("");
    SPD_INFO("       salsa-feeder -s tcp://localhost:41000 -c \"JOB_DEL_ID:E25633E7362F43FF8DFE32E3C525E223\"");
    SPD_INFO("                                remove job with id 'E25633E7362F43FF8DFE32E3C525E223' from job manager "
             "at localhost:41000");
    SPD_INFO("");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{

    // ***** Default values BEGIN *****
    // Log level : 'info'
    int debugLevel = static_cast<int>(spdlog::level::info);
    // submitter connect url
    std::string submitterUrl = "";
    // Command
    std::string inputCommand = "";
    // input filename
    std::string inputFilename = "";
    // input template
    std::string inputTemplate = "";
    // endpoint for monitoring
    std::string brokerUrl = "";
    // flag if running in batch mode
    int isBatch = 0;
    // flag if results tasks are handled
    int isResultTask = 0;
    // flag is logs are produced
    int produceLogs = 0;
    // ***** Default values END *****

    std::string   short_options  = "hs:f:t:c:b:W;";
    struct option long_options[] = {{"help", no_argument, nullptr, 'h'},
                                    {"version", no_argument, nullptr, 'v'},
                                    {"submit", required_argument, nullptr, 's'},
                                    {"command", required_argument, nullptr, 'c'},
                                    {"file", required_argument, nullptr, 'f'},
                                    {"template", required_argument, nullptr, 't'},
                                    {"broker", required_argument, nullptr, 'b'},
                                    {"batch", no_argument, &isBatch, static_cast<int>(1)},
                                    {"rc", no_argument, &isResultTask, static_cast<int>(1)},
                                    {"logs", no_argument, &produceLogs, static_cast<int>(1)},
                                    {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                    {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                    {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                    {nullptr, 0, nullptr, 0}};

    int nextOption = 0;

    do {
        nextOption = getopt_long(argc, argv, short_options.c_str(), long_options, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 's': submitterUrl = fmt::format(">{}", optarg); break;
        case 'f':
            inputFilename = optarg;
            inputTemplate = "";
            break;
        case 't':
            inputTemplate = optarg;
            inputFilename = "";
            break;

        case 'c': inputCommand = optarg; break;
        case 'b': brokerUrl = fmt::format(">{}", optarg); break;
        case 'q': isBatch = true; break;
        default: help();
        }
    } while (nextOption != -1);

    ::Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    SPD_INFO("Starting {}-feeder v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    if (submitterUrl.empty() && inputFilename.empty() && inputTemplate.empty() && inputCommand.empty()) help();

    Salsa::Job *  job = nullptr;
    Salsa::CliApp cliApp(submitterUrl);
    cliApp.init();
    cliApp.importEnvs();
    if (!inputCommand.empty()) cliApp.command(inputCommand);
    if (!inputTemplate.empty()) {
        job = cliApp.generateJobFromTemplate(inputTemplate, produceLogs);
    }
    else if (!inputFilename.empty()) {
        job = cliApp.generateJobFromFile(inputFilename, produceLogs);
    }

    if (job) {
        cliApp.sendJob(job);
        if (!isBatch) {
            if (isResultTask) {
                while (!zsys_interrupted) {
                    if (cliApp.recv(job) == -1) continue;
                    if (job->isFinished()) {
                        SPD_INFO("Job [{}] has finished", job->uuid());
                        job->print();
                        break;
                    }
                }
            }
            cliApp.mon(job, true, brokerUrl);
        }
        else {
            cliApp.mon(job, false, brokerUrl);
            zclock_sleep(100);
        }
    }
    // cliApp.exec();
    cliApp.finish();

    delete job;

    // Release and close all loggers
    spdlog::drop_all();

    return 0;
}
