#include <map>
#include <czmq.h>
#include <getopt.h>
#include "salsa.hh"
#include "MainApp.hh"
#include "BrokerApp.hh"

using namespace fmt::literals;

// TODO
#define SALSA_SUBPROG "broker"

[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{}-{} v{}.{}.{}-{}", salsa_NAME, SALSA_SUBPROG, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{

    spdlog::set_pattern("%v");
    SPD_INFO("Broker for {}.", salsa_NAME);
    SPD_INFO("");
    SPD_INFO("Usage: {}-{} [OPTION]...", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("       -i, --in[=VALUE]                input url (default: tcp://*:5000)");
    SPD_INFO("       -o, --out[=VALUE]               output url (default: tcp://*:5001)");
    SPD_INFO("       -s, --subscribe[=VALUE]         input subscribe string (default: \"\")");
    SPD_INFO("       -t, --timeout[=VALUE]           poller timeout (default: 100)");
    SPD_INFO("       -p, --timeout-publish[=VALUE]   publish timeout (default: 1000)");
    SPD_INFO("       -t, --timeout[=VALUE]           poller timeout (default: 3)");
    SPD_INFO("       -c, --timeout-clean[=VALUE]     redirector clean timeout in seconds (default: 24h (24*3600 s))");
    SPD_INFO("       -b, --timeout-heartbeat[=VALUE] publish info when no activity every heartbeat (default: 60 s )");
    SPD_INFO("           --silent                    no logs printed");
    SPD_INFO("           --debug                     debug logs printed");
    SPD_INFO("           --trace                     trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help                      display this help and exit");
    SPD_INFO("       -v, --version                   output version information and exit");
    SPD_INFO("");
    SPD_INFO("Examples:");
    SPD_INFO("       salsa-broker -i tcp://*:5555 -p 2000");
    SPD_INFO("                                input url is @tcp://*:5555 and publish is done every 2 sec");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

void publish(void * pub, std::map<std::string, std::string> & state, std::string id)
{
    auto it_state = state.find(id);
    if (it_state != state.end()) {
        std::string json = it_state->second;
        zmsg_t *    msg  = zmsg_new();
        zmsg_addstr(msg, fmt::format("salsa:{}", it_state->first).data());
        zmsg_addstr(msg, it_state->first.data());
        zmsg_addstrf(msg, "%s", json.data());
        SPD_DEBUG("Publishing sub[{}] id[{}] JSON={}", fmt::format("salsa:{}", it_state->first), it_state->first, json);
        zmsg_send(&msg, pub);
        zmsg_destroy(&msg);
    }
}

int main(int argc, char ** argv)
{
    // ***** Default values BEGIN *****
    std::string in               = "@tcp://*:5000";
    std::string out              = "@tcp://*:5001";
    int         timeout_publish  = 1000;
    int         timeout_poller   = 100;
    int         timeout_clean    = 24 * 3600 * 1000; // 24 hours
    int         timeout_hartbeat = 60 * 1000;        // 1 min
    std::string subscribe        = "";

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("SALSA_DEBUG_LEVEL")) debugLevel = atoi(getenv("SALSA_DEBUG_LEVEL"));
    // ***** Default values END *****

    std::string   shortOpts  = "hvs:i:o:t:p:b:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"subscribe", required_argument, nullptr, 's'},
                                {"in", required_argument, nullptr, 'i'},
                                {"out", required_argument, nullptr, 'o'},
                                {"timeout", required_argument, nullptr, 't'},
                                {"timeout-publish", required_argument, nullptr, 'p'},
                                {"timeout-clean", required_argument, nullptr, 'c'},
                                {"timeout-heartbeat", required_argument, nullptr, 'b'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'i': in = fmt::format("@{}", optarg); break;
        case 'o': out = fmt::format("@{}", optarg); break;
        case 's': subscribe = optarg; break;
        case 't': timeout_poller = atoi(optarg); break;
        case 'p': timeout_publish = atoi(optarg); break;
        case 'c': timeout_clean = atoi(optarg) * 1000; break;
        case 'b': timeout_hartbeat = atoi(optarg); break;
        default: help();
        }
    } while (nextOption != -1);

    ::Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    SPD_INFO("Starting {} v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    // MAIN

    // std::shared_ptr<Salsa::BrokerApp> pBroker = std::make_shared<Salsa::BrokerApp>();
    std::shared_ptr<Salsa::BrokerApp> pBroker =
        std::make_shared<Salsa::BrokerApp>(in, out, timeout_publish, timeout_clean, timeout_hartbeat);
    pBroker->timeout(timeout_poller);
    // pBroker->init();
    // pBroker->exec();
    // pBroker->finish();
    Salsa::MainApp mainApp;
    mainApp.addActor(pBroker);
    mainApp.run();

    // Release and close all loggers
    spdlog::drop_all();

    // Exit with success
    return 0;
}
