#include <random>
#include <czmq.h>
#include <getopt.h>
#include "salsa.hh"

// TODO
#define SALSA_SUBPROG "cpu"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{}-{} v{}.{}.{}-{}", salsa_NAME, SALSA_SUBPROG, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{

    spdlog::set_pattern("%v");
    SPD_INFO("Tool for generation of cpu job {}-{}.", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Usage: {}-{} [OPTION]...", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("       -n, --num-cores[=VALUE]  number of cores");
    SPD_INFO("       -t, --job-time[=VALUE]   job time");
    SPD_INFO("       -r, --rc[=VALUE]         program rc");
    SPD_INFO("           --silent             no logs printed");
    SPD_INFO("           --debug              debug logs printed");
    SPD_INFO("           --trace              trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help               display this help and exit");
    SPD_INFO("       -v, --version            output version information and exit");
    SPD_INFO("");
    SPD_INFO("Examples:");
    SPD_INFO("       salsa-cpu -n 4 -t 1000");
    SPD_INFO("                                runs on 4 cores each 1 sec (1000 ms)");
    SPD_INFO("");
    SPD_INFO("       salsa-cpu -n 10 -t 3000 -r 10");
    SPD_INFO("                                runs on 10 cores each 3 sec (3000 ms) with rc 10");
    SPD_INFO("");
    SPD_INFO("       salsa-cpu -n 10 -t 3000 -r -3");
    SPD_INFO("                                runs on 10 cores each 3 sec (3000 ms) with rc random number between 0-3");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

static void cpu_task(zsock_t * pipe, void * /*args*/)
{
    /// Loops until interrupted

    zsock_signal(pipe, 0);
    while (!zsys_interrupted) {
    }
}

int main(int argc, char * argv[])
{
    /** \brief Provides high intensity CPU usage

        Creates new thread looping in empty while for a user predefined amount of
        time.
    */

    // ***** Default values BEGIN *****
    int numCPU         = 1;
    int jobTime        = 1000;
    int execReturnCode = 0;

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("SALSA_DEBUG_LEVEL")) debugLevel = atoi(getenv("SALSA_DEBUG_LEVEL"));

    // ***** Default values END *****

    std::string   shortOpts  = "hvn:t:r:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"num-cores", required_argument, nullptr, 'n'},
                                {"job-time", required_argument, nullptr, 't'},
                                {"rc", required_argument, nullptr, 'r'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'n': numCPU = atoi(optarg); break;
        case 't': jobTime = atoi(optarg); break;
        case 'r': execReturnCode = atoi(optarg); break;
        case 'h': help(); break;
        case 'v': version(); break;
        default: help();
        }
    } while (nextOption != -1);

    ::Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    SPD_INFO("Starting {} v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    zactor_t * actor     = nullptr;
    zlistx_t * actorList = zlistx_new();
    for (int i = 0; i < numCPU; i++) {
        actor = zactor_new(cpu_task, nullptr);
        zlistx_add_end(actorList, actor);
    }

    zclock_sleep(jobTime);
    zsys_interrupted = 1;

    actor = static_cast<zactor_t *>(zlistx_first(actorList));
    while (actor) {
        zactor_destroy(&actor);
        actor = static_cast<zactor_t *>(zlistx_next(actorList));
    }

    // Release and close all loggers
    spdlog::drop_all();

    if (execReturnCode < 0) {
        std::random_device              rd;
        std::mt19937                    gen(rd());
        std::uniform_int_distribution<> dis(0, abs(execReturnCode));
        return dis(gen);
    }
    return execReturnCode;
}
