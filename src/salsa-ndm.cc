#include <fstream>
#include <getopt.h>
#include <memory>
#include <sstream>
#include <zyre.h>
#include <ndm/Space.hh>
#include <ndm/Config.hh>
#include <ndm/Utils.hh>
#include <ndm/Point.pb.h>
#include "salsa.hh"
#include "TaskInfo.pb.h"
#include "Job.hh"
#include "MainApp.hh"
#include "MonApp.hh"
#include "CliApp.hh"

using namespace fmt::literals;

// TODO
#define SALSA_SUBPROG "ndm"

[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{}-{} v{}.{}.{}-{}", salsa_NAME, SALSA_SUBPROG, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    SPD_INFO("NDM tool to submit jobs to {}.", salsa_NAME);
    SPD_INFO("");
    SPD_INFO("Usage: {}-{} [OPTION]...", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("       -s, --submitter[=VALUE]   submitter url (tcp://localhost:41000)");
    SPD_INFO("       -c, --config[=VALUE]     ndm config file");
    SPD_INFO("       -b, --broker[=VALUE]     broker url used for monitoring");
    SPD_INFO("           --batch              job is submited in batch mode (no tui)");
    SPD_INFO("           --rc                 flag if results of taks handled (experimental)");
    SPD_INFO("           --logs               flag if logs are produced");
    SPD_INFO("           --silent             no logs printed");
    SPD_INFO("           --debug              debug logs printed");
    SPD_INFO("           --trace              trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help               display this help and exit");
    SPD_INFO("       -v, --version            output version information and exit");
    SPD_INFO("");
    SPD_INFO("Examples:");
    SPD_INFO("       salsa-ndm -s tcp://localhost:41000 -c ndm.yaml");
    SPD_INFO("                                submits jobs from ndm.yaml to localhost:41000");
    SPD_INFO("");
    SPD_INFO("       salsa-ndm -s tcp://localhost:41000 -b tcp://localhost:5001 -c ndm.yaml");
    SPD_INFO("                                submits jobs from ndm.yaml to localhost:41000 ");
    SPD_INFO("                                and monitor broker at localhost:5001");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{

    // ***** Default values BEGIN *****
    // Log level : 'info'
    int debugLevel = static_cast<int>(spdlog::level::info);

    // Submitter connect url
    std::string submitterUrl = "";
    // Command
    std::string configFile = "";
    // Broker url
    std::string brokerUrl = "";
    // flag if running in batch mode
    int isBatch = 0;
    // flag if results tasks are handled
    int isResultTask = 0;
    // flag is logs are produced
    int produceLogs = 0;

    // ***** Default values END *****

    std::string   short_options  = "hvs:c:b:W;";
    struct option long_options[] = {{"help", no_argument, nullptr, 'h'},
                                    {"version", no_argument, nullptr, 'v'},
                                    {"submitter", required_argument, nullptr, 's'},
                                    {"config", required_argument, nullptr, 'c'},
                                    {"broker", required_argument, nullptr, 'b'},
                                    {"batch", no_argument, &isBatch, static_cast<int>(1)},
                                    {"rc", no_argument, &isResultTask, static_cast<int>(1)},
                                    {"logs", no_argument, &produceLogs, static_cast<int>(1)},
                                    {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                    {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                    {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                    {nullptr, 0, nullptr, 0}};

    int nextOption = 0;

    do {
        nextOption = getopt_long(argc, argv, short_options.c_str(), long_options, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help(); break;
        case 'v': version(); break;
        case 's': submitterUrl = fmt::format(">{}", optarg); break;
        case 'c': configFile = optarg; break;
        case 'b': brokerUrl = fmt::format(">{}", optarg); break;
        default: help();
        }
    } while (nextOption != -1);

    ::Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    SPD_INFO("Starting {}-feeder v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    if (submitterUrl.empty() && configFile.empty()) help();

    std::vector<NDM::Point> points;
    SPD_INFO("Using config file [{}] ...", configFile);
    NDM::Config cfg;
    if (!cfg.load(configFile)) {
        SPD_CRIT("Problem loading config file [{}] !!!", configFile);
        return 1;
    }

    Salsa::Job *  job = nullptr;
    Salsa::CliApp cliApp(submitterUrl);
    cliApp.init();
    job = cliApp.generateJobFromNdm(points, cfg, produceLogs);
    if (job) {
        cliApp.sendJob(job);
        if (!isBatch) {
            if (isResultTask) {
                while (!zsys_interrupted) {
                    if (cliApp.recv(job) == -1) continue;
                    if (job->isFinished()) {
                        SPD_INFO("Job [{}] has finished", job->uuid());
                        job->print();
                        break;
                    }
                }
            }
            cliApp.mon(job, true, brokerUrl);
        }
        else {
            cliApp.mon(job, false, brokerUrl);
            zclock_sleep(100);
        }
    }
    // cliApp.exec();
    cliApp.finish();
    return 0;
}
