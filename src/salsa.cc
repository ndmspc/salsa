#include <fstream>
#include <getopt.h>
#include <iostream>
#include <sstream>

#include <czmq.h>
#include <yaml.h>

#include "salsa.hh"

#include "NodeInfo.pb.h"
#include "NodeZyre.hh"
#include "ConfigZyre.hh"
#include "PublisherZmq.hh"
#include "SocketZyre.hh"
#include "MainApp.hh"
#include "BrokerApp.hh"

using namespace fmt::literals;

[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{} v{}.{}.{}-{}", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION), salsa_VERSION_MINOR(salsa_VERSION),
             salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    SPD_INFO("Cluster managment for salsa.");
    SPD_INFO("");
    SPD_INFO("Usage: {} [OPTION]...", salsa_NAME);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("       -c, --config[=VALUE]     using config file (default: /etc/salsa/default.yaml)");
    SPD_INFO("       -n, --networks[=VALUE]   network list separated by ','");
    SPD_INFO("       -p, --publisher[=VALUE]  url to publisher");
    SPD_INFO("       -e, --endpoint[=VALUE]   endpoind for gossip discovery");
    SPD_INFO("           --no-broker          runs without internal broker");
    SPD_INFO("           --silent             no logs printed");
    SPD_INFO("           --debug              debug logs printed");
    SPD_INFO("           --trace              trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help               display this help and exit");
    SPD_INFO("       -v, --version            output version information and exit");
    SPD_INFO("");
    SPD_INFO("Examples:");
    SPD_INFO("       salsa");
    SPD_INFO("                                redirector and worker via udp discovery (config: default.yaml)");
    SPD_INFO("       salsa -n wk");
    SPD_INFO("                                worker only");
    SPD_INFO("       salsa -n rdr -p tcp://localhost:5000");
    SPD_INFO("                                redirector only and job info will be published to tcp://localhost:5000");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

// =============================================================================
int main(int argc, char ** argv)
{
    // ***** Default values BEGIN *****
    // Config file
    std::string configFile = "default.yaml";
    if (getenv("SALSA_CONFIG")) {
        configFile = getenv("SALSA_CONFIG");
    }
    else {
        if (!zsys_file_exists(configFile.c_str())) {
            if (getenv("SALSA_ROOT")) {
                configFile = fmt::format("{}/etc/default.yaml", getenv("SALSA_ROOT"));
            }
            else {
                configFile = "/etc/salsa/default.yaml";
            }
        }
    }

    // Log dir
    // if (!getenv("SALSA_LOG_DIR")) {
    //     setenv("SALSA_LOG_DIR", "/tmp/salsa/logs", 1);
    // }

    // Networks
    std::string networks;
    if (getenv("SALSA_NETWORKS")) {
        networks = getenv("SALSA_NETWORKS");
    }

    // Endpoint (eg: tcp://ip:*)
    std::string endpoint;
    if (getenv("SALSA_ENDPOINT")) {
        endpoint = getenv("SALSA_ENDPOINT");
    }

    // Log level : 'info'
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("SALSA_DEBUG_LEVEL")) {
        debugLevel = atoi(getenv("SALSA_DEBUG_LEVEL"));
    }

    // Publisher
    std::string publisher;
    if (getenv("SALSA_PUB_URL")) {
        publisher = ">";
        publisher.append(getenv("SALSA_PUB_URL"));
    }

    int useBroker = 1;
    // ***** Default values END *****

    std::string   shortOpts  = "hvc:n:p:e:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"config", required_argument, nullptr, 'c'},
                                {"publisher", required_argument, nullptr, 'p'},
                                {"networks", required_argument, nullptr, 'n'},
                                {"endpoint", required_argument, nullptr, 'e'},
                                {"broker", required_argument, nullptr, 'b'},
                                {"no-broker", no_argument, &useBroker, static_cast<int>(0)},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help(); break;
        case 'v': version(); break;
        case 'c': configFile = optarg; break;
        case 'p': publisher = fmt::format(">{}", optarg); break;
        case 'n': networks = optarg; break;
        case 'e': endpoint = optarg; break;
        default: help();
        }
    } while (nextOption != -1);

    SPD_INFO("Starting {} v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    ::Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    if (useBroker) {
        std::string ntmp = networks.substr(0, 3);
        if (!(ntmp.empty() || ntmp == "rdr")) useBroker = false;
    }

    int rc = 0;

    // Begin loading configuration file
    if (!zsys_file_exists(configFile.c_str())) {
        SPD_CRIT("Config file [{}] doesn't exists!!!", configFile);
        return ++rc;
    }

    SPD_INFO("Using config file [{}] ...", configFile);
    Salsa::ConfigZyre cfg;
    if (!cfg.load(configFile)) {
        SPD_CRIT("Problem loading config file [{}] !!!", configFile);
        return ++rc;
    }

    // Filter out all but requred networks
    SPD_DEBUG("Networks : [{}]", networks);
    cfg.filter(networks);

    // Main ?
    // std::vector<std::shared_ptr<Salsa::ActorZmq>> actors;
    Salsa::MainApp mainApp;

    // Genereate salsa node from config file
    std::shared_ptr<Salsa::Node> pRootNode = cfg.apply(mainApp.actors());
    // std::shared_ptr<Salsa::Node> pRootNode = cfg.apply(actors);

    // Sanity check
    if (!pRootNode) {
        SPD_CRIT("Failed to apply config !!! Exiting...");
        return ++rc;
    }

    if (!getenv("SALSA_ENDPOINT")) {
        setenv("SALSA_ENDPOINT", endpoint.data(), 1);
    }

    if (!getenv("SALSA_PUB_URL") && !publisher.empty()) {
        setenv("SALSA_PUB_URL", publisher.data(), 1);
    }

    pRootNode->print();

    std::shared_ptr<Salsa::BrokerApp> pBroker = nullptr;
    if (useBroker) {
        pBroker = std::make_shared<Salsa::BrokerApp>();
        pBroker->timeout(100);
        pBroker->timeoutNoActivity(10000);
        pBroker->timeoutClean(30000);
        mainApp.addActor(pBroker);
    }
    mainApp.run();

    // Release and close all loggers
    spdlog::drop_all();

    // Exit with success
    return 0;
}
