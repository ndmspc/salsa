
#include <getopt.h>
#include "salsa.hh"
#include "MonApp.hh"

// TODO
#define SALSA_SUBPROG "mon"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{}-{} v{}.{}.{}-{}", salsa_NAME, SALSA_SUBPROG, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    SPD_INFO("Monitoring tool for {}.", salsa_NAME);
    SPD_INFO("");
    SPD_INFO("Usage: {}-{} [OPTION]...", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("       -b, --broker[=VALUE]     broker brokerUrl (default: tcp://localhost:5001)");
    SPD_INFO("       -s, --subscribe[=VALUE]  subscribe string (default \"\")");
    SPD_INFO("       -m, --mgrid[=VALUE]      sets job manager id");
    SPD_INFO("       -j, --jobid[=VALUE]      sets job id");
    SPD_INFO("           --silent             no logs printed");
    SPD_INFO("           --debug              debug logs printed");
    SPD_INFO("           --trace              trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help               display this help and exit");
    SPD_INFO("       -v, --version            output version information and exit");
    SPD_INFO("");
    SPD_INFO("Examples:");
    SPD_INFO("       salsa-mon -j F73A75FE0C0A4C959C4CA81232C9AFFA");
    SPD_INFO("                                runs jobid F73A75FE0C0A4C959C4CA81232C9AFFA");
    SPD_INFO("");
    SPD_INFO("       salsa-mon -m E75011F6D06648FF9F0FCE71B93243CF -j F73A75FE0C0A4C959C4CA81232C9AFFA");
    SPD_INFO("                                runs mgrid E75011F6D06648FF9F0FCE71B93243CF and jobid "
             "F73A75FE0C0A4C959C4CA81232C9AFFA");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{
    // ***** Default values BEGIN *****
    // brokerUrl
    std::string brokerUrl = ">tcp://localhost:5001";
    std::string subscribe = "salsa";
    // Manager ID
    std::string mgrid_str;
    // Job ID
    std::string jobid_str;

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("SALSA_DEBUG_LEVEL")) debugLevel = atoi(getenv("SALSA_DEBUG_LEVEL"));
    // ***** Default values END *****

    std::string   shortOpts  = "hb:s:m:j:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"broker", required_argument, nullptr, 'b'},
                                {"subscribe", required_argument, nullptr, 's'},
                                {"mgrid", required_argument, nullptr, 'm'},
                                {"jobid", required_argument, nullptr, 'j'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'b': brokerUrl = fmt::format(">{}", optarg); break;
        case 's': subscribe = optarg; break;
        case 'm': mgrid_str = optarg; break;
        case 'j': jobid_str = optarg; break;
        default: help();
        }
    } while (nextOption != -1);

    Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    SPD_INFO("Starting {} v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    // MAIN
    if (!mgrid_str.empty()) {
        subscribe = fmt::format("salsa:{}", mgrid_str);
    }

    Salsa::MonApp app(brokerUrl, subscribe, mgrid_str, jobid_str, true);
    app.exec();

    // Release and close all loggers
    spdlog::drop_all();

    // Exit with success
    return 0;
}
