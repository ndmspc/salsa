#include <czmq.h>
#include <getopt.h>
#include "salsa.hh"
#include "MainApp.hh"
#include "TestApp.hh"

#define SALSA_SUBPROG "test-actor"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{}-{} v{}.{}.{}-{}", salsa_NAME, SALSA_SUBPROG, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    SPD_INFO("Testing tool for {}-{}.", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Usage: {}-{} [OPTION]...", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("           --debug              debug logs printed");
    SPD_INFO("           --trace              trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help               display this help and exit");
    SPD_INFO("       -v, --version            output version information and exit");
    SPD_INFO("");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{
    // ***** Default values BEGIN *****

    // Manager ID
    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("SALSA_DEBUG_LEVEL")) debugLevel = atoi(getenv("SALSA_DEBUG_LEVEL"));

    // ***** Default values END *****

    std::string   shortOpts  = "hvW;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help(); break;
        case 'v': version(); break;
        default: help();
        }
    } while (nextOption != -1);

    ::Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    SPD_INFO("Starting {} v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    // std::shared_ptr<Salsa::BrokerApp> pTestApp = std::make_shared<Salsa::BrokerApp>();
    std::shared_ptr<Salsa::TestApp> pTestApp = std::make_shared<Salsa::TestApp>();
    // pTestApp->init();
    // pTestApp->exec();
    // pTestApp->finish();
    Salsa::MainApp mainApp;
    mainApp.addActor(pTestApp);
    mainApp.run();

    // Release and close all loggers
    spdlog::drop_all();

    // Exit with success
    return 0;
}
