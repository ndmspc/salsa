#include <czmq.h>
#include <fstream>
#include <memory>
#include "salsa.hh"
#include <spdlog/sinks/stdout_sinks.h>

int main(int argc, char ** argv)
{
    if (argc != 2)
        return 1;

    std::ifstream ifs(argv[1]);

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_logger_mt("console");
    SPD_TRACE("Running Exec client...");

    zsock_t * dealer = zsock_new_dealer(">tcp://localhost:5555");
    if (dealer == nullptr)
    {
        SPD_TRACE("Failed to create dealer!");
        return 2;
    }

    uint64_t    id      = 0;
    std::string command = "";
    do
    {
        std::getline(ifs, command);
        if (command.empty())
            continue;
        SPD_TRACE("[ID {:016X}][{}]", id++, command);
        zstr_send(dealer, command.c_str());
        zclock_sleep(50);
    } while (!zsys_interrupted && !command.empty());
    SPD_TRACE("----- Sent [{}] jobs -----", id);
    SPD_TRACE("Destroying and exiting...");

    zsock_destroy(&dealer);

    return 0;
}
