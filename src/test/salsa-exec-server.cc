#include <algorithm>
#include <queue>
#include <thread>
#include <vector>
#include <czmq.h>
#include "salsa.hh"
#include "ActorZmq.hh"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
// Just,... why?
static std::atomic<bool> interrupted(false);
static void              mkInterrupt(int signal);
static void              mkInterrupt(int signal)
{
    if ((signal)) {
        ;
    }
    interrupted = true;
}
#pragma clang diagnostic pop

void send_job(zactor_t *, std::string, uint64_t);
void send_job(zactor_t * actor, std::string cmd, uint64_t jobID)
{
    SPD_TRACE("Sending job [{}] ID[0x{:016X}] to actor [{}]", cmd, jobID, static_cast<void *>(actor));

    zmsg_t * msg = zmsg_new();
    zmsg_addstrf(msg, "%s", cmd.c_str());
    zmsg_addstrf(msg, "%s", fmt::format("file:///tmp/salsa/salsa-{:016X}.log", jobID).c_str());
    zsock_send(actor, "m", msg);
    zmsg_destroy(&msg);

    SPD_TRACE("Job ID[0x{:016X}] sent!", jobID);
}

int main()
{
    SPD_TRACE("Running Salsa Exec Server...");

    // Attach signals
    SPD_TRACE("Attaching signals");
    std::signal(SIGINT, mkInterrupt);
    std::signal(SIGTERM, mkInterrupt);

    // Create socket ROUTER
    SPD_TRACE("Creating zRouter");
    zsock_t * router = zsock_new_router("@tcp://*:5555");
    if (router == nullptr) {
        SPD_ERROR("Failed to create new router!");
        return 2;
    }

    unsigned int const nthreads = std::thread::hardware_concurrency();
    SPD_TRACE("Detected {} cores", nthreads);
    if (nthreads == 0) {
        SPD_ERROR("Failed to detect thread count!");
        return 0;
    }

    uint64_t jobid = 0;

    std::map<unsigned int, bool> nodeStatus;
    std::queue<std::string>      availableJobs;

    // Create actors
    std::vector<zactor_t *> zactors;

    // Create poller and add router to it
    zpoller_t * poller = zpoller_new(nullptr);
    zpoller_add(poller, router);

    // Create actors
    SPD_TRACE("Creating actors");
    for (unsigned int i = 0; i < nthreads; i++) {
        SPD_TRACE("-> zactor ID[{}]", i);
        zactor_t * actor = zactor_new(Salsa::ActorZmq::SalsaActorForkFn, nullptr);
        zpoller_add(poller, actor);
        zactors.push_back(actor);
        nodeStatus.insert(std::make_pair(i, false));
    }

    while (!zsys_interrupted) {
        void * res = zpoller_wait(poller, -1);
        if (res == router) {
            char * msg = zstr_recv(router);
            if (!msg) continue;
            SPD_TRACE("Got message [{}]", msg);
            if (msg[0] != '\0') availableJobs.push(std::string(msg));
            free(msg);
            // END of add to queue
        }
        else {
            unsigned int position = 0;
            for (auto actor : zactors) {
                if (actor == res) {
                    std::map<unsigned int, bool>::iterator it = nodeStatus.find(position);
                    if (it != nodeStatus.end()) {
                        zmsg_t * msg = zmsg_recv(actor);
                        if (zframe_streq(zmsg_first(msg), "$EXIT")) {
                            char * exitStat = zframe_strdup(zmsg_next(msg));
                            SPD_TRACE("Actor [{}][{}] exited [{}]", position, static_cast<void *>(actor), exitStat);
                            free(exitStat);
                            it->second = false;
                        }
                        zmsg_destroy(&msg);
                    }
                    else
                        SPD_ERROR("INTERNAL - Cannot find map entry for ID {:02X}", position);
                }
                else
                    position++;
            } // END of actor clearing
        }     // END of socket resolving

        if (!availableJobs.empty()) {
            for (auto & actor : nodeStatus) {
                if (actor.second == false) {
                    SPD_TRACE("Found free actor ID[{}][{}]", actor.first, actor.second);
                    send_job(zactors[actor.first], availableJobs.front(), jobid++);

                    SPD_TRACE("-> Waiting for actor to return PID");
                    zmsg_t * msg = zmsg_recv(zactors[actor.first]);
                    zmsg_first(msg);
                    char * cpid = zframe_strdup(zmsg_next(msg));
                    SPD_TRACE("-> Got PID [{}]", cpid);
                    free(cpid);
                    zmsg_destroy(&msg);

                    // Error checking perhaps?
                    nodeStatus.find(actor.first)->second = true;
                    availableJobs.pop();
                    SPD_TRACE("-> --- DONE: Job sent ---");
                    break;
                } // END if actor free
            }
        } // END assign job
    }     // END while not interrupted

    SPD_TRACE("<<zsys_interrupted>> Shutting down...");

    SPD_TRACE("Cleaning up");
    for (auto & zactor : zactors) {
        SPD_TRACE("Destroying actor {}", static_cast<void *>(zactor));
        zactor_destroy(&zactor);
    }

    if (!availableJobs.empty()) SPD_WARN("Unexecuted jobs will be lost!");

    zpoller_destroy(&poller);
    zsock_destroy(&router);
    SPD_TRACE("Exiting...");
    return 0;
}
