#include <czmq.h>
#include <getopt.h>
#include <curses.h>
#include "salsa.hh"
#include "JobWindow.hh"
#include "TuiUtils.hh"

#define SALSA_SUBPROG "tui"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    SPD_INFO("{}-{} v{}.{}.{}-{}", salsa_NAME, SALSA_SUBPROG, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    SPD_INFO("Testing tool for {}-{}.", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Usage: {}-{} [OPTION]...", salsa_NAME, SALSA_SUBPROG);
    SPD_INFO("");
    SPD_INFO("Options:");
    SPD_INFO("       -m, --mgrid[=VALUE]      sets job manager id");
    SPD_INFO("       -j, --jobid[=VALUE]      sets job id");
    SPD_INFO("           --silent             no logs printed");
    SPD_INFO("           --debug              debug logs printed");
    SPD_INFO("           --trace              trace logs printed");
    SPD_INFO("");
    SPD_INFO("       -h, --help               display this help and exit");
    SPD_INFO("       -v, --version            output version information and exit");
    SPD_INFO("");
    SPD_INFO("Examples:");
    SPD_INFO("       salsa-tui -j ID1");
    SPD_INFO("                                runs jobid ID1 (jobs available : ID1, ID2, ID3, ID4)");
    SPD_INFO("");
    SPD_INFO("       salsa-tui -m E75011F6D06648FF9F0FCE71B93243CF -j ID1");
    SPD_INFO("                                runs mgrid E75011F6D06648FF9F0FCE71B93243CF and jobid ID1");
    SPD_INFO("");
    SPD_INFO("Report bugs to: martin.vala@cern.ch");
    SPD_INFO("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    SPD_INFO("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{
    // ***** Default values BEGIN *****

    // Manager ID
    std::string mgrid_str = "7A642BE6420F466A8D9AD107B8F85C7D";

    // Job ID
    std::string jobid_str;

    // Test date sample
    std::string data = "{\"jobs\":[{\"A\":0,\"D\":100,\"F\":0,\"P\":0,\"R\":0,\"name\":\"ID1"
                       "\"},{\"A\":0,\"D\":0,\"F\":0,\"P\":10,\"R\":4,\"name\":\"ID2\"},{\"A\":0,\"D\":58,\"F\":3,"
                       "\"P\":0,\"R\":0,\"name\":\"ID3\"},{\"A\":0,\"D\":15,\"F\":0,"
                       "\"P\":0,\"R\":1,\"name\":\"ID4\"},{\"A\":0,\"D\":0,\"F\":0,"
                       "\"P\":100,\"R\":0,\"name\":\"ID5\"},{\"A\":100,\"D\":0,\"F\":0,"
                       "\"P\":0,\"R\":0,\"name\":\"ID6\"},{\"A\":0,\"D\":0,\"F\":0,"
                       "\"P\":0,\"R\":100,\"name\":\"ID7\"},{\"A\":0,\"D\":100,\"F\":0,"
                       "\"P\":0,\"R\":0,\"name\":\"ID8\"},{\"A\":0,\"D\":0,\"F\":100,"
                       "\"P\":0,\"R\":0,\"name\":\"ID9\"},{\"A\":0,\"D\":50,\"F\":50,"
                       "\"P\":0,\"R\":0,\"name\":\"ID10\"}]}";

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("SALSA_DEBUG_LEVEL")) debugLevel = atoi(getenv("SALSA_DEBUG_LEVEL"));

    // ***** Default values END *****

    std::string   shortOpts  = "hvm:j:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"mgrid", required_argument, nullptr, 'm'},
                                {"jobid", required_argument, nullptr, 'j'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'm': mgrid_str = optarg; break;
        case 'j': jobid_str = optarg; break;
        case 'h': help(); break;
        case 'v': version(); break;
        default: help();
        }
    } while (nextOption != -1);

    ::Salsa::Object::setConsoleLevel(static_cast<spdlog::level::level_enum>(debugLevel));

    SPD_INFO("Starting {} v{}.{}.{}-{} ...", salsa_NAME, salsa_VERSION_MAJOR(salsa_VERSION),
             salsa_VERSION_MINOR(salsa_VERSION), salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);

    // MAIN
    if (Salsa::TuiUtils::tui_init()) {
        mvprintw(LINES - 2, 1, "(F10 to Exit)");

        Salsa::JobWindow jw;
        if (!jobid_str.empty()) jw.cur_job_id(jobid_str);
        if (!mgrid_str.empty()) jw.cur_job_manager(mgrid_str);

        if (jw.update(mgrid_str, data)) jw.update("");

        nodelay(stdscr, 0);
        int ch;
        while ((ch = getch()) != KEY_F(10)) {
        }

        jw.destroy();
        // Destroy tui
        Salsa::TuiUtils::tui_destroy();
    }
    else
        SPD_ERROR("Problem initializing tui interface ...");

    // Release and close all loggers
    spdlog::drop_all();

    // Exit with success
    return 0;
}
