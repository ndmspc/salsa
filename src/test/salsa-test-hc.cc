#include "salsa.hh"
#include "HyperCube.hh"

int main(int argc, char ** args)
{
    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_color_mt("console");

    int power = 3;
    int start = 1;

    if (argc > 1) power = atoi(args[1]);
    if (argc > 2) start = atoi(args[2]);

    logger->info("Hyper Cube : power={} start={}", power, start);

    Salsa::HyperCube hc(power, start);

    for (std::size_t i = 1; i < 1030; i++) hc.addNode("node-" + std::to_string(i));

    hc.removeNode("node-6");

    auto nVertex = pow(2, power);

    if (hc._nodeMap.size() < nVertex) {
        logger->info("for work you need to add {} nodes", nVertex - hc._nodeMap.size());
    }
    else {
        hc.createAdjMatrix();
        hc.createPaths();
        hc.print();
    }

    // Doesn't this get done automatically? (That's kind of purpose of
    // shared_ptr...)
    spdlog::drop_all();

    return 0;
}
