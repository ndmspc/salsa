#include "salsa.hh"
#include "NDimMapping.hh"

int main()
{

    std::shared_ptr<spdlog::logger> logger = spdlog::stdout_color_mt("console");

    Salsa::NDimMapping ndm;
    ndm.print();
    // Doesn't this get done automatically? (That's kind of purpose of
    // shared_ptr...)
    spdlog::drop_all();

    return 0;
}
