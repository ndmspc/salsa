Usage - test/dev apps
=================

## salsa-exec-server
```sh
./salsa-exec-server # Just run the exec
```

## salsa-exec-client
```sh
./salsa-exec-client <list>
```
First argument is file that contains command list one per line.
