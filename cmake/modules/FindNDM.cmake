# Find NDM
#
# Find the NDM includes and library
# 
# if you nee to add a custom library search path, do it via via CMAKE_PREFIX_PATH 
# 
# This module defines
#  NDM_INCLUDE_DIRS, where to find header, etc.
#  NDM_LIBRARIES, the libraries needed to use NDM.
#  NDM_FOUND, If false, do not try to use NDM.

# only look in default directories
find_path(
	NDM_INCLUDE_DIR 
	NAMES ndm/ndm.hh
	DOC "NDM include dir"
	HINTS $ENV{NDM_INCLUDE_DIR}
)

find_path(
	NDM_LIB_DIR 
	NAMES libndim.so
	DOC "NDM lib dir"
	HINTS $ENV{NDM_LIB_DIR} /usr/lib64
)

find_library(
	NDM_LIBRARY
	NAMES ndim ndim_proto
	DOC "NDM libraries"
	HINTS $ENV{NDM_LIB_DIR}
)

set(NDM_INCLUDE_DIRS ${NDM_INCLUDE_DIR})
set(NDM_LIBRARIES ${NDM_LIBRARY})

# handle the QUIETLY and REQUIRED arguments and set NDM_FOUND to TRUE
# if all listed variables are TRUE, hide their existence from configuration view
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(NDM DEFAULT_MSG NDM_LIBRARIES NDM_INCLUDE_DIR)
mark_as_advanced (NDM_FOUND NDM_INCLUDE_DIR NDM_LIBRARY NDM_LIB_DIR)

