# Find SALSA
#
# Find the SALSA includes and library
# 
# if you nee to add a custom library search path, do it via via CMAKE_PREFIX_PATH 
# 
# This module defines
#  SALSA_INCLUDE_DIRS, where to find header, etc.
#  SALSA_LIBRARIES, the libraries needed to use SALSA.
#  SALSA_FOUND, If false, do not try to use SALSA.

# only look in default directories
find_path(
	SALSA_INCLUDE_DIR 
	NAMES salsa/salsa.hh
	DOC "SALSA include dir"
	HINTS $ENV{SALSA_INCLUDE_DIR}
)

find_path(
	SALSA_LIB_DIR 
	NAMES libSalsaBase.so
	DOC "SALSA lib dir"
	HINTS $ENV{SALSA_LIB_DIR} /usr/lib64
)

find_library(
	SALSA_LIBRARY
	NAMES SalsaProto SalsaBase SalsaZyre SalsaTui SalsaApp
	DOC "SALSA libraries"
	HINTS $ENV{SALSA_LIB_DIR}
)

set(SALSA_INCLUDE_DIRS ${SALSA_INCLUDE_DIR})
set(SALSA_LIBRARIES ${SALSA_LIBRARY})

# handle the QUIETLY and REQUIRED arguments and set SALSA_FOUND to TRUE
# if all listed variables are TRUE, hide their existence from configuration view
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SALSA DEFAULT_MSG SALSA_LIBRARY SALSA_INCLUDE_DIR)
mark_as_advanced (SALSA_FOUND SALSA_INCLUDE_DIR SALSA_LIBRARY SALSA_LIB_DIR)

