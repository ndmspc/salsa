#pragma once

#include <json/json.h>
#include "Object.hh"
#include "JobInfo.pb.h"
#include "TuiUtils.hh"

namespace Salsa {
///
/// \class JobWindow
///
/// \brief  JobWindow class for TUI
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class JobWindow : public Object {
public:
    JobWindow();
    virtual ~JobWindow();

    bool update(std::string mgr, std::string data);
    void update(std::string msg = "Waiting for job...");
    void destroy();

    /// Sets job manager
    void cur_job_manager(std::string m) { mCurJobManager = m; }
    /// Returns job manager
    std::string cur_job_manager() const { return mCurJobManager; }

    /// Sets job id
    void cur_job_id(std::string t) { mCurJobID = t; }
    /// Returns job id
    std::string cur_job_id() const { return mCurJobID; }

    /// Sets job info
    void job_info(JobInfo * ji) { mJobInfo = ji; }
    /// Returns Job info
    JobInfo * job_info() const { return mJobInfo; }

    /// Rerurns window
    WINDOW * window() const { return mpWindow; }

private:
    WINDOW *           mpWindow{nullptr};     ///< Current window
    Json::CharReader * mpJsonReader{nullptr}; ///< JSON reader
    std::string        mCurJobManager{};      ///< Current job manager
    std::string        mCurJobID{};           ///< Current job id
    int                mXMax{COLS};           ///< X max
    int                mYMax{LINES};          ///< y max
    int                mXPos{0};              ///< Current x position
    int                mYPos{0};              ///< Current y position
    JobInfo *          mJobInfo{nullptr};     ///< Current Job Info
    // int         mColor{0};         ///< Current color

    /// Maps value to lenght size
    inline uint32_t map(uint32_t length, uint32_t max, uint32_t calc) { return max ? (length * calc) / max : 0; }
    void stat_line(int yPos, int xPos, std::string key, std::string value, int color = COLOR_PAIR(TUI_CP_WIN_REV));
};

} // namespace Salsa
