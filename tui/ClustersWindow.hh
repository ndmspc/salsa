#pragma once

#include "Object.hh"

namespace Salsa {
///
/// \class ClustersWindow
///
/// \brief  ClustersWindow class for TUI
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class ClustersWindow : public Object {
public:
    ClustersWindow();
    virtual ~ClustersWindow();
};

} // namespace Salsa
