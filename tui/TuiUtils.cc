#include <curses.h>
#include "TuiUtils.hh"
namespace Salsa {

TuiUtils::TuiUtils() : Object()
{
    ///
    /// Constructor
    ///
}
TuiUtils::~TuiUtils()
{
    ///
    /// Destructor
    ///
}

bool TuiUtils::tui_init(bool use_cbreak)
{
    ///
    /// Initialize ncurses and tui
    ///

    initscr();     /* Start curses mode 		*/
    start_color(); /* Start the color functionality */

    if (has_colors() == FALSE) {
        endwin();
        printf("Your terminal does not support color\n");
        return false;
    }

    use_default_colors();

    init_pair(TUI_CP_SCR, COLOR_WHITE, COLOR_BLUE);
    init_pair(TUI_CP_WIN, COLOR_BLACK, COLOR_WHITE);
    init_pair(TUI_CP_WIN_REV, COLOR_WHITE, COLOR_BLACK);
    init_pair(TUI_CP_OK, COLOR_WHITE, COLOR_GREEN);
    init_pair(TUI_CP_RUN, COLOR_WHITE, COLOR_YELLOW);
    init_pair(TUI_CP_TOT, COLOR_WHITE, COLOR_BLUE);
    init_pair(TUI_CP_ERR, COLOR_WHITE, COLOR_RED);
    init_pair(TUI_CP_SHADOW, COLOR_BLACK, COLOR_BLACK);
    init_pair(TUI_CP_HEAD, COLOR_BLACK, COLOR_WHITE);

    if (use_cbreak)
        cbreak();
    else
        raw();
    noecho();
    curs_set(0);
    nodelay(stdscr, 1);
    keypad(stdscr, 1);
    bkgd(COLOR_PAIR(TUI_CP_SCR));
    refresh();
    return true;
}

bool TuiUtils::tui_destroy()
{
    ///
    /// Destroy ncurses and tui
    ///

    endwin();
    return true;
}

std::string TuiUtils::current_time(std::string format)
{
    ///
    /// Returns string with current date and time
    ///
    std::chrono::time_point<std::chrono::system_clock> time_now   = std::chrono::system_clock::now();
    std::time_t                                        time_now_t = std::chrono::system_clock::to_time_t(time_now);
    std::tm                                            now_tm     = *std::localtime(&time_now_t);
    char                                               buf[64];
    std::strftime(buf, 64, format.data(), &now_tm);

    return fmt::format("{}", buf);
}

} // namespace Salsa
