#pragma once

#include "Object.hh"

namespace Salsa {
///
/// \class JobsWindow
///
/// \brief  JobsWindow class for TUI
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class JobsWindow : public Object {
public:
    JobsWindow();
    virtual ~JobsWindow();
};

} // namespace Salsa
