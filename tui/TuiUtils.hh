#pragma once

#include <curses.h>
#include "Object.hh"

namespace Salsa {
///
/// \class TuiUtils
///
/// \brief  TuiUtils class for TUI
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class TuiUtils : public Object {
public:
    TuiUtils();
    virtual ~TuiUtils();

    static bool        tui_init(bool use_cbreak = false);
    static bool        tui_destroy();
    static std::string current_time(std::string format = "%Y-%m-%d %H:%M:%S");
};

} // namespace Salsa
