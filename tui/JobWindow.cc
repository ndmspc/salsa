
#include <chrono> // chrono::system_clock
#include <ctime>  // localtime
#include <iomanip>
#include "TuiUtils.hh"
#include "JobWindow.hh"
namespace Salsa {

JobWindow::JobWindow() : Object()
{
    ///
    /// Constructor
    ///

    mXMax = COLS / 2;
    mYMax = LINES / 3;
    if (mYMax < 14) mYMax = 14;

    mYPos = (LINES - mYMax) / 2;
    mXPos = (COLS - mXMax) / 2;

    Json::CharReaderBuilder builder;
    mpJsonReader = builder.newCharReader();

    update();
}
JobWindow::~JobWindow()
{
    ///
    /// Destructor
    ///

    if (mpWindow) destroy();

    delete mpJsonReader;
    delete mJobInfo;
}

void JobWindow::destroy()
{
    ///
    /// Destroy window
    /// It hast to be called before 'Salsa::TuiUtils::tui_destroy()'
    ///

    wborder(mpWindow, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    wclear(mpWindow);
    wrefresh(mpWindow);
    delwin(mpWindow);
    mpWindow = nullptr;
    bkgd(COLOR_PAIR(TUI_CP_SCR));
    refresh();
}

bool JobWindow::update(std::string mgr, std::string data)
{
    ///
    /// Update window with data
    ///

    if (mgr != mCurJobManager) return false;

    if (!mJobInfo) mJobInfo = new Salsa::JobInfo();

    Json::Value pRoot;
    std::string errors;
    bool        found = false;
    if (mpJsonReader->parse(data.data(), data.data() + data.size(), &pRoot, &errors)) {
        for (auto & outer : pRoot["jobs"]) {
            if (cur_job_id() != outer["name"].asString()) continue;
            mJobInfo->set_jobid(outer["name"].asString());
            mJobInfo->set_pending(outer["P"].asUInt());
            mJobInfo->set_assigned(outer["A"].asUInt());
            mJobInfo->set_running(outer["R"].asUInt());
            mJobInfo->set_done(outer["D"].asUInt());
            mJobInfo->set_failed(outer["F"].asUInt());
            mJobInfo->set_total(outer["P"].asUInt() + outer["A"].asUInt() + outer["R"].asUInt() + outer["D"].asUInt() +
                                outer["F"].asUInt());
            found = true;
            break;
        }
    }
    else {
        SPD_ERROR("JSON parse error: {}", errors);
        return false;
    }

    if (!found) return false;

    return true;
}

void JobWindow::update(std::string msg)
{
    ///
    /// Updates window content
    ///

    if (mpWindow == nullptr) {
        mpWindow = newwin(mYMax, mXMax, mYPos, mXPos);
        keypad(mpWindow, 1);
        wbkgd(mpWindow, COLOR_PAIR(TUI_CP_WIN));
    }

    box(mpWindow, 0, 0);
    std::string s;
    if (mJobInfo && msg.empty()) {
        s = "Job View";
        mvwaddch(mpWindow, 0, (mXMax - s.size()) / 2 - 1, ACS_RTEE);
        waddch(mpWindow, ' ');
        wattron(mpWindow, COLOR_PAIR(TUI_CP_HEAD));
        wprintw(mpWindow, "%s", s.data());
        wattroff(mpWindow, COLOR_PAIR(TUI_CP_HEAD));
        waddch(mpWindow, ' ');
        waddch(mpWindow, ACS_LTEE);
        mvwprintw(mpWindow, mYMax / 2, (mXMax - 22) / 2, "                  ");
    }
    else {
        mvwprintw(mpWindow, mYMax / 2, (mXMax - 22) / 2, "%s", msg.data());
    }

    int i = 0;
    attrset(COLOR_PAIR(TUI_CP_SHADOW));
    for (i = 0; i < mYMax; i++) mvaddch(mYPos + i + 1, mXPos + mXMax, ' ');
    for (i = 0; i < mXMax; i++) mvaddch(mYPos + mYMax, mXPos + i + 1, ' ');
    attroff(COLOR_PAIR(TUI_CP_SHADOW));

    if (!mJobInfo) {
        wrefresh(mpWindow);
        refresh();
        return;
    }

    int      pb_y  = 2;
    int      pb_x  = 4;
    uint32_t index = 0;
    uint32_t j     = 0;
    uint32_t pb_w  = mXMax - pb_x * 2;

    uint32_t pPending  = map(pb_w, mJobInfo->total(), mJobInfo->pending() + mJobInfo->assigned());
    uint32_t pFailed   = map(pb_w, mJobInfo->total(), mJobInfo->failed());
    uint32_t pFinished = map(pb_w, mJobInfo->total(), mJobInfo->done());
    if (mJobInfo->done() && mJobInfo->failed() && mJobInfo->total() == mJobInfo->done() + mJobInfo->failed())
        pFinished++;
    uint32_t pRunning = map(pb_w, mJobInfo->total(), mJobInfo->running());
    uint32_t pEmpty   = pb_w + pPending - pFinished - pFailed - pRunning;

    // fix for last black rect in progress bar
    // also fix when all jobs are running (https://gitlab.openbrain.sk/salsa/salsa/issues/113)
    if (!pPending && (pFinished + pFailed) && pRunning) pRunning++;

    s = fmt::format("Manager : {}", mCurJobManager);
    if (s.size() > (uint32_t)(mXMax - 8 - 3)) {
        s = s.substr(0, mXMax - 8 - 3);
        s += "...";
    }
    mvwprintw(mpWindow, pb_y++, pb_x, "%s", s.data());

    s = fmt::format("Job     : {}", mCurJobID);
    if (s.size() > (uint32_t)(mXMax - 8 - 3)) {
        s = s.substr(0, mXMax - 8 - 3);
        s += "...";
    }
    mvwprintw(mpWindow, pb_y++, pb_x, "%s", s.data());

    mvwprintw(mpWindow, mYMax - 2, mXMax - 38, "%s", fmt::format("Last update : {}", TuiUtils::current_time()).data());

    pb_y += 1;
    wmove(mpWindow, pb_y, pb_x);
    // wattrset(mpWindow, COLOR_PAIR(TUI_CP_PEN));
    // for (j = 0; j < pPending; j++, index++) waddch(mpWindow, ' ');

    wattrset(mpWindow, COLOR_PAIR(TUI_CP_ERR));
    for (j = 0; j < pFailed; j++, index++) waddch(mpWindow, ' ');

    wattrset(mpWindow, COLOR_PAIR(TUI_CP_OK));
    for (j = 0; j < pFinished; j++, index++) waddch(mpWindow, ' ');

    wattrset(mpWindow, COLOR_PAIR(TUI_CP_RUN));
    for (j = 0; j < pRunning; j++, index++) waddch(mpWindow, ' ');

    wattrset(mpWindow, COLOR_PAIR(TUI_CP_SHADOW));
    // wattrset(mpWindow, COLOR_PAIR(TUI_CP_WIN));
    for (j = 0; j < pb_w - pEmpty - 1; j++, index++) {
        if (index == pb_w) break;
        waddch(mpWindow, ' ');
    }
    wattrset(mpWindow, COLOR_PAIR(TUI_CP_WIN));

    pb_y++;
    std::string f = "{:>10} ({:>{}.{}f}%%)";
    stat_line(++pb_y, pb_x, "Total      : ",
              fmt::format(f, mJobInfo->total(),
                          (double)(mJobInfo->done() + mJobInfo->failed()) / mJobInfo->total() * 100, 5, 1),
              COLOR_PAIR(TUI_CP_TOT));
    stat_line(++pb_y, pb_x, "Pending    : ",
              fmt::format(f, mJobInfo->pending() + mJobInfo->assigned(),
                          (double)(mJobInfo->pending() + mJobInfo->assigned()) / mJobInfo->total() * 100, 5, 1),
              COLOR_PAIR(TUI_CP_WIN_REV));
    stat_line(++pb_y, pb_x, "Running    : ",
              fmt::format(f, mJobInfo->running(), (double)mJobInfo->running() / mJobInfo->total() * 100, 5, 1),
              COLOR_PAIR(TUI_CP_RUN));
    stat_line(++pb_y, pb_x, "Successful : ",
              fmt::format(f, mJobInfo->done(), (double)mJobInfo->done() / mJobInfo->total() * 100, 5, 1),
              COLOR_PAIR(TUI_CP_OK));
    stat_line(++pb_y, pb_x, "Failed     : ",
              fmt::format(f, mJobInfo->failed(), (double)mJobInfo->failed() / mJobInfo->total() * 100, 5, 1),
              COLOR_PAIR(TUI_CP_ERR));

    wrefresh(mpWindow);
    refresh();
}

void JobWindow::stat_line(int yPos, int xPos, std::string key, std::string value, int color)
{
    ///
    /// Prints key (bold) and value (normal)
    ///

    wattron(mpWindow, A_BOLD);
    mvwprintw(mpWindow, yPos, xPos, "%s", key.data());
    wattroff(mpWindow, A_BOLD);
    wattron(mpWindow, A_REVERSE | color);
    wprintw(mpWindow, "%s", value.data());
    wattroff(mpWindow, A_REVERSE | color);
}

} // namespace Salsa
