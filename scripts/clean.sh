#!/bin/bash

SALSA_DIR="$(dirname $(dirname $(readlink -m $0)))"

for d in bin lib lib64 share; do
  [ -d $SALSA_DIR/$d ] && rm -rf $SALSA_DIR/$d
done
