#!/bin/bash

which clang-format &>/dev/null || {
    echo "clang-format not found! Please install it with 'dnf install -y clang'"
    exit 1
}

[[ -f .clang-format.bak ]] && mv .clang-format.bak .clang-format
[[ ! -f .clang-format ]] && { echo "Missing .clang-format"; exit 2; }

for DIR in $@
do
	echo "--> $DIR"
	pushd $DIR

	mkdir -p bak
	cp *.[ch]* bak

	for FILE in *.[ch]*
	do
		echo $FILE
		clang-format $FILE > $FILE.FMT
		mv $FILE.FMT $FILE
	done
	popd
done

exit 0
