#!/bin/bash

for FILE in $@
do
	[[ ! -f $FILE ]] && break;

	echo $FILE

	mv $FILE ${FILE}.tmp

	echo '#pragma clang diagnostic push' >> ${FILE}
	echo '#pragma clang diagnostic ignored "-Weverything"' >> ${FILE}
	cat ${FILE}.tmp >> ${FILE}
	echo '#pragma clang diagnostic pop' >> ${FILE}

	rm ${FILE}.tmp
done
