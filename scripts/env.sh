#!/bin/bash

SALSA_DIR="$(dirname $(dirname $(readlink -m ${BASH_ARGV[0]})))"

export PATH="$SALSA_DIR/bin:$SALSA_DIR/scripts:$PATH"
export LD_LIBRARY_PATH="$SALSA_DIR/lib:$SALSA_DIR/lib64:$LD_LIBRARY_PATH"
