#include <algorithm>
#include <cstdlib>
#include <vector>

#include "Job.hh"
#include "NodeManagerZyre.hh"
#include "PublisherZmq.hh"
#include "NodeZyre.hh"
using namespace fmt::literals;

namespace Salsa {
NodeZyre::NodeZyre(std::string name)
    : Node(name)
    , ActorZmq()
{
    ///
    /// Constructor
    ///
}

NodeZyre::~NodeZyre()
{
    ///
    /// Destructor
    ///

    SPD_TRACE("### Destroy NodeZyre [{}] ###", mpNodeInfo->name());

    // clearing socket
    mSockets.clear();

    for (auto pSocket : mZmqSockets) {
        zsock_destroy(&pSocket);
    }

    if (mpNodeManager) {
        delete mpNodeManager;
        mpNodeManager = nullptr;
    }
}

int NodeZyre::init()
{
    ///
    /// Init
    ///

    SPD_TRACE("Salsa::NodeZyre::init()<-");

    /// important for signal handling
    ActorZmq::init();

    if (!mpPoller) {
        return 1;
    }

    if (mpNodeManager == nullptr) {
        mpNodeManager = new NodeManagerZyre(this);
        mpNodeManager->clusterAlias(mClusterAlias);

        if (!mType.compare("FEEDER") && !mpNodeManager->publisher()) {
            char * pPubUrl = getenv("SALSA_PUB_URL");
            if (pPubUrl) {
                mJobInfoBrokerUrl = pPubUrl;
            }
            // SPD_INFO("JobInfo broker url [{}]", mJobInfoBrokerUrl);
            mpNodeManager->publisher(new PublisherZmq(mJobInfoBrokerUrl));
            SPD_INFO("[{}] will publish to url=[{}]", mType, mJobInfoBrokerUrl);
        }
        char * pTimeout = getenv("SALSA_FINISHED_JOB_TIMEOUT");
        if (pTimeout) {
            mpNodeManager->finishedJobTimeout(atol(pTimeout));
        }
        char * pCheckTimeout = getenv("SALSA_FINISHED_JOB_CHECK_TIMEOUT");
        if (pCheckTimeout) {
            mJobCheckTimeout = atoi(pCheckTimeout);
        }
    }

    for (auto socket : mSockets) {
        mpPoller->add(socket.get());

        if (socket->header("X-SALSA-NODE-TYPE") == "CONSUMER")
            mpNodeManager->addConsumer(zyre_uuid(socket->zyre()), socket);

        if (socket->header("X-SALSA-NODE-TYPE") == "FEEDER") {
            mpNodeManager->addFeeder(zyre_uuid(socket->zyre()), socket);

            auto f = mpNodeManager->feeder(zyre_uuid(socket->zyre()));
            f->nodeInfo()->set_name(mJobInfoGroupName);
            f->nodeInfo()->set_hostname(hostname());
            f->nodeInfo()->set_submiturl(mSubmitClientUrl);
        }

        if (socket->header("X-SALSA-NODE-TYPE") == "WORKER") {
            mpNodeManager->addWorker(zyre_uuid(socket->zyre()), socket);
            auto w = mpNodeManager->worker(zyre_uuid(socket->zyre()));
            w->nodeInfo()->set_hostname(hostname());
        }
    }

    for (auto socket : mZmqSockets) {
        mpPoller->add(socket);
    }

    // mpNodeManager->print();
    SPD_TRACE("Salsa::NodeZyre::init()->");
    return 0;
}

int NodeZyre::exec()
{
    ///
    /// Exec
    ///

    SPD_TRACE("Salsa::NodeZyre::exec()<-");

    void *                      pPointer = nullptr;
    std::shared_ptr<SocketZyre> pSocket  = nullptr;
    zsock_t *                   pZmqSock = nullptr;
    int64_t                     cur_time;
    int64_t                     last_time = zclock_time();

    while (!mTerminated && !Salsa::Actor::interrupted()) {
        SPD_TRACE("Actor::wait()");
        pPointer = ActorZmq::wait();
        if (!pPointer) {
            break;
        }

        if (pPointer == mpPipe) {
            SPD_TRACE("Signal from pipe={}", static_cast<void *>(mpPipe));
            // We are not reacting to pipe events for now
            continue;
        }
        else {
            for (auto pZmqSocket : mZmqSockets) {
                SPD_TRACE("Searching ZMQ inSocket=[{}] zmqSocket[{}]", static_cast<void *>(pPointer),
                          static_cast<void *>(pZmqSocket));
                if (pZmqSocket == pPointer) {
                    pZmqSock = pZmqSocket;
                    break;
                }
            }

            if (pZmqSock) {
                SPD_TRACE("HANDLING zmq socket [{}]", static_cast<void *>(pZmqSock));
                zmsg_t * pMsg = zmsg_recv(pZmqSock);
                handleExternalZmq(pMsg, pZmqSock);
                zmsg_destroy(&pMsg);
                pZmqSock = nullptr;
                continue;
            }

            for (auto pNet : mSockets) {
                SPD_TRACE("Searching ZYRE socket={} in net={} socket={}", static_cast<void *>(pPointer),
                          static_cast<void *>(pNet.get()), static_cast<void *>(pNet->socket()));
                if (pNet && pNet->socket() && pPointer == pNet->socket()) {
                    pSocket = pNet;
                    break;
                }
            }

            if (!pSocket) {
                if (mpNodeManager->handleTaskPool(pPointer)) continue;
            }

            if (!pSocket) {
                SPD_ERROR("Socket comming from unknown network : socket={}", pPointer);
                continue;
            }

            // ==================================================

            Message * pMsg = pSocket->pull();
            if (!pMsg) {
                SPD_ERROR("Message from socket={} is null", pPointer);
                continue;
            }

            Message::EEventType type = pMsg->event();
            SPD_TRACE("Salsa::NodeZyre::exec() : Event from net [{}] pMsg [{}] type [{}]",
                      static_cast<void *>(pSocket.get()), static_cast<void *>(pMsg), static_cast<int>(type));

            bool                     doPublish    = true;
            bool                     forcePublish = false;
            bool                     cleanedJobs  = false;
            std::vector<std::string> values;

            if (type == Message::ENTER) {
                const char * pHeader =
                    zyre_event_header(static_cast<MessageZyre *>(pMsg)->zyreEvent(), "X-SALSA-NODE-TYPE");
                std::string snt;
                if (pHeader) snt = pHeader;

                SPD_DEBUG("[{}] ENTER uuid=[{}] node_type=[{}]", zyre_name(pSocket->zyre()), pMsg->uuid(), snt);
                mpNodeManager->onEnter(zyre_uuid(pSocket->zyre()), snt, pMsg, values);
                // doPublish = false;
            }
            else if (type == Message::EXIT) {
                SPD_TRACE("[{}] EXIT uuid=[{}]", zyre_name(pSocket->zyre()), pMsg->uuid());
                mpNodeManager->onExit(zyre_uuid(pSocket->zyre()), pMsg, values);
                // doPublish = false;
            }
            else if (type == Message::EVASIVE) {
                SPD_TRACE("[{}] EVASIVE uuid=[{}]", zyre_name(pSocket->zyre()), pMsg->uuid());
                doPublish = false;
            }
            else if (type == Message::WHISPER) {
                SPD_TRACE("[{}] WHISPER uuid=[{}]", zyre_name(pSocket->zyre()), pMsg->uuid());
                mpNodeManager->onWhisper(zyre_uuid(pSocket->zyre()), pMsg, values);
                // forcePublish = true;
            }

            cur_time = zclock_time();
            if ((cur_time - last_time) > mJobCheckTimeout) {
                forcePublish = true;
                doPublish    = true;
                cleanedJobs  = mpNodeManager->terminateFinishedJobs();
                last_time    = zclock_time();
            }

            if (doPublish) {
                SPD_TRACE("Trying to publishing from [{}] [force={}] [cleanedJobs={}] ...",
                          zyre_uuid(sockets()[0]->zyre()), forcePublish, cleanedJobs);
                bool wasPublished = mpNodeManager->publish(zyre_uuid(sockets()[0]->zyre()), forcePublish);
                if (wasPublished) last_time = zclock_time();
            }

            delete pMsg;
            pSocket  = nullptr;
            pZmqSock = nullptr;
        }

    } // END WHILE not terminated

    SPD_TRACE("Salsa::NodeZyre::exec()->");
    return 0;
}

int NodeZyre::finish()
{
    ///
    /// Finish
    ///

    SPD_TRACE("Salsa::NodeZyre::finish()<-");

    SPD_TRACE("Salsa::NodeZyre::finish()->");

    return 0;
}

void NodeZyre::addSocket(std::shared_ptr<SocketZyre> socket)
{
    ///
    /// Adding zyre socket
    ///

    if (socket) {
        auto pNode = std::make_shared<Node>(zyre_name(socket->zyre()), zyre_uuid(socket->zyre()));

        pNode->parent(shared_from_this());
        Node::add(pNode);
        mSockets.push_back(socket);
    }
}

std::vector<std::shared_ptr<SocketZyre>> NodeZyre::sockets() const
{
    ///
    /// Returns list of sockets
    ///
    return mSockets;
}

void NodeZyre::addSocket(zsock_t * socket)
{
    ///
    /// Returns list of zmq sockets
    ///
    if (socket) {
        mZmqSockets.push_back(socket);
    }
}

void NodeZyre::handleExternalZmq(zmsg_t * pMsg, zsock_t * pSocket)
{
    ///
    /// Handle message from external zmq
    /// Example: SUBMITTER
    ///

    zframe_t * pID  = zmsg_pop(pMsg);
    char *     pCmd = zmsg_popstr(pMsg);
    if (!strcmp(pCmd, "TASK")) {

        std::string logdir;
        if (getenv("SALSA_LOG_DIR")) logdir = getenv("SALSA_LOG_DIR");

        int        task_count   = 0;
        char *     pPayload_str = zmsg_popstr(pMsg);
        TaskInfo * pTaskInfo    = nullptr;
        while (pPayload_str) {
            std::string payload = pPayload_str;
            free(pPayload_str);

            pTaskInfo = new TaskInfo();
            {

                if (!pTaskInfo->ParseFromString(payload)) {
                    SPD_ERROR("Message does not contain ProtoBuf message!");
                    return;
                }
            }
            SPD_DEBUG("TASK [{}:{}] ", pTaskInfo->jobid(), pTaskInfo->taskid());
            if (task_count == 0) {
                if (!logdir.empty()) {
                    logdir = fmt::format("{}/{}", logdir, pTaskInfo->jobid());
                    if (!zsys_file_exists(logdir.data())) zsys_dir_create(logdir.data());
                }
            }
            if (pTaskInfo->logtargets_size() > 0 && pTaskInfo->logtargets()[0] == "default") {
                if (!logdir.empty())
                    pTaskInfo->add_logtargets(
                        fmt::format("file://{}/ndm-{:010d}.log", logdir.data(), pTaskInfo->taskid()));
            }
            mpNodeManager->addTask(pTaskInfo, "", "", Salsa::Job::pending);
            task_count++;
            pPayload_str = zmsg_popstr(pMsg);
        }

        Job * job = mpNodeManager->job(pTaskInfo->jobid());
        if (job) {
            if (job->submitterSocketID() == nullptr) job->submitterSocketID(zframe_dup(pID));
            /// Doing special and most used case zmsSockets==1
            if (mZmqSockets.size() == 1) {
                job->submitterSocketIndex(0);
            }
            else {
                int i = 0;
                for (auto s : mZmqSockets) {
                    if (s == pSocket) {
                        job->submitterSocketIndex(i);
                        break;
                    }
                    i++;
                }
            }

            // SPD_INFO("{} {}", job->submitterSocketIndex(), job->submitterSocketID());
        }

        zmsg_t * pMsgOut = zmsg_new();
        zmsg_add(pMsgOut, pID);
        zmsg_addstr(pMsgOut, "");
        zmsg_addstr(pMsgOut, "TASK_ADDED");
        zmsg_addstr(pMsgOut, fmt::format("{}", task_count).data());
        zmsg_addstr(pMsgOut, fmt::format("{}", logdir).data());
        zmsg_send(&pMsgOut, pSocket);
        zmsg_destroy(&pMsgOut);

        // zmsg_t * pMsgOut = zmsg_new();
        // zframe_destroy(&pID);
        // pID = static_cast<zframe_t *>(job->submitterSocketID());
        // zmsg_add(pMsgOut, pID);
        // zmsg_addstr(pMsgOut, "");
        // zmsg_addstr(pMsgOut, "TASK_ADDED");
        // zmsg_addstr(pMsgOut, fmt::format("{}", task_count).data());
        // // zmsg_send(&pMsgOut, pSocket);
        // zmsg_send(&pMsgOut, mZmqSockets[job->submitterSocketIndex()]);
        // zmsg_destroy(&pMsgOut);

        pID = nullptr;

        mpNodeManager->print();

        // delete pTaskInfo;
    }
    else if (!strcmp(pCmd, "AUTH")) {

        SPD_DEBUG("Checking AUTH ...");
        zmsg_t * pMsgOut = zmsg_new();
        zmsg_add(pMsgOut, pID);
        zmsg_addstr(pMsgOut, "");
        zmsg_addstr(pMsgOut, "AUTH");
        zmsg_addstr(pMsgOut, "OK");
        /// TODO redirector ID
        std::string rdr;
        rdr = zyre_uuid(sockets()[0]->zyre());
        zmsg_addstr(pMsgOut, rdr.data());
        zmsg_addstr(pMsgOut,
                    fmt::format("v{}.{}.{}-{}", salsa_VERSION_MAJOR(salsa_VERSION), salsa_VERSION_MINOR(salsa_VERSION),
                                salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK)
                        .data());
        zmsg_addstr(pMsgOut, mJobInfoClientUrl.data());
        zmsg_send(&pMsgOut, pSocket);
        SPD_DEBUG("Sent AUTH OK {} ...", static_cast<void *>(pSocket));
        pID = nullptr;
    }

    else if (!strcmp(pCmd, "JOB_DEL_ID")) {
        char *      pJobUUID_str = zmsg_popstr(pMsg);
        std::string jobUUID      = pJobUUID_str;
        free(pJobUUID_str);

        mpNodeManager->terminateJob(jobUUID);
        SPD_TRACE("Publishing to [{}] ...", zyre_uuid(sockets()[0]->zyre()));
        mpNodeManager->publish(zyre_uuid(sockets()[0]->zyre()), true);

        zmsg_t * pMsgOut = zmsg_new();
        zmsg_add(pMsgOut, pID);
        zmsg_addstr(pMsgOut, "");
        zmsg_addstr(pMsgOut, pCmd);
        zmsg_addstr(pMsgOut, "OK");
        zmsg_send(&pMsgOut, pSocket);
        SPD_DEBUG("Sent [{}] OK {} ...", pCmd, static_cast<void *>(pSocket));
    }
    else if (!strcmp(pCmd, "JOB_DEL_FINISHED")) {
        mpNodeManager->terminateAllJobs(true);
        SPD_TRACE("Publishing to [{}] ...", zyre_uuid(sockets()[0]->zyre()));
        mpNodeManager->publish(zyre_uuid(sockets()[0]->zyre()), true);

        zmsg_t * pMsgOut = zmsg_new();
        zmsg_add(pMsgOut, pID);
        zmsg_addstr(pMsgOut, "");
        zmsg_addstr(pMsgOut, pCmd);
        zmsg_addstr(pMsgOut, "OK");
        zmsg_send(&pMsgOut, pSocket);
        SPD_DEBUG("Sent [{}] OK {} ...", pCmd, static_cast<void *>(pSocket));
    }
    else if (!strcmp(pCmd, "JOB_DEL_ALL")) {
        mpNodeManager->terminateAllJobs();
        SPD_TRACE("Publishing to [{}] ...", zyre_uuid(sockets()[0]->zyre()));
        mpNodeManager->publish(zyre_uuid(sockets()[0]->zyre()), true);

        zmsg_t * pMsgOut = zmsg_new();
        zmsg_add(pMsgOut, pID);
        zmsg_addstr(pMsgOut, "");
        zmsg_addstr(pMsgOut, pCmd);
        zmsg_addstr(pMsgOut, "OK");
        zmsg_send(&pMsgOut, pSocket);
        SPD_DEBUG("Sent [{}] OK {} ...", pCmd, static_cast<void *>(pSocket));
    }
    else if (!strcmp(pCmd, "WORKER_COUNT")) {

        zmsg_t * pMsgOut = zmsg_new();
        zmsg_add(pMsgOut, pID);
        zmsg_addstr(pMsgOut, "");
        zmsg_addstr(pMsgOut, pCmd);
        zmsg_addstr(pMsgOut, std::to_string(mpNodeManager->nSlots()).data());
        zmsg_send(&pMsgOut, pSocket);
        SPD_DEBUG("Sent [{}] OK {} ...", pCmd, static_cast<void *>(pSocket));
    }
    else {
        zmsg_t * pMsgOut = zmsg_new();
        zmsg_add(pMsgOut, pID);
        zmsg_addstr(pMsgOut, "");
        zmsg_addstr(pMsgOut, pCmd);
        zmsg_addstr(pMsgOut, "FAILED");
        zmsg_send(&pMsgOut, pSocket);
        SPD_DEBUG("Sent [{}] OK {} ...", pCmd, static_cast<void *>(pSocket));
    }
    free(pCmd);
    // if (pID) zframe_destroy(&pID);
}

} // namespace Salsa
