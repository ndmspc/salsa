#include "SocketZyre.hh"
#include "MessageZyre.hh"

namespace Salsa {
SocketZyre::SocketZyre(std::string name, std::map<std::string, std::string> headers)
    : Socket(), mpSocket(zyre_new(name.c_str()))
{
    ///
    /// Constructor with zyre name
    /// \param name Zyre name
    /// \param headers List of headers
    ///

    SPD_TRACE("Creating zyre socket name [{}]", name);

    for (auto const & header : headers) {
        SPD_DEBUG("[{}] header : [{}] [{}]", name, header.first, header.second);
        zyre_set_header(mpSocket, header.first.c_str(), "%s", header.second.c_str());
    }
    mHeaders = headers;
}
SocketZyre::~SocketZyre()
{
    ///
    /// Destructor
    ///

    disconnect();

    zyre_destroy(&mpSocket);
}

int SocketZyre::connect()
{
    ///
    /// Connect zyre to the network
    ///

    SPD_TRACE("Starting zyre socket name [{}]", zyre_name(mpSocket));
    return zyre_start(mpSocket);
}

int SocketZyre::disconnect()
{
    ///
    /// Disconnect zyre from network
    ///

    if (mpSocket) {
        SPD_TRACE("Stopping zyre socket name [{}]", zyre_name(mpSocket));
        zyre_stop(mpSocket);
    }
    return 0;
}

Message * SocketZyre::pull()
{
    ///
    /// Pull message from the network
    ///

    zyre_event_t * pEvent = zyre_event_new(mpSocket);
    return new MessageZyre(pEvent);
}

int SocketZyre::push(Message * pMsg)
{
    ///
    /// Push message to the network
    ///

    SPD_WARN("SocketZyre::push(Message *) not supported!");
    if (!pMsg) {
        return 1;
    }
    return 0;
}

int SocketZyre::push(std::string targetUUID, std::string payload)
{
    if (targetUUID.empty()) {
        SPD_ERROR("Target UUID is empty!");
    }

    if (payload.empty()) {
        SPD_ERROR("Payload is empty!");
    }

    return zyre_whispers(mpSocket, targetUUID.c_str(), "%s", payload.c_str());
}

} // namespace Salsa
