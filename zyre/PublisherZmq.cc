#include "PublisherZmq.hh"

namespace Salsa {
PublisherZmq::PublisherZmq(std::string url)
    : Publisher(url)
{
    ///
    /// Constructor
    ///

    mpSocket = zsock_new_pub(url.c_str());
}
PublisherZmq::~PublisherZmq()
{
    ///
    /// Destructor
    ///

    zsock_destroy(&mpSocket);
}

void PublisherZmq::publish(std::string id, std::string name, std::string data, bool forcePublish)
{
    ///
    /// Publish function
    ///

    if (!mpSocket) {
        SPD_ERROR("PublisherZmq::publish() mpSocket is null");
        return;
    }

    SPD_TRACE("PublisherZmq::publish() Sending msg sub [salsa:{}] id [{}] data [{}]", id, name, data);

    zmsg_t * msg = zmsg_new();
    zmsg_addstr(msg, fmt::format("salsa:{}", id).data()); // Later we change it to something meaningfull
    zmsg_addstr(msg, name.data());
    zmsg_addstr(msg, data.data());
    zmsg_addstr(msg, fmt::format("{}", forcePublish).data());
    zmsg_send(&msg, mpSocket);
}

} // namespace Salsa
