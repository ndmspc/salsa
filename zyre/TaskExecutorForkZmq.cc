#include "TaskExecutorForkZmq.hh"
#include "TaskState.hh"
namespace Salsa {
TaskExecutorForkZmq::TaskExecutorForkZmq(zactor_t * actor) : TaskExecutor(), mpZActor(actor)
{
    ///
    /// Constructor
    ///
}
TaskExecutorForkZmq::~TaskExecutorForkZmq()
{
    ///
    /// Destructor
    ///
    if (mpZActor) {
        zactor_destroy(&mpZActor);
    }
}
bool TaskExecutorForkZmq::run(std::string worker, std::string upstream)
{
    ///
    /// Run
    ///

    if (mpTaskState == nullptr) return false;

    if (pipe() == nullptr) return false;

    zmsg_t * pOutMsg = zmsg_new();
    zmsg_addstrf(pOutMsg, "%s", mpTaskState->task()->data().c_str());
    zmsg_addstrf(pOutMsg, "%d", mpTaskState->task()->clientid());
    zmsg_addstrf(pOutMsg, "%d", mpTaskState->task()->groupid());
    zmsg_addstr(pOutMsg, worker.c_str());
    zmsg_addstr(pOutMsg, upstream.c_str());
    zmsg_addstr(pOutMsg, mpTaskState->task()->jobid().c_str());
    for (int iPos = 0; iPos < mpTaskState->task()->logtargets_size(); iPos++) {
        if (iPos == 0) zmsg_addstrf(pOutMsg, "%s", "logs");
        zmsg_addstrf(pOutMsg, "%s", mpTaskState->task()->logtargets(iPos).c_str());
    }
    for (int iPos = 0; iPos < mpTaskState->task()->envs_size(); iPos++) {
        if (iPos == 0) zmsg_addstrf(pOutMsg, "%s", "envs");
        zmsg_addstrf(pOutMsg, "%s", mpTaskState->task()->envs(iPos).c_str());
    }

    zsock_send(pipe(), "m", pOutMsg);
    zmsg_destroy(&pOutMsg);

    return true;
}
void * TaskExecutorForkZmq::pipe() const
{
    ///
    /// Returns pipe
    ///
    return mpZActor;
}

bool TaskExecutorForkZmq::handlePipe(std::vector<std::string> & extra)
{
    ///
    /// Handle pipe
    ///

    zmsg_t * pMessage = zmsg_recv(pipe());
    if (zframe_streq(zmsg_first(pMessage), "$PID")) {
        char * pPidStr = zframe_strdup(zmsg_next(pMessage));

        uint32_t pid = static_cast<uint32_t>(strtoul(pPidStr, nullptr, 0));
        mpTaskState->pid(pid);
        // mpTaskState->state(TaskState::EState::running);
        std::string payload;
        mpTaskState->task()->SerializeToString(&payload);
        SPD_DEBUG("JOB [{}:{}] PID [{}] started", mpTaskState->task()->jobid(), mpTaskState->task()->taskid(), pPidStr);

        free(pPidStr);
    }
    else if (zframe_streq(zmsg_first(pMessage), "$EXIT")) {
        char *   pExitStatusStr = zframe_strdup(zmsg_next(pMessage));
        uint32_t exitStatus     = static_cast<uint32_t>(strtoul(pExitStatusStr, nullptr, 0));
        free(pExitStatusStr);
        mpTaskState->task()->set_returncode(exitStatus);

        SPD_DEBUG("JOB [{}:{}] PID [{}] finished with rc [{}] killed [{}]", mpTaskState->task()->jobid(),
                  mpTaskState->task()->taskid(), mpTaskState->pid(), mpTaskState->task()->returncode(),
                  mpTaskState->state() == TaskState::killed);

        // mpTaskState->state(TaskState::EState::idle);
        // mpTaskState->pid(0);

        // std::string payload;
        // mpTaskState->task()->SerializeToString(&payload);
        char * pWkUUID = zframe_strdup(zmsg_next(pMessage));
        extra.push_back(pWkUUID);
        char * pUpstream = zframe_strdup(zmsg_next(pMessage));
        extra.push_back(pUpstream);

        free(pWkUUID);
        free(pUpstream);
    }
    zmsg_destroy(&pMessage);

    return true;
}
} // namespace Salsa
