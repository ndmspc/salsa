#pragma once

#include "ActorZmq.hh"
#include "TaskExecutor.hh"

namespace Salsa {
///
/// \class TaskExecutorForkZmq
///
/// \brief TaskExecutorForkZmq class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class TaskExecutorForkZmq : public TaskExecutor {
public:
    TaskExecutorForkZmq(zactor_t * pActor = nullptr);
    virtual ~TaskExecutorForkZmq();

    virtual bool   run(std::string worker, std::string upstream);
    virtual void * pipe() const;
    virtual bool   handlePipe(std::vector<std::string> & extra);

protected:
    zactor_t * mpZActor = nullptr; ///< ZMQ Actor pointer
};
} // namespace Salsa
