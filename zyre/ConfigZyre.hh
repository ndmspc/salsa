#pragma once

#include "ActorZmq.hh"
#include "Config.hh"

namespace Salsa {
///
/// \class ConfigZyre
///
/// \brief Base ConfigZyre class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class ConfigZyre : public Config {
public:
    ConfigZyre();

    virtual ~ConfigZyre();

    std::shared_ptr<Salsa::Node> apply(std::vector<std::shared_ptr<Salsa::ActorZmq>> * targetActors);

private:
    void applyOptions(YAML::detail::iterator_value & src, YAML::Node & opt);
};
} // namespace Salsa
