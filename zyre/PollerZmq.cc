#include "PollerZmq.hh"
#include "ActorZmq.hh"

namespace Salsa {
PollerZmq::PollerZmq() : Poller(), mpPoller(zpoller_new(nullptr))
{
    ///
    /// Constructor
    ///
}

PollerZmq::~PollerZmq()
{
    ///
    /// Destructor
    ///

    SPD_TRACE("### Destroy PollerZmq ###");
    zpoller_destroy(&mpPoller);
    mpPoller = nullptr;
}

void PollerZmq::add(zsock_t * pSocket)
{
    ///
    /// Add Salsa socket to poller
    /// @param pSocket Zeromq socket
    ///

    zpoller_add(mpPoller, pSocket);
    SPD_TRACE("Adding socket (zsock_t*) {} to poller {}", static_cast<void *>(pSocket), static_cast<void *>(this));
}

void PollerZmq::add(zactor_t * pSocket)
{
    ///
    /// Add Salsa socket to poller
    /// @param pSocket Zeromq socket
    ///

    zpoller_add(mpPoller, pSocket);
    SPD_TRACE("Adding socket (zactor_t*) {} to poller {}", static_cast<void *>(pSocket), static_cast<void *>(this));
}

void PollerZmq::add(SocketZyre * pSocket)
{
    ///
    /// Add Salsa socket to poller
    /// @param pSocket Salsa socket
    ///

    zpoller_add(mpPoller, pSocket->socket());
    SPD_TRACE("Adding socket (Salsa::Socket*){} to poller {}", static_cast<void *>(pSocket), static_cast<void *>(this));
}

void * PollerZmq::wait(int timeout)
{
    ///
    /// Wait for socket
    /// @param timeout Timeout of poller
    ///
    // What the hell Clang?
    // PollerZmq.cxx|53 col 8 warning| '@param' command used in a comment that is not attached to a
    // function declaration [-Wdocumentation]
    //
    // Alright. I'm gonna stop you right there. For your information: I just added these comments
    // and the warning just kinda disappeared. What. The. ...

    void * pSocket = zpoller_wait(mpPoller, timeout);
    SPD_TRACE("wait(timeout [{}]) => socket [{}]", timeout, static_cast<void *>(pSocket));
    return pSocket;
}

} // namespace Salsa
