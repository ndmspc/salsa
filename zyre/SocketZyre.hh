#pragma once

#include <zyre.h>

#include "Node.hh"
#include "Socket.hh"

namespace Salsa {
///
/// \class SocketZyre
///
/// \brief Salsa zyre socket class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class SocketZyre : public Socket {
public:
    SocketZyre(std::string name = "no_name", std::map<std::string, std::string> headers = {});
    virtual ~SocketZyre();

    virtual int connect() final;
    virtual int disconnect() final;

    virtual Message * pull();
    virtual int       push(Message *);
    /// Push message to UUID
    int push(std::string, std::string);

    /// Set zyre port
    void port(int newPort)
    {
        mPort = newPort;
        zyre_set_port(mpSocket, newPort);
    }

    /// Get zyre port
    int port() const { return mPort; }

    /// Returns zyre pointer
    virtual zyre_t * zyre() const { return mpSocket; }

    /// Returns zyre socket pointer
    virtual zsock_t * socket() const { return zyre_socket(mpSocket); }
    /// Returns value for key from header
    std::string header(const char * pKey) const { return mHeaders.find(pKey)->second; }

private:
    zyre_t *                           mpSocket = nullptr; ///< Zyre instance
    int                                mPort    = 0;       ///< Port for Zyre
    std::map<std::string, std::string> mHeaders = {};      ///< List of headers
};

} // namespace Salsa
