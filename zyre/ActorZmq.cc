#include "ActorZmq.hh"

namespace Salsa {
ActorZmq::ActorZmq() : Actor()
{
    ///
    /// Constructor
    ///

    mpPoller = new PollerZmq();
}
ActorZmq::~ActorZmq()
{
    ///
    /// Destructor
    ///

    // Why no smart pointer? Because this one is fully managed within current class.
    delete mpPoller;
}

void ActorZmq::SalsaActorFn(zsock_t * pPipe, void * pArg)
{
    ///
    /// Actor function engine
    ///

    zsock_signal(pPipe, 0);
    ActorZmq * pActor = static_cast<Salsa::ActorZmq *>(pArg);
    pActor->pipe(pPipe);

    SPD_TRACE("SalsaActorFn::init() <-");
    int ret = 0;
    if ((ret = pActor->init())) {
        SPD_ERROR("init() failed! [{}]", ret);
        return;
    }
    SPD_TRACE("SalsaActorFn::init()->");

    if (!Salsa::Actor::interrupted() && !pActor->terminated()) {
        SPD_TRACE("SalsaActorFn::exec() <-");
        if ((ret = pActor->exec())) {
            SPD_ERROR("exec() failed! [{}]", ret);
            return;
        }
        SPD_TRACE("SalsaActorFn::exec() ->");
    }

    SPD_TRACE("SalsaActorFn::finish() <-");
    if ((ret = pActor->finish())) {
        SPD_ERROR("finish() failed! [{}]", ret);
        return;
    }
    SPD_TRACE("SalsaActorFn::finish() ->");
}

void ActorZmq::SalsaActorForkFn(zsock_t * pPipe, void *)
{
    // _/(;_;)\_
    // AT+OK
    zsock_signal(pPipe, 0);

    pid_t pid = 0;

    // PA will be running indefinitely, until interrupted
    while (true) {
        zmsg_t * pReceived = zmsg_recv(pPipe);
        if (!pReceived) {
            SPD_WARN("PA: pReceived == <nullptr> (Exec interrupted)");
            break;
        }

        // read first frame
        zframe_t * pFrame = zmsg_first(pReceived);

        // Terminate on signal
        if (zframe_streq(pFrame, "$TERM")) {
            SPD_TRACE("PA: Terminate received");
            zmsg_destroy(&pReceived);
            break; // TERMINATE persistent actor
        }
        else {
            // GET command from message
            char * pCommand = zframe_strdup(pFrame);
            SPD_TRACE("PA: got Command [{}]", pCommand);
            pFrame = zmsg_next(pReceived);

            // GET UID from message
            char * pUID = zframe_strdup(pFrame);
            SPD_TRACE("PA: got UID [{}]", pUID);
            pFrame = zmsg_next(pReceived);

            // GET GID from message
            char * pGID = zframe_strdup(pFrame);
            SPD_TRACE("PA: got GID [{}]", pGID);
            pFrame = zmsg_next(pReceived);

            // GET Upsream uuid from message
            char * pWorker = zframe_strdup(pFrame);
            SPD_TRACE("PA: got Woker [{}]", pWorker);
            pFrame = zmsg_next(pReceived);

            // GET Upsream uuid from message
            char * pUpsream = zframe_strdup(pFrame);
            SPD_TRACE("PA: got Upstream [{}]", pUpsream);
            pFrame = zmsg_next(pReceived);

            // GET Client uuid from message
            char * pClient = zframe_strdup(pFrame);
            SPD_TRACE("PA: got Client [{}]", pClient);
            pFrame = zmsg_next(pReceived);

            std::string pMessage_str;
            std::string pLoop_str;
            // GET targets from message
            Salsa::Log log;
            if (pFrame) {

                char * pLoop = zframe_strdup(pFrame);
                pLoop_str    = pLoop;
                zstr_free(&pLoop);
                SPD_TRACE("PA: got str logs [{}]", pLoop);

                std::string pMessage_str;
                if (pLoop_str == "logs") {
                    while ((pFrame = zmsg_next(pReceived)) != nullptr) {
                        char * pMessage = zframe_strdup(pFrame);
                        pMessage_str    = pMessage;
                        if (pMessage_str == "envs") {
                            pLoop_str = "envs";
                            break;
                        }
                        SPD_TRACE("PA: Adding log target [{}]", pMessage);
                        log.add(pMessage);
                        zstr_free(&pMessage);
                    }
                    //// if targets are empty (why?), fallback to console output
                    // if (log.empty())
                    //	log.add("");
                }
            }

            std::vector<std::string> envs;
            if (pLoop_str == "envs") {
                while ((pFrame = zmsg_next(pReceived)) != nullptr) {
                    char * pMessage = zframe_strdup(pFrame);
                    SPD_TRACE("PA: Adding env  [{}]", pMessage);
                    envs.push_back(pMessage);
                    zstr_free(&pMessage);
                }
            }
            char * envp[envs.size()];
            int    i = 0;
            for (auto s : envs) {
                char * cstr = new char[s.length() + 1];
                strcpy(cstr, s.c_str());
                // do stuff
                envp[i] = cstr;
                // delete[] cstr;
                i++;
            }
            envp[i] = NULL;

            // Destroy message and go on with your life
            SPD_TRACE("PA: Destroying message [{}]", static_cast<void *>(pReceived));
            zmsg_destroy(&pReceived);

            // Initialization DONE ---------------------------------------------

            SPD_TRACE("PA: Creating logger");
            log.create();
            log.spd()->info("Running [{}]", pCommand);

            SPD_TRACE("PA: Waiting for pipes...");
            int pipefd[2];
            if (pipe2(pipefd, O_NONBLOCK)) {
                SPD_ERROR("FAILED to receive pipes!"); // TODO Inform manager about pipe failure ?
            }
            SPD_TRACE("PA: Got pipes [{}, {}]", pipefd[0], pipefd[1]);

            // = = = = = = = = = = FORK = = = = = = = = = =
            pid = fork();
            if (pid == 0) {

                // TODO this needs to be improved if we cannot set UID
                // TODO Check if uid is equal to process uid (in non-root case)
                if (getuid() == 0) {
                    SPD_TRACE("PA Child: uid [{}]->[{}] guid [{}]->[{}]", getuid(), pUID, getgid(), pGID);
                    if (setgid(atoi(pGID)) == -1) {
                        SPD_ERROR("Problem setting GUI to process !!! ");
                        return;
                    }
                    if (setuid(atoi(pUID)) == -1) {
                        SPD_ERROR("Problem setting UID to process !!! ");
                        return;
                    }

                    SPD_TRACE("PA Child: uid [{}] guid [{}]", getuid(), getgid());
                }

                SPD_TRACE("PA Child: Running command [{}]", pCommand);
                // FORK Child handler
                unsigned int iCount    = 0;
                char **      ppCommand = nullptr;
                char *       tmp       = std::strtok(pCommand, " ");

                SPD_TRACE("PA Child: Tokenizing");
                do {
                    // iCount + 2 because 1) iCounter an iterator and 2) we need nullptr at the end
                    ppCommand           = static_cast<char **>(realloc(ppCommand, (iCount + 2) * sizeof(char **)));
                    ppCommand[iCount++] = strdup(tmp);
                    tmp                 = std::strtok(nullptr, " ");
                } while (tmp);
                ppCommand[iCount] = nullptr;

                // SPD_TRACE("PA Child: Sleeping for 100ms");
                // std::this_thread::sleep_for(std::chrono::milliseconds(100));

                SPD_TRACE("PA Child: Configuring pipes");

                // After these lines you'll be unable to log anything to console, so don't even
                // try. It's literally a waste of time.
                close(pipefd[0]);
                dup2(pipefd[1], STDOUT_FILENO);
                dup2(pipefd[1], STDERR_FILENO);
                close(pipefd[1]);

                if (execvpe(ppCommand[0], ppCommand, envp) == -1) {
                    // int const err = errno;
                    // SPD_ERROR("PA failed to execute command! Error: [{}]", strerror(err));
                    // *facepalm*
                    exit(127);
                }
            }
            else if (pid > 0) {
                // FORK Parent handler
                // Send PID to parent
                SPD_TRACE("PA Parent: Sending PID [{}] to parent", pid);
                {
                    zmsg_t * pTx = zmsg_new();
                    zmsg_addstr(pTx, "$PID");
                    zmsg_addstrf(pTx, "%d", pid);
                    zmsg_addstr(pTx, pUpsream);
                    zmsg_addstr(pTx, pClient);
                    zsock_send(pPipe, "m", pTx);
                    zmsg_destroy(&pTx);
                }

                int stat = -1;
                close(pipefd[1]);

                log.fd(pipefd[0]);
                zactor_t * pWatcherActor = zactor_new(actorProcwaitSupport_, &log);

                SPD_TRACE("PA Parent: Running command...");
                // Read from pipe until child dies
                while (true) {
                    waitpid(pid, &stat, WUNTRACED);
                    if (WIFEXITED(stat) || WIFSIGNALED(stat)) {
                        zstr_sendf(pWatcherActor, "$EXIT");
                        break;
                    }
                }

                zactor_destroy(&pWatcherActor);

                close(pipefd[0]);
                int rc = WEXITSTATUS(stat);
                // In case of kill -9 : returning 137
                if (stat == 9) rc = 137;

                SPD_TRACE("PA Parent: Exit [{}] rc [{}]", stat, rc);
                {

                    zmsg_t * pTx = zmsg_new();
                    zmsg_addstr(pTx, "$EXIT");
                    zmsg_addstrf(pTx, "%d", rc);
                    zmsg_addstr(pTx, pWorker);
                    zmsg_addstr(pTx, pUpsream);
                    zmsg_addstr(pTx, pClient);
                    zsock_send(pPipe, "m", pTx);
                    zmsg_destroy(&pTx);
                }
                log.spd()->info("Process exited with status [{}]", stat);
            }
            else {
                // FORK Failed handler
                SPD_ERROR("PA Parent: fork() failure!");
                {
                    zmsg_t * pTx = zmsg_new();
                    zmsg_addstr(pTx, "$FORKFAIL");
                    zsock_send(pPipe, "m", pTx);
                    zmsg_destroy(&pTx);
                }
            } // END FORK handling

            free(pCommand);
            free(pUID);
            free(pGID);
            free(pWorker);
            free(pUpsream);
            free(pClient);

        } // END execute command
    }     // END while (true)

    SPD_TRACE("PA: Terminating persistent actor");
    return;
}

void ActorZmq::actorProcwaitSupport_(zsock_t * pPipe, void * pLogger)
{
    zsock_signal(pPipe, 0);

    Log & commandLogger = *(static_cast<Log *>(pLogger));
    //  3)                2)1)
    // Since this is kind of hard to read:
    // 1) Cast pLogger to Log *
    // 2) Get its value
    // 3) Set reference to it

    int       fd    = commandLogger.fd();
    const int LIMIT = PIPE_BUF;
    char      buffer[LIMIT + 1];
    std::memset(buffer, 0, LIMIT + 1);

    zpoller_t * pPoller = zpoller_new(nullptr);
    zpoller_add(pPoller, pPipe);
    zpoller_add(pPoller, &fd);

    while (true) {
        // Possible death race condition... I'm looking at you Valgrind
        void * pRecvSock = zpoller_wait(pPoller, -1);
        if (pRecvSock == pPipe) {
            char *      pMsg    = zstr_recv(pPipe);
            std::string recvMsg = pMsg;
            free(pMsg);
            if (recvMsg == "$EXIT") {
                break;
            }
        }
        else if (pRecvSock == &fd) {
            ssize_t readRet = read(fd, buffer, LIMIT);
            if (readRet > 0) {
                if (buffer[0] != '\0') {
                    commandLogger.write(buffer);
                    memset(buffer, 0, sizeof(buffer));
                }
            }
        }
    }

    zpoller_remove(pPoller, pPipe);
    zpoller_remove(pPoller, &fd);
    zpoller_destroy(&pPoller);
    return;
}

void ActorZmq::pipe(void * pPipe)
{
    ///
    /// Setting pipe socket
    ///

    SPD_TRACE("ActorZmq::pipe()<-");
    mpPipe = static_cast<zsock_t *>(pPipe);

    if (!mpPoller) {
        mpPoller = new PollerZmq();
    }

    if (mpPipe) {
        mpPoller->add(mpPipe);
    }
    SPD_TRACE("ActorZmq::pipe()->");
}

int ActorZmq::init()
{
    ///
    /// Init
    ///

    SPD_TRACE("ActorZmq::init()<-");
    // Setting up signal handler
    std::signal(SIGINT, Salsa::Actor::signalHandler);
    std::signal(SIGTERM, Salsa::Actor::signalHandler);

    SPD_TRACE("ActorZmq::init()->");
    return 0;
}

int ActorZmq::exec()
{
    ///
    /// Exec
    ///

    SPD_TRACE("ActorZmq::exec()<-");

    void * pEvent;
    while (!mTerminated && !Salsa::Actor::interrupted()) {
        pEvent = wait();
        if (pEvent) {
            // handle other socket
            SPD_WARN("ActorZmq::exec() : Other socket from ActorZmq class");
        }
    }

    SPD_TRACE("ActorZmq::exec() : Salsa::interrupted() [{}]", Salsa::Actor::interrupted());
    SPD_TRACE("ActorZmq::exec()->");
    return 0;
}

int ActorZmq::finish()
{
    ///
    /// Finish
    ///

    SPD_TRACE("ActorZmq::finish()<-");
    SPD_TRACE("ActorZmq::finish()->");
    return 0;
}

void * ActorZmq::wait()
{
    ///
    /// Waiting for event
    ///
    if (!mpPoller) {
        SPD_ERROR("Poller is nullptr!");
        return nullptr;
    }

    void * pEvent = mpPoller->wait(mTimeout);
    SPD_TRACE("ActorZmq::exec(): pEvent [{}] mpPipe [{}]", static_cast<void *>(pEvent), static_cast<void *>(mpPipe));

    if (mpPipe && pEvent == mpPipe) {
        zmsg_t * pMsg = zmsg_recv(mpPipe);
        if (!pMsg) {
            return nullptr;
        }

        char * pCommand = zmsg_popstr(pMsg);
        zmsg_destroy(&pMsg);
        if (streq(pCommand, "$TERM")) {
            SPD_TRACE("ActorZmq::exec(): received $TERM");
            mTerminated = true;
        }
        else {
            SPD_ERROR("ActorZmq::exec(): invalid message to actor msg: [{}]", pCommand);
            assert(false); // We should __not__ use assert here, because it's only used to debug...
        }
        zstr_free(&pCommand);
    }
    else {
        if (zpoller_expired(mpPoller->poller())) {
            SPD_TRACE("ActorZmq::exec(): Poller expired timeout [{}]...", mTimeout);
        }
        else if (zpoller_terminated(mpPoller->poller())) {
            SPD_TRACE("ActorZmq::exec(): Poller terminated ...");
            mTerminated = true;
        }
        else {
            return pEvent;
        }
    }

    return pEvent;
}

zpoller_t * ActorZmq::poller() const
{
    ///
    /// Returns zpoller
    ///
    return mpPoller->poller();
}
PollerZmq * ActorZmq::pollerZmq() const
{
    ///
    /// Returns PollerZmq
    ///
    return mpPoller;
}
} // namespace Salsa
