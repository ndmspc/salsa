#pragma once

#include "Poller.hh"
#include "SocketZyre.hh"

namespace Salsa {
///
/// \class PollerZmq
///
/// \brief salsa node class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class PollerZmq : public Poller {
public:
    PollerZmq();
    virtual ~PollerZmq();

    virtual void   add(SocketZyre * pSocket);
    virtual void   add(zsock_t * pSocket);
    virtual void   add(zactor_t * pSocket);
    virtual void * wait(int timeout = -1);

    /// Returns Poller
    zpoller_t * poller() const { return mpPoller; }

private:
    zpoller_t * mpPoller; ///< ZeroMQ poller
};

} // namespace Salsa
