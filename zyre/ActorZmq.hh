#pragma once

#include <czmq.h>

#include "Actor.hh"
#include "Log.hh"
#include "PollerZmq.hh"

namespace Salsa {
///
/// \class ActorZmq
///
/// \brief ZeroMQ implementation of salsa actor class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class ActorZmq : public Actor {
public:
    ActorZmq();
    virtual ~ActorZmq();

    virtual void pipe(void * pipe);
    virtual int  init();
    virtual int  exec();
    virtual int  finish();

    /// Sets timeout
    void timeout(int t) { mTimeout = t; }
    /// Returns timeout
    int timeout() const { return mTimeout; }

    virtual void * wait();

    zpoller_t * poller() const;
    PollerZmq * pollerZmq() const;

    /// Flag if actor should be terminated
    bool terminated() const { return mTerminated; }

    static void SalsaActorFn(zsock_t * pPipe, void * pArgv);

    /// Actor function with fork capability
    static void SalsaActorForkFn(zsock_t * pPipe, void * pArgv);

protected:
    zsock_t *   mpPipe      = nullptr; ///< Zmq pipe socket
    PollerZmq * mpPoller    = nullptr; ///< Internal poller
    bool        mTerminated = false;   ///< Flag if actor should be terminated
    int         mTimeout    = -1;      ///< Poller timeout

private:
    /// Support actor method (used for PID waiting)
    static void actorProcwaitSupport_(zsock_t * pipe, void * argv);
};

} // namespace Salsa
