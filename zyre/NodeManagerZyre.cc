#include "NodeManagerZyre.hh"
#include "MessageZyre.hh"
#include "PublisherZmq.hh"
#include "SocketZyre.hh"
#include "TaskExecutorFake.hh"
#include "TaskExecutorForkZmq.hh"
namespace Salsa {
NodeManagerZyre::NodeManagerZyre(NodeZyre * pNZ) : NodeManager(), mpNodeZyre(pNZ)
{
    ///
    /// Constructor
    ///
}
NodeManagerZyre::~NodeManagerZyre()
{
    ///
    /// Destructor
    ///
}

Socket * NodeManagerZyre::onEnter(std::string self, std::string fromType, Message * pMsg,
                                  std::vector<std::string> & values)
{
    ///
    /// On ENTER event
    ///

    Socket * pSocket = NodeManager::onEnter(self, fromType, pMsg, values);

    if (pSocket) {
        sendWhisper(pSocket, pMsg->uuid(), values);
    }

    return pSocket;
}

Socket * NodeManagerZyre::onExit(std::string self, Message * pMsg, std::vector<std::string> & values)
{
    ///
    /// On EXIT event
    ///

    Socket * pSocket = NodeManager::onExit(self, pMsg, values);

    if (pSocket) {
        sendWhisper(pSocket, pMsg->uuid(), values);
    }
    return pSocket;
}

Socket * NodeManagerZyre::onWhisper(std::string self, Message * pMsg, std::vector<std::string> & values)
{
    ///
    /// On WHISPER event
    ///

    Socket * pSocket = NodeManager::onWhisper(self, pMsg, values);

    if (pSocket) {
        sendWhisper(pSocket, pMsg->uuid(), values);
    }
    return pSocket;
}

void NodeManagerZyre::addTaskSlot()
{
    ///
    /// Reserve task slot
    ///

    if (mpTaskPool == nullptr) mpTaskPool = new TaskPool(this);

    if (getenv("SALSA_FAKE")) {
        SPD_DEBUG("Fake jobs");
        TaskExecutor * pExec  = new TaskExecutorFake(mpTaskPool);
        TaskState *    pState = new TaskState(pExec);
        pExec->taskState(pState);
        mpTaskPool->add(pState->executor()->pipe(), pState);
    }
    else {
        zactor_t *     pActor = zactor_new(Salsa::ActorZmq::SalsaActorForkFn, nullptr);
        TaskExecutor * pExec  = new TaskExecutorForkZmq(pActor);
        TaskState *    pState = new TaskState(pExec);
        pExec->taskState(pState);
        mpNodeZyre->pollerZmq()->add(static_cast<zactor_t *>(pState->executor()->pipe()));
        mpTaskPool->add(pState->executor()->pipe(), pState);
    }
}

void NodeManagerZyre::runTask(TaskState * pTaskState, std::string wk, std::string upstream)
{
    ///
    /// Run task
    ///

    SPD_TRACE("Task [{}:{}] wk [{}] upstream [{}]", pTaskState->task()->jobid(), pTaskState->task()->taskid(), wk,
              upstream);
    pTaskState->executor()->run(wk, upstream);
}

void NodeManagerZyre::resultTaskToExternal(Job * job, TaskInfo * task)
{
    ///
    /// Handle results of task and send info to external zmq
    ///
    SPD_TRACE("Sending results to external taskid [{}:{}] rc[{}]", task->jobid(), task->taskid(), task->returncode());

    zmsg_t * msg_out = zmsg_new();
    zmsg_add(msg_out, zframe_dup(static_cast<zframe_t *>(job->submitterSocketID())));
    // zmsg_add(msg_out, static_cast<zframe_t *>(job->submitterSocketID()));
    zmsg_addstr(msg_out, "");
    zmsg_addstr(msg_out, "TASK_RESULT");
    zmsg_addstr(msg_out, task->jobid().data());
    zmsg_addstr(msg_out, fmt::format("{}", task->taskid()).data());
    zmsg_addstr(msg_out, fmt::format("{}", task->returncode()).data());
    zmsg_send(&msg_out, mpNodeZyre->socketExternal(job->submitterSocketIndex()));
    zmsg_destroy(&msg_out);
}

bool NodeManagerZyre::handleTaskPool(void * pPipe)
{
    ///
    /// Handle task pool
    ///

    if (mpTaskPool == nullptr) return false;

    return mpTaskPool->handlePipe(pPipe);
}

bool NodeManagerZyre::sendWhisper(Socket * pSocket, std::string to, std::vector<std::string> & vec)
{
    ///
    /// Sends message via zyre whisper
    ///

    if (vec.size()) {
        SocketZyre * pSocketZyre = static_cast<SocketZyre *>(pSocket);
        zmsg_t *     pMsg        = nullptr;

        if (vec[0] == "&") vec.erase(vec.begin());
        for (auto str : vec) {
            if (pMsg == nullptr) {
                pMsg = zmsg_new();
            }
            if (str == "&") {
                zyre_whisper(pSocketZyre->zyre(), to.c_str(), &pMsg);
                zmsg_destroy(&pMsg);
                pMsg = nullptr;
            }
            else {
                zmsg_addstr(pMsg, str.c_str());
            }
        }
        zyre_whisper(pSocketZyre->zyre(), to.c_str(), &pMsg);
        zmsg_destroy(&pMsg);
        return true;
    }
    return false;
}

} // namespace Salsa
