#include "ConfigZyre.hh"
#include "NodeZyre.hh"
#include "PollerZmq.hh"
namespace Salsa {
ConfigZyre::ConfigZyre()
    : Config()
{
    ///
    /// Constructor
    ///
}
ConfigZyre::~ConfigZyre()
{
    ///
    /// Destructor
    ///
}

std::shared_ptr<Salsa::Node> ConfigZyre::apply(std::vector<std::shared_ptr<Salsa::ActorZmq>> * targetActors)
{
    ///
    /// Apply config
    ///

    if (!targetActors) return nullptr;

    std::shared_ptr<Salsa::Node> node = nullptr;

    if (!mConfig["salsa"]["type"] || mConfig["salsa"]["type"].as<std::string>() != "zyre") {
        SPD_ERROR("Salsa type is not [zyre] !!! ");
        return nullptr;
    }

    if (!mConfig["salsa"]["spec"]) {
        SPD_ERROR("Salsa spec was not found !!!");
        return nullptr;
    }

    SPD_TRACE("Caching hostname via zsys_hostname()");
    char *      pHostnameCStr_ = zsys_hostname();
    std::string hostname       = "nohostname";
    if (pHostnameCStr_) {
        hostname = pHostnameCStr_;
        free(pHostnameCStr_);
    }
    SPD_TRACE("Cached hostname [{}]", hostname);

    node       = std::make_shared<Salsa::Node>("SALSA");
    int nodeId = 0;

    for (auto spec : mConfig["salsa"]["spec"]) {

        if (!spec["nodes"]) {
            SPD_ERROR("Nodes array is missing for [{}] !!!", spec["name"].as<std::string>());
            return nullptr;
        }
        auto       name  = spec["name"].as<std::string>();
        bool       found = false;
        YAML::Node opt;

        if (mFilter.size() == 0) {
            found = true;
        }
        else {
            for (auto filter : mFilter) {
                if (name == filter.first) {
                    opt = filter.second;
                    if (opt["replicas"]) {
                        spec["replicas"] = opt["replicas"].as<int>();
                    }
                    found = true;
                }
            }
        }

        if (!found) continue;
        SPD_TRACE("name [{}]", name);
        int count = 1;

        if (spec["replicas"]) {
            count = spec["replicas"].as<int>();
        }
        for (int iCount = 0; iCount < count; iCount++) {
            // Create zyre node
            std::string                      zyreName  = fmt::format("{}:{}:{}:{}", name, hostname, getpid(), nodeId);
            std::shared_ptr<Salsa::NodeZyre> pNodeZyre = std::make_shared<Salsa::NodeZyre>(zyreName);

            if (spec["jobinfo"]["group"]) {
                SPD_INFO("Setting jobInfoGroup [{}] from config", spec["jobinfo"]["group"].as<std::string>());
                pNodeZyre->jobInfoGroupName(spec["jobinfo"]["group"].as<std::string>());
            }

            if (spec["jobinfo"]["broker"]["protocol"] && spec["jobinfo"]["broker"]["ip"] &&
                spec["jobinfo"]["broker"]["port"]) {
                std::string url = spec["jobinfo"]["broker"]["ip"].as<std::string>();
                if (url.empty()) url = hostname;
                url = fmt::format(">{}{}:{}", spec["jobinfo"]["broker"]["protocol"].as<std::string>(), url,
                                  spec["jobinfo"]["broker"]["port"].as<std::string>());
                SPD_INFO("Setting jobInfoBrokerUrl [{}] from config", url);
                pNodeZyre->jobInfoBrokerUrl(url);
            }
            if (spec["jobinfo"]["client"]["protocol"] && spec["jobinfo"]["client"]["ip"] &&
                spec["jobinfo"]["client"]["port"]) {
                std::string url = spec["jobinfo"]["client"]["ip"].as<std::string>();
                if (url.empty()) url = hostname;
                url = fmt::format(">{}{}:{}", spec["jobinfo"]["client"]["protocol"].as<std::string>(), url,
                                  spec["jobinfo"]["client"]["port"].as<std::string>());
                SPD_INFO("Broker url for client : {}", url);
                pNodeZyre->jobInfoClientUrl(url);
            }
            if (spec["timeout"]["poller"]) {
                pNodeZyre->timeout(spec["timeout"]["poller"].as<int>());
                SPD_INFO("Setting poller timeout [{}]", pNodeZyre->timeout());
            }

            if (spec["timeout"]["jobfinished"]) {
                SPD_INFO("Setting jobfinished timeout [{}]", spec["timeout"]["jobfinished"].as<std::string>());
                setenv("SALSA_FINISHED_JOB_TIMEOUT", spec["timeout"]["jobfinished"].as<std::string>().data(), true);
            }
            if (spec["timeout"]["jobcheck"]) {
                SPD_INFO("Setting jobcheck timeout [{}]", spec["timeout"]["jobcheck"].as<std::string>());
                setenv("SALSA_FINISHED_JOB_CHECK_TIMEOUT", spec["timeout"]["jobcheck"].as<std::string>().data(), true);
            }

            for (auto nodes : spec["nodes"]) {
                SPD_TRACE(" name [{}]", nodes["name"].as<std::string>());
                SPD_TRACE(" zyreName [{}]", zyreName);
                SPD_TRACE(" type  [{}]", nodes["type"].as<std::string>());

                applyOptions(nodes, opt);

                if (nodes["submit"]["protocol"] && nodes["submit"]["ip"] && nodes["submit"]["port"]) {
                    if (nodes["submit"]["ip"].as<std::string>() == "$all") nodes["submit"]["ip"] = "*";
                    std::string url =
                        fmt::format("{}://{}:{}", nodes["submit"]["protocol"].as<std::string>(),
                                    nodes["submit"]["ip"].as<std::string>(), nodes["submit"]["port"].as<int>());

                    SPD_INFO("Submit : url [{}]", url);
                    zsock_t * s = zsock_new_router(url.c_str());
                    if (s == nullptr) {
                        SPD_CRIT("Failed to bind submitter on '{}' !!!", url);
                        return nullptr;
                    }
                    pNodeZyre->addSocket(static_cast<zsock_t *>(s));

                    std::string submitter_hostname = hostname;
                    if (getenv("SALSA_SUBMIT_HOSTNAME")) submitter_hostname = getenv("SALSA_SUBMIT_HOSTNAME");
                    std::string submit_url_client =
                        fmt::format("{}://{}:{}", nodes["submit"]["protocol"].as<std::string>(), submitter_hostname,
                                    nodes["submit"]["port"].as<int>());

                    SPD_INFO("Submit url for client [{}] ...", submit_url_client);
                    pNodeZyre->submitClientUrl(submit_url_client);
                }
                else {
                    pNodeZyre->type(nodes["type"].as<std::string>());
                    if (nodes["alias"]) pNodeZyre->clusterAlias(nodes["alias"].as<std::string>());

                    SPD_INFO("clusterAlias[{}] ...", pNodeZyre->clusterAlias());
                    Salsa::NodeInfo * pNodeInfo = pNodeZyre->nodeInfo();
                    pNodeInfo->set_name(zyreName);
                    pNodeInfo->set_hostname(hostname);

                    std::map<std::string, std::string> headers;
                    headers.insert(
                        std::pair<std::string, std::string>("X-SALSA-NODE-TYPE", nodes["type"].as<std::string>()));

                    // Create zyre socket for node
                    std::shared_ptr<Salsa::SocketZyre> pSocketZyre =
                        std::make_shared<Salsa::SocketZyre>(zyreName, headers);

                    applyOptions(nodes, opt);

                    if (nodes["discovery"]["type"]) {
                        int         port;
                        std::string url, endpoint;
                        if (getenv("SALSA_ENDPOINT")) endpoint = getenv("SALSA_ENDPOINT");

                        std::string discoveryType = nodes["discovery"]["type"].as<std::string>();

                        if (discoveryType == "udp") {
                            port = 10000;
                            if (nodes["discovery"]["port"]) port = nodes["discovery"]["port"].as<int>();
                            SPD_TRACE("Using discovery [{}] via port [{}]...", discoveryType, port);
                            pSocketZyre->port(port); // Set socket's port
                        }
                        else if (discoveryType == "gossip") {
                            std::string p, i;
                            port = 20000;
                            if (nodes["discovery"]["protocol"]) p = nodes["discovery"]["protocol"].as<std::string>();
                            if (nodes["discovery"]["ip"]) i = nodes["discovery"]["ip"].as<std::string>();
                            if (nodes["discovery"]["port"]) port = nodes["discovery"]["port"].as<int>();

                            url = fmt::format("{}://{}:{}", p, i, port);

                            SPD_INFO("Using discovery : [{}] port [{}] endpoint [{}]...", discoveryType, url, endpoint);
                            if (!endpoint.empty()) zyre_set_endpoint(pSocketZyre->zyre(), "%s", endpoint.c_str());

                            if (nodes["discovery"]["bind"] && nodes["discovery"]["bind"].as<bool>() == true) {
                                if (i == "$all") i = "*";
                                url = fmt::format("{}://{}:{}", p, i, port);
                                zyre_gossip_bind(pSocketZyre->zyre(), "%s", url.c_str());
                            }
                            else {
                                zyre_gossip_connect(pSocketZyre->zyre(), "%s", url.c_str());
                            }

                            if (mConfig["salsa"]["options"]["evasive"]) {
                                SPD_INFO("Setting 'evasive' timeout to [{}] msec ...",
                                         mConfig["salsa"]["options"]["evasive"].as<int>());
                                zyre_set_evasive_timeout(pSocketZyre->zyre(),
                                                         mConfig["salsa"]["options"]["evasive"].as<int>());
                            }
                            if (mConfig["salsa"]["options"]["expired"]) {
                                SPD_INFO("Setting 'expired' timeout to [{}] msec ...",
                                         mConfig["salsa"]["options"]["expired"].as<int>());
                                zyre_set_expired_timeout(pSocketZyre->zyre(),
                                                         mConfig["salsa"]["options"]["expired"].as<int>());
                            }
                        }
                        else {
                            SPD_WARN("No discovery type specified !!!");
                        }

                        const char * zyreInterface = getenv("SALSA_INTERFACE");
                        if (zyreInterface && strcmp(zyreInterface, "")) {
                            SPD_INFO("Using SALSA_INTERFACE [{}]", zyreInterface);
                            zyre_set_interface(pSocketZyre->zyre(), zyreInterface);
                        }

                        pSocketZyre->connect();            // Connect to socket
                        pNodeZyre->addSocket(pSocketZyre); // Add socket to zyre node

                        SPD_INFO("Node : type [{}] name [{}] discovery type [{}] url [{}] port [{}] "
                                 "endpoint [{}]",
                                 name, zyreName, discoveryType, url, port, endpoint);

                        if (Salsa::Object::getConsoleOutput()->level() < static_cast<int>(spdlog::level::info)) {
                            zyre_print(pSocketZyre->zyre());
                        }
                    }
                    node->add(pNodeZyre);               // Add zyre node to main node
                    targetActors->push_back(pNodeZyre); // Add zyre node to actor index
                    nodeId++;
                }
            }
        }
        SPD_TRACE("---");
        nodeId++;
    }

    return node;
}

void ConfigZyre::applyOptions(YAML::detail::iterator_value & src, YAML::Node & opt)
{
    ///
    /// Apply options
    ///
    if (opt["type"] && src["discovery"]["type"]) {
        src["discovery"]["type"] = opt["type"].as<std::string>();
    }
    if (opt["protocol"] && src["discovery"]["protocol"]) {
        src["discovery"]["protocol"] = opt["protocol"].as<std::string>();
    }
    if (opt["ip"] && src["discovery"]["ip"]) {
        src["discovery"]["ip"] = opt["ip"].as<std::string>();
    }
    if (opt["port"] && src["discovery"]["port"]) {
        src["discovery"]["port"] = opt["port"].as<int>();
    }
    if (opt["submitport"] && src["submit"]["port"]) {
        src["submit"]["port"] = opt["submitport"].as<int>();
    }
}

} // namespace Salsa
