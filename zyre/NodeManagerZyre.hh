#pragma once

#include "NodeManager.hh"
#include "NodeZyre.hh"
#include "PollerZmq.hh"

namespace Salsa {
///
/// \class NodeManagerZyre
///
/// \brief NodeManagerZyre class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class NodeManagerZyre : public NodeManager {
public:
    NodeManagerZyre(NodeZyre * pNodeZyre);
    virtual ~NodeManagerZyre();

    virtual void resultTaskToExternal(Job * job, TaskInfo * task);

    virtual Socket * onEnter(std::string self, std::string fromType, Message * pMsg, std::vector<std::string> & out);
    virtual Socket * onExit(std::string self, Message * pMsg, std::vector<std::string> & out);
    virtual Socket * onWhisper(std::string self, Message * pMsg, std::vector<std::string> & out);

    virtual bool handleTaskPool(void * pPool);
    virtual void addTaskSlot();
    virtual void runTask(TaskState * pTaskState, std::string wk, std::string upstream);
    virtual bool sendWhisper(Socket * pSocket, std::string to, std::vector<std::string> & vect);

private:
    NodeZyre * mpNodeZyre = nullptr; ///< Current zyre node
};
} // namespace Salsa
