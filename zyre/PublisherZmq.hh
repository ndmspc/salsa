#pragma once

#include <czmq.h>

#include "Publisher.hh"

namespace Salsa {
///
/// \class PublisherZmq
///
/// \brief Base PublisherZmq class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class PublisherZmq : public Publisher {
public:
    PublisherZmq(std::string url = ">tcp://localhost:1234");
    virtual ~PublisherZmq();

    virtual void publish(std::string id, std::string name, std::string data, bool forcePublish = false);

private:
    zsock_t * mpSocket = nullptr; ///< Socket that... does stuff...
};
} // namespace Salsa
