#pragma once

#include "ActorZmq.hh"
#include "MessageZyre.hh"
#include "Node.hh"

#include "PollerZmq.hh"
#include "SocketZyre.hh"

namespace Salsa {
///
/// \class NodeZyre
///
/// \brief salsa node class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///
class NodeManagerZyre;
class NodeZyre : public Node, public ActorZmq {
public:
    /// Construct Zyre node with provided name (and packetizer)
    NodeZyre(std::string name = "");
    /// Destruct Zyre node
    virtual ~NodeZyre();

    virtual int init();
    virtual int exec();
    virtual int finish();

    void                                     addSocket(std::shared_ptr<SocketZyre> socket);
    std::vector<std::shared_ptr<SocketZyre>> sockets() const;
    void                                     addSocket(zsock_t * pSocket);

    /// Rerturns external socket
    zsock_t * socketExternal(int i) { return mZmqSockets.at(i); }

    void handleExternalZmq(zmsg_t * pMsg, zsock_t * pSocket);

    /// Sets type of current node
    void type(std::string t) { mType = t; }
    /// Returns type of current node
    std::string type() { return mType; }
    /// Sets Cluster alias
    void clusterAlias(std::string n) { mClusterAlias = n; }
    /// Returns Cluster alias
    std::string clusterAlias() { return mClusterAlias; }
    /// Sets JobInfo Group name
    void jobInfoGroupName(std::string n) { mJobInfoGroupName = n; }
    /// Returns JobInfo Group name
    std::string jobInfoGroupName() { return mJobInfoGroupName; }
    /// Sets JobInfo broker url
    void jobInfoBrokerUrl(std::string url) { mJobInfoBrokerUrl = url; }
    /// Returns JobInfo broker url
    std::string jobInfoBrokerUrl() { return mJobInfoBrokerUrl; }
    /// Sets Submit client url
    void submitClientUrl(std::string url) { mSubmitClientUrl = url; }
    /// Returns Submit client url
    std::string submitClientUrl() { return mSubmitClientUrl; }
    /// Sets JobInfo client url
    void jobInfoClientUrl(std::string url) { mJobInfoClientUrl = url; }
    /// Returns JobInfo client url
    std::string jobInfoClientUrl() { return mJobInfoClientUrl; }

private:
    std::vector<std::shared_ptr<SocketZyre>> mSockets{};              ///< List of zyre sockets
    std::vector<zsock_t *>                   mZmqSockets{};           ///< List of zmq sockets
    NodeManagerZyre *                        mpNodeManager = nullptr; ///< Job manager
    std::string                              mType;                   ///< Current node type
    std::string                              mClusterAlias{"local"};  ///< Cluster alias
    std::string                              mJobInfoGroupName;       ///< JobInfo Group name
    std::string                              mJobInfoBrokerUrl;       ///< JobInfo broker url (salsa-broker --in ...)
    std::string                              mJobInfoClientUrl; ///< JobInfo url for client (salsa-broker --out ...)
    std::string                              mSubmitClientUrl;  ///< Submit url for client
    int                                      mJobCheckTimeout;  ///< Job check timeout
};

} // namespace Salsa
