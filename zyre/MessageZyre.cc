#include "MessageZyre.hh"

namespace Salsa {
MessageZyre::MessageZyre(zyre_event_t * pEvent) : Message(), mpEvent(pEvent)
{
    ///
    /// Constructor with zyre event
    /// \param pEvent Zyre event
    ///
}
MessageZyre::~MessageZyre()
{
    ///
    /// Destructor
    ///
    zyre_event_destroy(&mpEvent);
}

void MessageZyre::print() const
{
    ///
    /// Print zyre message info
    ///

    zyre_event_print(mpEvent);
}

Message::EEventType MessageZyre::event() const
{
    ///
    /// Returns node event from zyre event
    ///

    if (mpEvent) {
        std::string eventType = zyre_event_type(mpEvent);
        SPD_TRACE("MessageZyre::nodeEvent() : type [{}]", eventType);

        if (eventType == "ENTER") {
            return Message::ENTER;
        }
        else if (eventType == "EXIT") {
            return Message::EXIT;
        }
        else if (eventType == "EVASIVE") {
            return Message::EVASIVE;
        }
        else if (eventType == "WHISPER") {
            return Message::WHISPER;
        }
    }

    return Message::UNKNOWN;
}

std::string MessageZyre::uuid() const
{
    ///
    /// Returns node uuid
    ///
    if (!mpEvent) {
        return "no_uuid";
    }

    return zyre_event_peer_uuid(mpEvent);
}

std::string MessageZyre::name() const
{
    ///
    /// Returns node uuid
    ///
    if (!mpEvent) {
        return "no_name";
    }

    return zyre_event_peer_name(mpEvent);
}

zyre_event_t * MessageZyre::zyreEvent() const
{
    ///
    /// Returns zyre event
    ///
    return mpEvent;
}

std::vector<std::string> & MessageZyre::content()
{
    ///
    /// Retursn vector of partial messages as strings
    ///

    mContent.clear();
    zmsg_t * pMsg = zyre_event_msg(mpEvent);
    if (pMsg) {
        char * m_str = zmsg_popstr(pMsg);
        while (m_str) {
            mContent.push_back(m_str);
            free(m_str);
            m_str = zmsg_popstr(pMsg);
        }
    }
    return mContent;
}

} // namespace Salsa
