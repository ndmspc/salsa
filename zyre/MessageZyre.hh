#pragma once

#include <zyre.h>
#include "Message.hh"

namespace Salsa {
///
/// \class MessageZyre
///
/// \brief Salsa zyre message class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class MessageZyre : public Message {
public:
    MessageZyre(zyre_event_t * pEvent = nullptr);
    virtual ~MessageZyre();

    virtual void print() const;

    virtual std::string uuid() const;
    virtual std::string name() const;
    virtual EEventType  event() const;

    virtual std::vector<std::string> & content();

    /// Get zyre event from message
    zyre_event_t * zyreEvent() const;

private:
    zyre_event_t *           mpEvent;  ///< Zyre event
    std::vector<std::string> mContent; ///< Content
};

} // namespace Salsa
