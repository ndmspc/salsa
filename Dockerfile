FROM registry.gitlab.com/ndmspc/user/al9:dep AS builder
ARG MY_PROJECT_VER=0.0.0-rc0
COPY . /builder/
WORKDIR /builder
RUN scripts/make.sh clean rpm

FROM docker.io/library/almalinux:9
COPY --from=builder /builder/build/RPMS/x86_64/salsa*.rpm /
RUN rm salsa-devel*.rpm
RUN dnf update -y
RUN dnf install epel-release 'dnf-command(config-manager)' 'dnf-command(copr)' -y
RUN dnf copr enable ndmspc/stable -y
RUN dnf install -y nmap
RUN dnf install -y salsa*.rpm
RUN rm -rf *.rpm
RUN dnf clean all
ENTRYPOINT [ "salsa" ]
