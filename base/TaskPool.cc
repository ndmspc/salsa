#include "TaskPool.hh"
#include "Job.hh"
#include "NodeManager.hh"
#include "TaskExecutor.hh"
namespace Salsa {

TaskPool::TaskPool(NodeManager * pNodeManager) : Object(), mpNodeManager(pNodeManager)
{
    ///
    /// Constructor
    ///
}
TaskPool::~TaskPool()
{
    ///
    /// Destructor
    ///
    for (auto task : mTasks) {
        delete task.second;
    }
    mTasks.clear();
}

void TaskPool::add(void * pPointer, TaskState * pTaskState)
{
    ///
    /// Adds Task EState
    ///
    if (pPointer == nullptr) return;

    mTasks.insert(std::make_pair(pPointer, pTaskState));
    pTaskState->id(mTasks.size());
}

TaskState * TaskPool::find(void * pPointer) const
{
    ///
    /// Returns TaskState
    /// /param p pointer
    ///
    auto found = mTasks.find(pPointer);
    if (found != mTasks.end()) {
        return found->second;
    }
    return nullptr;
}

TaskState * TaskPool::findById(uint32_t id) const
{
    ///
    /// Returns task state by id
    ///

    for (auto task : mTasks) {
        if (task.second->id() == id) return task.second;
    }

    return nullptr;
}

TaskState * TaskPool::findFreeTask() const
{
    ///
    /// Find avail slot in task pool
    ///

    for (auto task : mTasks) {
        if (task.second->state() == TaskState::idle) return task.second;
    }

    return nullptr;
}

uint32_t TaskPool::nSlotFree()
{
    ///
    /// Return n free slots
    ///
    uint32_t nFreeSlots = 0;
    for (auto task : mTasks) {
        if (task.second->state() == TaskState::idle) nFreeSlots++;
    }

    return nFreeSlots;
}

void TaskPool::changeState(uint32_t id, TaskState::EState state)
{
    ///
    /// Change state for task with id
    ///
    if (id == 0) return;

    for (auto task : mTasks) {
        if (task.second->id() == id) task.second->state(state);
    }
}

bool TaskPool::terminateJob(Job * pJob)
{
    ///
    /// Kill all tasks from pJob
    ///

    if (mTasks.size() == 0) return true;

    pJob->print();
    std::vector<TaskInfo *> runningTasks;
    pJob->tasks(runningTasks, Job::EQueueType::running);

    for (auto runningTask : runningTasks) {
        for (auto task : mTasks) {
            if (task.second->state() == TaskState::killed) {
                continue;
            }
            else if (task.second->task() == runningTask) {
                task.second->killTask();
                break;
            }
        }
    }
    // job->tasks(v, Job::EQueueType::pending);
    // job->tasks(v, Job::EQueueType::assigned);

    return false;
}

void TaskPool::print(bool verbose) const
{
    ///
    /// Prints task pool info
    ///
    uint32_t stat[TaskState::all] = {};

    for (auto task : mTasks) {
        stat[task.second->state()]++;
        task.second->print(verbose);
    }
    SPD_DEBUG("TaskPool I[{}] A[{}] R[{}]", stat[TaskState::idle], stat[TaskState::assigned], stat[TaskState::running]);
}

bool TaskPool::handlePipe(void * pPipe)
{
    ///
    /// Handle pipe
    ///

    TaskState * pTaskState = find(pPipe);
    if (pTaskState == nullptr) {
        SPD_ERROR("pTaskState by actor [{}] is null!!!", static_cast<void *>(pPipe));
        return false;
    }
    if (pTaskState->executor() == nullptr) {
        SPD_ERROR("pTaskState->executor() by actor [{}] is null!!!", static_cast<void *>(pPipe));
        return false;
    }
    if (pTaskState->executor()->pipe() == nullptr) {
        SPD_ERROR("pTaskState->executor()->pipe() by actor [{}] is null!!!", static_cast<void *>(pPipe));
        return false;
    }

    std::vector<std::string> extra;
    pTaskState->executor()->handlePipe(extra);
    TaskState::EState state = pTaskState->state();
    if (state == TaskState::assigned) {
        pTaskState->state(TaskState::running);
        // handle mJobs
        // Nothing for now i think
    }
    else if (state == TaskState::running || state == TaskState::killed) {
        pTaskState->state(TaskState::idle);
        pTaskState->pid(0);

        std::string wkUUID   = extra[0];
        std::string upstream = extra[1];
        // handle mJobs

        Job * pJob = mpNodeManager->job(pTaskState->task()->jobid());
        if (pJob != nullptr) {
            SPD_TRACE("TASK ENDED JOB [{}:{}]", pTaskState->task()->jobid(), pTaskState->task()->taskid());
            pJob->removeTask(pTaskState->task()->taskid(), Salsa::Job::running);
        }
        std::vector<std::string> out;
        out.push_back("TASK_RESULT");
        std::string payload;
        pTaskState->task()->SerializeToString(&payload);
        out.push_back(payload);
        pTaskState = findFreeTask();
        if (pTaskState && pTaskState->id() > 0) {
            SPD_TRACE("AFTER TASK_RESULT sending reserving task [{}]", pTaskState->id());
            out.push_back("&");
            out.push_back("FREESLOT");
            out.push_back(fmt::format("{}", pTaskState->id()));
            pTaskState->state(TaskState::assigned);
        }

        SPD_TRACE("Searching to worker [{}]", wkUUID);
        std::shared_ptr<Salsa::Worker> pWorker = mpNodeManager->worker(wkUUID);
        if (pWorker) {
            SPD_TRACE("Sending via pWorker [{}] to feeder [{}]", wkUUID, upstream);
            mpNodeManager->sendWhisper(pWorker->pipe().get(), upstream, out);
        }
    }
    print();

    return true;
}

} // namespace Salsa
