#include "Job.hh"

namespace Salsa {
Job::Job(std::string uuid, std::string type)
    : Object()
    , mUUID(uuid)
    , mType(type)
{
    ///
    /// Constructor
    ///

    // mTimeStarted = std::chrono::system_clock::now();
    mTimeStarted =
        std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

Job::~Job()
{
    ///
    /// Destructor
    ///
    for (int iType = EQueueType::pending; iType < EQueueType::all; iType++) {
        for (auto iTask : mTasks[iType]) {
            delete iTask.second;
            iTask.second = nullptr;
        }
        mTasks[iType].clear();
    }
}

bool Job::addTask(uint32_t id, TaskInfo * pTaskInfo, EQueueType type)
{
    ///
    /// Adds task to list of jobs
    ///
    if (!pTaskInfo) {
        return false;
    }

    if (type >= all) {
        SPD_CRIT("EQueueType is out of range [{}]", static_cast<int>(type));
        return false;
    }

    if (mUid == 99 && mGid == 99) {
        mUid = pTaskInfo->clientid();
        mGid = pTaskInfo->groupid();
    }

    mTasks[type].insert(std::make_pair(id, pTaskInfo));

    return true;
}

bool Job::moveTask(uint32_t id, EQueueType from, EQueueType to)
{
    ///
    /// Moves job from queue FROM to TO
    ///

    return moveTask(id, nullptr, from, to);
}

bool Job::moveTask(JobID_t id, TaskInfo * pTaskInfo, EQueueType from, EQueueType to)
{
    ///
    /// Moves job from queue FROM to TO
    ///
    auto iFound = mTasks[from].find(id);
    if (iFound != mTasks[from].end()) {
        if (pTaskInfo == nullptr) {
            pTaskInfo = iFound->second;
        }
        else {
            delete iFound->second;
        }
        // TODO This is asking for trouble...
        // Possible fix would be... std::shared_ptr;
        // Also, why do we even need to supply pTaskInfo anyways?

        if (to < EQueueType::done && from != EQueueType::assigned) mChanged = true;

        mTasks[from].erase(iFound);
        addTask(id, pTaskInfo, to);

        // mChanged = true;
        return true;
    }
    else {
        SPD_WARN("Job with id [{}] was not found in queue [{}] !!!", static_cast<int>(id), static_cast<int>(from));
        return false;
    }
}

bool Job::removeTask(uint32_t id, EQueueType from)
{
    ///
    /// Removes job from queue
    ///

    // TODO This could cause problems at some point...
    auto found = mTasks[from].find(id);
    if (found != mTasks[from].end()) {

        // if (from == EQueueType::running) mChanged = true;
        mTasks[from].erase(found);
        return true;
    }

    return false;
}

TaskInfo * Job::nextTask()
{
    ///
    /// return next job
    ///

    auto iAvailTask = mTasks[EQueueType::pending].begin();
    if (iAvailTask == mTasks[EQueueType::pending].end()) {
        return nullptr;
    }

    TaskInfo * pNewTask = iAvailTask->second;
    moveTask(iAvailTask->first, EQueueType::pending, EQueueType::assigned);
    return pNewTask;
}

void Job::tasks(std::vector<TaskInfo *> & targetVec, EQueueType type, bool shouldClear)
{
    ///
    /// return next job
    ///

    for (auto task : mTasks[type]) {
        targetVec.push_back(task.second);
    }

    if (shouldClear) {
        mTasks[type].clear();
    }
}

bool Job::isTaskInQueue(JobID_t id, EQueueType type) const
{
    ///
    /// Returns if task is i queue
    ///

    return (mTasks[type].find(id) != mTasks[type].end());

    // auto found = mTasks[type].find(id);
    // if (found != mTasks[type].end()) {
    //    return true;
    //}
    // else {
    //    return false;
    //}
}

void Job::print() const
{
    ///
    /// Prints job information
    ///
    SPD_DEBUG("{} P[{}] A[{}] R[{}] D[{}] F[{}] started[{}] finished[{}]", mUUID, mTasks[EQueueType::pending].size(),
              mTasks[EQueueType::assigned].size(), mTasks[EQueueType::running].size(), mTasks[EQueueType::done].size(),
              mTasks[EQueueType::failed].size(), mTimeStarted, mTimeFinished);
    SPD_TRACE("Feeder [{}] Consumer [{}]", mFeederUUID, mConsumerUUID);
}

void Job::json(Json::Value & json)
{
    ///
    /// Export data in json format
    ///

    Json::Value d;
    d["name"]            = mUUID;
    d["uid"]             = mUid;
    d["gid"]             = mGid;
    Json::UInt64 ts      = static_cast<Json::UInt64>(mTimeStarted);
    Json::UInt64 tf      = static_cast<Json::UInt64>(mTimeFinished);
    d["time"]["started"] = ts;
    if (tf) d["time"]["finished"] = tf;
    d["P"] = static_cast<Json::Value::UInt64>(mTasks[EQueueType::pending].size());
    // d["A"] = static_cast<Json::Value::UInt64>(mTasks[EQueueType::assigned].size());
    d["R"] = static_cast<Json::Value::UInt64>(mTasks[EQueueType::running].size() + mTasks[EQueueType::assigned].size());
    d["D"] = static_cast<Json::Value::UInt64>(mTasks[EQueueType::done].size());
    d["F"] = static_cast<Json::Value::UInt64>(mTasks[EQueueType::failed].size());

    d["rc"]["done"]   = Json::arrayValue;
    d["rc"]["failed"] = Json::arrayValue;
    if (size(EQueueType::all) <= mMaxIdsInJson) {
        for (auto ti : mTasks[EQueueType::done]) {
            d["rc"]["done"].append(ti.second->taskid());
        }
        for (auto ti : mTasks[EQueueType::failed]) {
            d["rc"]["failed"].append(ti.second->taskid());
        }
    }

    json.append(d);
}

size_t Job::size(EQueueType type) const
{
    ///
    /// Return size in queue
    /// (if EQueueType::all is set is show sum of sizes)
    ///
    if (type >= EQueueType::all) {
        size_t sum = mTasks[EQueueType::pending].size();
        sum += mTasks[EQueueType::assigned].size();
        sum += mTasks[EQueueType::running].size();
        sum += mTasks[EQueueType::done].size();
        sum += mTasks[EQueueType::failed].size();
        return sum;
    }
    else {

        return mTasks[type].size();
    }
}

size_t Job::sizeNotFinished() const
{
    ///
    /// Returns number of unfinished jobs
    ///
    size_t sum = mTasks[EQueueType::pending].size();
    sum += mTasks[EQueueType::assigned].size();
    sum += mTasks[EQueueType::running].size();
    return sum;
}

void Job::consumer(std::string uuid)
{
    ///
    /// Sets consumer uuid
    ///
    mConsumerUUID = uuid;
}

std::string Job::consumer() const
{
    ///
    /// return consumer UUID
    ///
    // TODO Also potentialy DANGEROUS
    return std::move(mConsumerUUID);
}

void Job::feeder(std::string uuid)
{
    ///
    /// Sets feeder uuid
    ///
    mFeederUUID = uuid;
}

std::string Job::feeder() const
{
    ///
    /// return feeder UUID
    ///
    // TODO Potentialy DANGEROUS!
    return std::move(mFeederUUID);
}

bool Job::haveMoreTasks() const
{
    ///
    /// return has more tasks
    ///
    return !mTasks[Job::pending].empty();
}

bool Job::isFinished()
{
    ///
    /// Returns if job is finished
    ///
    if (sizeNotFinished() > 0) return false;

    if (mTimeFinished == 0) {
        mJustFinished = true;
        mTimeFinished =
            std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch())
                .count();
    }
    return true;
}

bool Job::isJustFinished()
{
    if (mJustFinished) {
        mJustFinished = false;
        return true;
    }
    return mJustFinished;
};

} // namespace Salsa
