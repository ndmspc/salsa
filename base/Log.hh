#pragma once
#include <memory>
#include <string>
#include <vector>
// #include <spdlog/sinks/basic_file_sink.h>
// #include <spdlog/sinks/stdout_sinks.h>
#include "salsa.hh"

namespace Salsa {
///
/// \class Log
///
/// \brief ---
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Log {
public:
    Log();
    ~Log();

    /// Add output sink (file, console, zmq) for SPDLOG
    int add(std::string);
    /// Set name of job (only used for spdlog logger identification)
    void name(char const * pNewName) { mName = pNewName; }
    /// Set name of job (only used for spdlog logger identification)
    void name(std::string newName) { mName = newName; }

    /// Get name of job (only used for spdlog logger identification)
    std::string name() const { return mName; }

    /// Create SPDLOG loger
    int create();
    /// Write to logger
    int write(char const *);
    /// Get SPDLOG logger handle
    std::shared_ptr<spdlog::logger> spd() { return mpTarget; }
    /// Get info about sinks
    int empty() { return mSinks.empty(); }

    /// Set FD of pipe to watch
    void fd(int newFD) { mFD = newFD; }
    /// Get FD of currently watched pipe
    int fd() const { return mFD; }

private:
    int                             mFD = -1;           ///< FD of current pipe
    static uint64_t                 msID;               ///< Static Job newName (holds index)
    std::string                     mName = nullptr;    ///< newName (name) of current job
    std::vector<spdlog::sink_ptr>   mSinks;             ///< Sinks for SPDLOG
    std::shared_ptr<spdlog::logger> mpTarget = nullptr; ///< SPDLOG logger handle
};
} // namespace Salsa
