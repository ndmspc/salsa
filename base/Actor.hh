#pragma once

#include <csignal>

#include "Object.hh"

namespace Salsa {
///
/// \class Actor
///
/// \brief Base salsa actor class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Actor : public Object {
public:
    Actor();
    virtual ~Actor();

    /// Setter for pipe
    virtual void pipe(void *) = 0;

    /// First function
    virtual int init() = 0;

    /// Main function
    virtual int exec() = 0;

    /// Last function
    virtual int finish() = 0;

    /// Returns if salsa is interrupted
    static std::sig_atomic_t interrupted() { return msInterrupted; }

    /// Setter salsa interruption
    static void interrupted(std::sig_atomic_t sig) { msInterrupted = sig; }

    /// Setter salsa interruption
    static void signalHandler(int signalNumber);

private:
    static std::sig_atomic_t msInterrupted; ///< flag if salsa is interrupted
};

} // namespace Salsa
