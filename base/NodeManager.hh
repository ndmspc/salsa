#pragma once
#include "Consumer.hh"
#include "Feeder.hh"
#include "Job.hh"
#include "Object.hh"
#include "Publisher.hh"
#include "Socket.hh"
#include "TaskPool.hh"
#include "Worker.hh"
#include "TaskInfo.pb.h"

namespace Salsa {
///
/// \class NodeManager
///
/// \brief NodeManager class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class NodeManager : public Object {
public:
    NodeManager();
    virtual ~NodeManager();

    void print(std::string opt = "") const;

    // TODO Annotate add ops
    void addConsumer(std::string uuid, std::shared_ptr<Socket> s);
    void addFeeder(std::string uuid, std::shared_ptr<Socket> s);
    void addWorker(std::string uuid, std::shared_ptr<Socket> s);
    void addTask(TaskInfo * taskInfo, std::string cuuid, std::string fuuid,
                 Salsa::Job::EQueueType t = Salsa::Job::pending);

    // TODO Yet again...
    virtual Socket * onEnter(std::string self, std::string fromType, Message * msg, std::vector<std::string> & out);
    virtual Socket * onExit(std::string self, Message * msg, std::vector<std::string> & out);
    virtual Socket * onWhisper(std::string self, Message * msg, std::vector<std::string> & out);

    // TODO Return ops
    std::shared_ptr<Feeder>   feeder(std::string uuid) const;
    std::shared_ptr<Consumer> consumer(std::string uuid) const;
    std::shared_ptr<Worker>   worker(std::string uuid) const;
    Job *                     job(std::string uuid);

    /// Get NM's task pool
    TaskPool * taskPool();

    // TODO Annotate
    virtual void addTaskSlot();
    bool         hasJobs() const;
    virtual bool terminateFinishedJobs();
    /// Returns finished job timeout
    uint64_t finishedJobTimeout() const { return mFinishedJobTimeout; }
    /// Sets finished job timeout
    void finishedJobTimeout(uint64_t t) { mFinishedJobTimeout = t; }

    int32_t nSlots(double mult = 1.0) const;
    void    jobs(std::string clientUUID, std::vector<std::string> & jobs) const; // TODO ehm, what?

    // TODO Task ops
    TaskInfo *   getNextTask();
    virtual void resultTask(TaskInfo * task);
    /// Handle return of task and send it to external client
    virtual void resultTaskToExternal(Job *, TaskInfo *){};
    virtual void noMoreTasks(std::string jobUUID);
    virtual bool haveMoreTasks();
    virtual bool haveMoreTasks(std::string jobUUID);

    /// Run task interface
    virtual void runTask(TaskState * ts, std::string wk, std::string upstream) = 0;

    // TODO Terminate ops
    virtual void terminateJob(std::string uuid);
    virtual void terminateAllJobs(bool finishedonly = false);

    virtual bool handleTaskPool(void * p);
    virtual bool sendWhisper(Socket * s, std::string to, std::vector<std::string> & v);

    // TODO annotate ops
    virtual void        publisher(Publisher * p);
    virtual Publisher * publisher() const;
    virtual bool        publish(std::string id, bool force = false) const;
    /// Sets Cluster alias
    void clusterAlias(std::string n) { mClusterAlias = n; }
    /// Returns Cluster alias
    std::string clusterAlias() { return mClusterAlias; }

protected:
    std::string                                      mClusterAlias{"local"}; ///< Cluster alias
    std::map<std::string, Job *>                     mJobs{};                ///< List of jobs
    std::vector<std::string>                         mActiveJobs{};          ///< List of active jobs
    std::vector<std::string>                         mFinishedJobs{};        ///< List of finished jobs
    uint64_t                                         mFinishedJobTimeout;    ///< Finished job timeout in seconds
    std::map<std::string, std::shared_ptr<Worker>>   mWorkers{};             ///< List of Workers
    std::map<std::string, std::shared_ptr<Consumer>> mConsumers{};           ///< List of Consumers
    std::map<std::string, std::shared_ptr<Feeder>>   mFeeders{};             ///< List of Feeders
    TaskPool *                                       mpTaskPool  = nullptr;  ///< Task pool
    Publisher *                                      mpPublisher = nullptr;  ///< Publisher
};
} // namespace Salsa
