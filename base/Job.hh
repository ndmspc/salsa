#pragma once
#include <json/json.h>
#include "Object.hh"
#include "TaskInfo.pb.h"

namespace Salsa {
///
/// \class Job
///
/// \brief Job class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Job : public Object {
public:
    /// Queue types
    enum EQueueType { pending = 0, assigned = 1, running = 2, done = 3, failed = 4, all = 5 };

    Job(std::string uuid = "", std::string type = "NONE");
    virtual ~Job();

    void print() const;
    void json(Json::Value & json);

    /// returns UUID
    std::string uuid() const { return mUUID; }

    /// Return time started for the job
    uint64_t timeStarted() const { return mTimeStarted; }
    /// Returns time finished for the job
    uint64_t timeFinished() const { return mTimeFinished; }

    // TODO Get next available task?job?
    TaskInfo * nextTask(); // I assume task

    // TODO Annotate task ops
    void tasks(std::vector<TaskInfo *> & v, EQueueType type, bool clear = true);
    bool addTask(uint32_t id, TaskInfo * pJob, EQueueType type);
    bool moveTask(uint32_t id, EQueueType from, EQueueType to);
    bool moveTask(uint32_t id, TaskInfo * pJI, EQueueType from, EQueueType to);
    bool removeTask(uint32_t id, EQueueType from);

    // TODO size ops
    size_t size(EQueueType t = all) const;
    size_t sizeNotFinished() const;

    // Get/set consumer (receiver)
    void        consumer(std::string uuid);
    std::string consumer() const;

    // Get/set feeder (transmitter)
    void        feeder(std::string uuid);
    std::string feeder() const;

    /// Task statuses
    bool haveMoreTasks() const;
    /// Returns if jobs is finished
    bool isFinished();

    /// Returns if job was just finished
    bool isJustFinished();

    /// Check task presence in certain queue
    bool isTaskInQueue(uint32_t id, EQueueType type) const;

    using JobID_t = uint32_t; ///< Job ID type alias

    /// Returns if job info was changed
    bool changed() const { return mChanged; }
    /// Set if job info was changed
    void changed(bool c) { mChanged = c; }

    /// Returns submitter socket index
    int submitterSocketIndex() const { return mSubmitterSocketIndex; }
    /// Set submitter socket index
    void submitterSocketIndex(int i) { mSubmitterSocketIndex = i; }

    /// Returns submitter socket identity
    void * submitterSocketID() const { return mSubmitterSocketID; }
    /// Set submitter socket identity
    void submitterSocketID(void * id) { mSubmitterSocketID = id; }

    /// Returns Maximum number when joh ids are produced in json
    size_t maxIdsInJson() const { return mMaxIdsInJson; }
    /// Set Maximum number when joh ids are produced in json
    void maxIdsInJson(size_t i) { mMaxIdsInJson = i; }

protected:
    std::map<uint32_t, TaskInfo *> mTasks[all] = {};            ///< Lists of jobs
    std::string                    mUUID{""};                   ///< Job UUID
    uint32_t                       mUid{99};                    ///< Job user id (nobody : 99)
    uint32_t                       mGid{99};                    ///< Job group id (nogroup : 99)
    std::string                    mConsumerUUID{""};           ///< Source (consumer) UUID
    std::string                    mFeederUUID{""};             ///< Feeder UUID
    std::string                    mType{"NONE"};               ///< Job type
    uint64_t                       mTimeStarted{};              ///< Time started
    uint64_t                       mTimeFinished{0};            ///< Time finished
    int                            mSubmitterSocketIndex{-1};   ///< Submitter socket index in NodeZyre::mZmqSockets
    void *                         mSubmitterSocketID{nullptr}; ///< Submitter socket identity
    size_t                         mMaxIdsInJson{1000};         ///< Maximum number when joh ids are produced in json
    bool mJustFinished{false}; ///< Flag if job is finished. Note that it is reported only once

private:
    bool mChanged = false; ///< Flag if job was changed
};
} // namespace Salsa
