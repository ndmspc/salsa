#include "Worker.hh"
#include "NodeInfo.pb.h"
#include "NodeManager.hh"
#include "TaskInfo.pb.h"
namespace Salsa {
Worker::Worker(std::string uuid, std::shared_ptr<Socket> pipe, NodeManager * nm) : Distributor(uuid, pipe, nm)
{
    ///
    /// Constructor
    ///

    std::string const numCoresEnvName = "SALSA_WORKERS_COUNT";

    mNumCores = std::thread::hardware_concurrency();
    if (std::getenv(numCoresEnvName.c_str())) {
        try {
            std::string numCoresStr = std::getenv(numCoresEnvName.c_str());
            if (!numCoresStr.empty()) {
                SPD_INFO("Using SALSA_WORKERS_COUNT env to set number of cores [{}] ...", numCoresStr);
                std::size_t found    = numCoresStr.find('m');
                int         tmpCores = 0;

                if (found != std::string::npos) {
                    numCoresStr.erase(numCoresStr.begin() + found);
                    tmpCores = std::stoi(numCoresStr) / 1000;
                }
                else {
                    tmpCores = std::stoi(numCoresStr);
                }

                if (tmpCores > 0) {
                    mNumCores = static_cast<unsigned int>(tmpCores);
                }
                else {
                    SPD_ERROR("Provided env variable [{}] is zero/negative! Value: [{}]!!!", numCoresEnvName, tmpCores);
                    throw std::exception();
                }
            }
        }
        catch (std::exception &) // std::stoi
        {
            // Perhaps it would be better to (re-)throw exception?
            SPD_ERROR("An exception occured while trying to parse env var [{}]", numCoresEnvName);
            throw;
        }
    }

    SPD_INFO("WORKER [{}] has {} cores", mUUID, mNumCores);

    for (uint32_t iSlot = 0; iSlot < mNumCores; iSlot++) {
        SPD_TRACE("Worker [{}] slot [{}]", mUUID, iSlot);
        mpNodeManager->addTaskSlot();
    }
    mpNodeInfo->set_uuid(mUUID);
    mpNodeInfo->set_slots(mNumCores);
}

Worker::~Worker()
{
    ///
    /// Destructor
    ///
}

uint32_t Worker::numCores() const
{
    ///
    /// Returns number of cores
    ///
    return mNumCores;
}

// void Worker::print() const
// {
//     ///
//     /// Prints Worker information
//     ///
// }

void Worker::onEnter(Message * /*inMsg*/, std::vector<std::string> & out, std::string type)
{
    ///
    /// onEnter
    ///

    if (type == "FEEDER") {
        out.push_back("NODEINFO");
        std::string payload;
        mpNodeInfo->SerializeToString(&payload);
        out.push_back(payload);
    }
}
void Worker::onExit(Message * /*inMsg*/, std::vector<std::string> & /*out*/)
{
    ///
    /// onExit
    ///
}

void Worker::onWhisper(Message * inMsg, std::vector<std::string> & out)
{
    ///
    /// onWhisper
    ///

    std::vector<std::string> inContent = inMsg->content();

    if (inContent[0] == "SUB") {
        TaskState * ts = mpNodeManager->taskPool()->findFreeTask();
        if (ts && ts->id() > 0) {
            SPD_TRACE("AFTER SUB reserving task [{}]", ts->id());
            out.push_back("FREESLOT");
            out.push_back(fmt::format("{}", ts->id()));
            ts->state(TaskState::assigned);
        }
        mpNodeManager->taskPool()->print();
    }
    else if (inContent[0] == "TASK") {
        std::string payload = inContent[1];
        uint32_t    id      = static_cast<uint32_t>(strtoul(inContent[2].c_str(), nullptr, 0));
        TaskState * ts      = mpNodeManager->taskPool()->findById(id);
        SPD_TRACE("Searching in task pool for id [{}] and found state [{}] ", id, static_cast<int>(ts->state()));

        TaskInfo * task = ts->task();
        if (!task) {
            task = new TaskInfo();
            ts->task(task);
        }

        if (!task->ParseFromString(payload)) {
            SPD_ERROR("Message does not contain ProtoBuf message!");
            for (auto s : inContent) {
                SPD_ERROR("::onWhisper inMSG [{}]", s);
            }
            return;
        }
        SPD_TRACE("[{}] TASK from [{}] JOB [{}:{}] started", mUUID, inMsg->uuid(), task->jobid(), task->taskid());

        // TODO : check if it is assigned (it should be already by reservation)
        if (ts->state() != TaskState::assigned)
            SPD_ERROR("Task [{}:{}] is not assigned and it should be. Problem with reservation !!!!", task->jobid(),
                      task->taskid());

        mpNodeManager->addTask(task, mUUID, inMsg->uuid(), Salsa::Job::running);
        mpNodeManager->runTask(ts, mUUID, inMsg->uuid());

        if (!getenv("SALSA_FAST")) {
            out.push_back("TASK_IS_RUNNING");
            out.push_back(payload);
        }

        ts = mpNodeManager->taskPool()->findFreeTask();
        if (ts && ts->id() > 0) {
            SPD_TRACE("AFTER TASK reserving task [{}]", ts->id());
            // TODO if free core available then do
            out.push_back("&");
            out.push_back("FREESLOT");
            out.push_back(fmt::format("{}", ts->id()));
            ts->state(TaskState::assigned);
        }
    }
    else if (inContent[0] == "NOMORETASKS") {
        // Unsubscribe worker from feeder if needed
        SPD_TRACE("Releasing reservation [{}] because of no more jobs", inContent[1]);
        uint32_t id = static_cast<uint32_t>(strtoul(inContent[1].c_str(), nullptr, 0));
        mpNodeManager->taskPool()->changeState(id, TaskState::idle);
        mpNodeManager->taskPool()->print();
    }
    else if (inContent[0] == "TERMINATEJOB") {
        // terminate job
        mpNodeManager->terminateJob(inContent[1]);
        SPD_INFO("WORKER [{}] has finished job [{}]", mUUID, inContent[1]);
    }
}

} // namespace Salsa
