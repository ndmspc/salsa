#pragma once

#include "Distributor.hh"
#include "TaskInfo.pb.h"

namespace Salsa {
///
/// \class Consumer
///
/// \brief Base Consumer class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Consumer : public Distributor {
public:
    Consumer(std::string uuid, std::shared_ptr<Socket> pPipe, NodeManager * pNM);
    virtual ~Consumer();

    /// TODO Consumer action on ENTER event
    virtual void onEnter(Message * pInMsg, std::vector<std::string> & out, std::string type);

    /// TODO Consumer action on EXIT event
    virtual void onExit(Message * pInMsg, std::vector<std::string> & out);

    /// TODO Consumer action on WHISPER event
    virtual void onWhisper(Message * pInMsg, std::vector<std::string> & out);

    // void resultTask(std::string uuid, TaskInfo * task);
};
} // namespace Salsa
