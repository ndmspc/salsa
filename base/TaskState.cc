#include <signal.h>
#include "TaskState.hh"
#include "TaskExecutor.hh"
namespace Salsa {

TaskState::TaskState(TaskExecutor * pTaskExecutor) : Object(), mpTaskExecutor(pTaskExecutor)
{
    ///
    /// Constructor
    ///
}
TaskState::~TaskState()
{
    ///
    /// Destructor
    ///
    delete mpTask;
    delete mpTaskExecutor;
}

void TaskState::id(uint32_t id)
{
    ///
    /// Seting id
    ///
    mId = id;
}
uint32_t TaskState::id() const
{
    ///
    /// Returns id
    ///
    return mId;
}

void TaskState::state(EState s)
{
    ///
    /// Seting state
    ///
    mState = s;
}
TaskState::EState TaskState::state() const
{
    ///
    /// Returns current state
    ///
    return mState;
}

void TaskState::pid(uint32_t pid)
{
    ///
    /// Seting PID
    ///
    mPID = pid;
}
uint32_t TaskState::pid() const
{
    ///
    /// Returns PID
    ///
    return mPID;
}

TaskInfo * TaskState::task() const
{
    ///
    /// Returns task
    ///
    return mpTask;
}

void TaskState::task(TaskInfo * t)
{
    ///
    /// Sets task
    ///

    mpTask = t;
}

TaskExecutor * TaskState::executor()
{
    ///
    /// Returns task executor
    ///
    return mpTaskExecutor;
}

void TaskState::killTask()
{
    ///
    /// Kill task
    ///

    if (mPID == 0) return;

    int rc = kill(mPID, SIGKILL);
    if (rc == 0) {
        SPD_WARN("JOB [{}:{}] PID [{}] was killed by [SIGKILL] signal", mpTask->jobid(), mpTask->taskid(), mPID);
    }
    else {
        SPD_ERROR("JOB [{}:{}] PID [{}] could not be killed it [SIGKILL] !!!", mpTask->jobid(), mpTask->taskid(), mPID);
    }

    // mPID   = 0;
    mState = killed;
}

void TaskState::print(bool verbose) const
{
    ///
    /// Prints task pool info
    ///

    if (!verbose) return;

    SPD_INFO("id [{}] state [{}] job [{}:{}]", mId, static_cast<int>(mState), mpTask ? mpTask->jobid() : "n/a",
             mpTask ? mpTask->taskid() : -1);
}

} // namespace Salsa
