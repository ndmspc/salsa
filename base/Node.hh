#pragma once

// TODO? #include "Object.hh"
#include <json/json.h>
#include <memory>
#include "salsa.hh"
#include <string>

#include "NodeInfo.pb.h"
#include "Publisher.hh"

namespace Salsa {
///
/// \class Node
///
/// \brief Base Node class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Node : public std::enable_shared_from_this<Node> // TODO ? public Object
{
public:
    Node(std::string name = "", std::string uuid = "");
    virtual ~Node();

    virtual void print() const;
    virtual void json(Json::Value & root);
    virtual void publish();

    /// Returns node name
    std::string name() const { return mpNodeInfo->name(); }
    /// Returns node UUID
    std::string uuid() const { return mpNodeInfo->uuid(); }
    /// Returns node hostname
    std::string hostname() const { return mpNodeInfo->hostname(); }
    /// Returns parent node
    std::weak_ptr<Node> parent() const { return mpParent; }
    /// Returns nodes
    std::vector<std::shared_ptr<Node>> nodes() const { return mChildNodes; }

    /// Sets node name
    void name(std::string n) { mpNodeInfo->set_name(n); }
    /// Sets node uuid
    void uuid(std::string uuid) { mpNodeInfo->set_uuid(uuid); }
    /// Sets node hostname
    void hostname(std::string h) { mpNodeInfo->set_hostname(h); }
    /// Sets parent
    void parent(std::weak_ptr<Node> node) { mpParent = node; }

    /// Adds node to the list of nodes
    void add(std::shared_ptr<Node> node)
    {
        mChildNodes.push_back(node);
        node->parent(shared_from_this());
    }
    /// Find node by name
    std::shared_ptr<Node> find(std::string name) const;

    /// Remove node by uuid
    void removeByUUID(std::string uuid);

    /// Adds publisher to the node
    void add(std::shared_ptr<Publisher> pPublisher) { mPublishers.push_back(pPublisher); }
    /// Returns publishers
    std::vector<std::shared_ptr<Publisher>> publishers() const { return mPublishers; }
    /// Returns Node Info
    NodeInfo * nodeInfo() const { return mpNodeInfo; }

protected:
    NodeInfo *                              mpNodeInfo{new NodeInfo()}; ///< Node Info
    std::weak_ptr<Node>                     mpParent;                   ///< Parent node
    std::vector<std::shared_ptr<Node>>      mChildNodes = {};           ///< List of nodes
    std::vector<std::shared_ptr<Publisher>> mPublishers = {};           ///< List of publishers
};
} // namespace Salsa
