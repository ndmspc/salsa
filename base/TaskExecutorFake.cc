#include "TaskExecutorFake.hh"
#include "TaskPool.hh"
namespace Salsa {
TaskExecutorFake::TaskExecutorFake(TaskPool * pTaskPool) : TaskExecutor(), mpTaskPool(pTaskPool)
{
    ///
    /// Constructor
    ///
}
TaskExecutorFake::~TaskExecutorFake()
{
    ///
    /// Destructor
    ///
    delete mpPointer;
}
bool TaskExecutorFake::run(std::string worker, std::string upstream)
{
    ///
    /// Run
    ///
    mWorker   = worker;
    mUpstream = upstream;
    SPD_DEBUG("Running fake task worker [{}] upstream [{}]", mWorker, mUpstream);
    mpTaskState->state(TaskState::EState::running);
    mpTaskPool->handlePipe(mpPointer);
    return true;
}
bool TaskExecutorFake::handlePipe(std::vector<std::string> & extra)
{
    ///
    /// Handle pipe
    ///

    SPD_DEBUG("Handling pipe for fake task worker [{}] upstream [{}]", mWorker, mUpstream);
    extra.push_back(mWorker);
    extra.push_back(mUpstream);

    return true;
}
void * TaskExecutorFake::pipe() const
{
    ///
    /// Returns pipe
    ///
    return mpPointer;
}

} // namespace Salsa
