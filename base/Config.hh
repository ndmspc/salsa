#pragma once
#include "yaml.h"

#include "Node.hh"
#include "Object.hh"

namespace Salsa {
///
/// \class Config
///
/// \brief Base Config class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Config : public Object {
public:
    Config();
    virtual ~Config();

    virtual bool load(std::string file);
    void         filter(std::string const & f);
    virtual void print() const;

private:
    void findAndReplaceAll(std::string & data, std::string toSearch, std::string replaceStr);

protected:
    YAML::Node                        mConfig;   ///< YAML Configuration
    std::map<std::string, YAML::Node> mFilter{}; ///< Filter list
};
} // namespace Salsa
