#pragma once

#include "Distributor.hh"

namespace Salsa {
///
/// \class Worker
///
/// \brief Worker class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Worker : public Distributor {
public:
    Worker(std::string uuid, std::shared_ptr<Socket> pPipe, NodeManager * pNM);
    virtual ~Worker();
    uint32_t numCores() const;

    // void print() const;

    virtual void onEnter(Message * pInMsg, std::vector<std::string> & out, std::string type);
    virtual void onExit(Message * pInMsg, std::vector<std::string> & out);
    virtual void onWhisper(Message * pInMsg, std::vector<std::string> & out);

private:
    uint32_t mNumCores = -1; ///< Number of cores
};
} // namespace Salsa
