#pragma once

#include "Distributor.hh"
#include "TaskInfo.pb.h"

namespace Salsa {
///
/// \class Feeder
///
/// \brief Base Feeder class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Feeder : public Distributor {
public:
    Feeder(std::string uuid, std::shared_ptr<Socket> pPipe, NodeManager * pNM);
    virtual ~Feeder();

    // TODO Another one...
    virtual void onEnter(Message * pInMsg, std::vector<std::string> & out, std::string type);
    virtual void onExit(Message * pInMsg, std::vector<std::string> & out);
    virtual void onWhisper(Message * pInMsg, std::vector<std::string> & out);

    // TODO needs annotation
    void subscribe(std::string uuid);
    void removeWorkerTask(TaskInfo * pTI);
    void removeWorkerTask(TaskInfo * pTI, std::string uuid);
    void terminateJob(std::string uuid);

protected:
    std::map<std::string, std::vector<TaskInfo *>> mWorkerTasks{}; ///< Worker tasks
};
} // namespace Salsa
