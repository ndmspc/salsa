#pragma once

#include "TaskExecutor.hh"

namespace Salsa {
///
/// \class TaskExecutorFake
///
/// \brief TaskExecutorFake class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///
class TaskPool;
class TaskExecutorFake : public TaskExecutor {
public:
    TaskExecutorFake(TaskPool * pTP);
    virtual ~TaskExecutorFake();

    virtual bool   run(std::string, std::string);
    virtual void * pipe() const;
    virtual bool   handlePipe(std::vector<std::string> & extra);

private:
    Object *    mpPointer{new Object()}; ///< Fake pointer
    std::string mWorker{};               ///< Worker
    std::string mUpstream{};             ///< Upstream
    TaskPool *  mpTaskPool = nullptr;    ///< Fake pointer
};
} // namespace Salsa
