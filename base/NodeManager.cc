#include <json/json.h>
#include "NodeManager.hh"
namespace Salsa {
NodeManager::NodeManager()
    : Object()
    , mFinishedJobTimeout(24 * 3600)
{
    ///
    /// Constructor
    ///
    srand(static_cast<uint32_t>(time(nullptr))); // seed with time since epoch
}

NodeManager::~NodeManager()
{
    ///
    /// Destructor
    ///

    for (auto job : mJobs) {
        if (mpTaskPool) {
            mpTaskPool->terminateJob(job.second);
        }
        delete job.second;
    }
    // terminateJobAll();
    mJobs.clear();
    delete mpTaskPool;
    delete mpPublisher;
}

void NodeManager::print(std::string /*opt*/) const
{
    ///
    /// Prints NodeManager information
    ///

    SPD_TRACE("mFeeders [{}] mConsumers [{}] mWorkers [{}] mJobs [{}] ", mFeeders.size(), mConsumers.size(),
              mWorkers.size(), mJobs.size());

    if (mJobs.size() > 0) {
        SPD_DEBUG("= JOBS =======================");
        for (auto j : mJobs) {
            j.second->print();
        }
        SPD_DEBUG("==============================");
    }
    else {
        SPD_DEBUG("= NO JOBS ====================");
    }

    if (mpTaskPool) {
        mpTaskPool->print();
    }
}

void NodeManager::addConsumer(std::string uuid, std::shared_ptr<Socket> pSocket)
{
    ///
    /// Add consumer
    ///

    mConsumers.emplace(uuid, std::make_shared<Consumer>(uuid, pSocket, this));
}

void NodeManager::addFeeder(std::string uuid, std::shared_ptr<Socket> pSocket)
{
    ///
    /// Add feeder
    ///

    mFeeders.emplace(uuid, std::make_shared<Feeder>(uuid, pSocket, this));
}

void NodeManager::addWorker(std::string uuid, std::shared_ptr<Socket> pSocket)
{
    ///
    /// Add worker
    ///

    mWorkers.emplace(uuid, std::make_shared<Worker>(uuid, pSocket, this));
}

Socket * NodeManager::onEnter(std::string self, std::string fromType, Message * msg, std::vector<std::string> & out)
{
    ///
    /// On ENTER event
    ///

    SPD_TRACE("NodeManager::onEnter self [{}] from [{}] type [{}] msg [{}]", self, msg->uuid(), fromType,
              static_cast<void *>(msg));

    // TODO Implement map<std::string /* self */, Node::ENodeType?>
    auto pFeeder = feeder(self);
    if (pFeeder) {
        SPD_DEBUG("::onEnter FEEDER [{}] has client on network [{}] type [{}]", self, msg->uuid(), fromType);
        SPD_INFO("FEEDER [{}] <= [{}] [{}]", self, msg->uuid(), fromType);
        if (fromType == "CONSUMER") {
            pFeeder->addClient(msg->uuid(), fromType);
            pFeeder->onEnter(msg, out, fromType);
        }
        else if (fromType == "WORKER") {
            pFeeder->addClient(msg->uuid(), fromType);
            pFeeder->onEnter(msg, out, fromType);
        }
        else if (fromType == "DISCOVERY") {
            // We fully ignoring it
            SPD_DEBUG("DISCOVERY is here");
        }
        else {
            pFeeder->addOther(msg->uuid(), fromType);
        }

        return pFeeder->pipe().get();
    }

    auto pConsumer = consumer(self);
    if (pConsumer) {
        SPD_DEBUG("::onEnter CONSUMER [{}] has client on network [{}] type [{}]", self, msg->uuid(), fromType);
        SPD_INFO("CONSUMER [{}] <= [{}] [{}]", self, msg->uuid(), fromType);
        if (fromType == "FEEDER") {
            pConsumer->addClient(msg->uuid(), fromType);
            pConsumer->onEnter(msg, out, fromType);
        }
        else {
            pConsumer->addOther(msg->uuid(), fromType);
        }

        return pConsumer->pipe().get();
    }

    auto pWorker = worker(self);
    if (pWorker) {
        SPD_DEBUG("::onEnter WORKER [{}] has client on network [{}] type [{}]", self, msg->uuid(), fromType);
        SPD_INFO("WORKER [{}] <= [{}] [{}]", self, msg->uuid(), fromType);
        if (fromType == "FEEDER") {
            pWorker->addClient(msg->uuid(), fromType);
            pWorker->onEnter(msg, out, fromType);
        }
        else {
            pWorker->addOther(msg->uuid(), fromType);
        }

        return pWorker->pipe().get();
    }

    return nullptr;
}
Socket * NodeManager::onExit(std::string self, Message * msg, std::vector<std::string> & out)
{
    ///
    /// On EXIT event
    ///

    SPD_TRACE("NodeManager::onExit self [{}] from [{}] msg [{}]", self, msg->uuid(), static_cast<void *>(msg));

    auto pWorker = worker(self);
    if (pWorker) {
        SPD_DEBUG("::onExit WORKER [{}] client on network [{}] has left", self, msg->uuid());
        SPD_INFO("WORKER [{}] => [{}]", self, msg->uuid());
        pWorker->onExit(msg, out);
        pWorker->removeClient(msg->uuid());
        return pWorker->pipe().get();
    }

    auto pFeeder = feeder(self);
    if (pFeeder) {
        SPD_DEBUG("::onExit FEEDER [{}] client on network [{}] has left", self, msg->uuid());
        SPD_INFO("FEEDER [{}] => [{}]", self, msg->uuid());
        pFeeder->onExit(msg, out);
        pFeeder->removeClient(msg->uuid());
        return pFeeder->pipe().get();
    }

    auto pConsumer = consumer(self);
    if (pConsumer) {
        SPD_DEBUG("::onExit CONSUMER [{}] client on network [{}] has left", self, msg->uuid());
        SPD_INFO("CONSUMER [{}] => [{}]", self, msg->uuid());
        pConsumer->onExit(msg, out);
        pConsumer->removeClient(msg->uuid());
        return pConsumer->pipe().get();
    }

    return nullptr;
}

Socket * NodeManager::onWhisper(std::string self, Message * msg, std::vector<std::string> & out)
{
    ///
    /// On WHISPER event
    ///

    SPD_TRACE("NodeManager::onWhisper self [{}] from [{}] msg [{}]", self, msg->uuid(), static_cast<void *>(msg));

    auto pFeeder = feeder(self);
    if (pFeeder) {
        SPD_TRACE("::onWhisper() FEEDER [{}] from [{}] has msg", self, msg->uuid());
        pFeeder->onWhisper(msg, out);
        return pFeeder->pipe().get();
    }

    auto pConsumer = consumer(self);
    if (pConsumer) {
        SPD_TRACE("::onWhisper() CONSUMER [{}] from [{}] has msg", self, msg->uuid());
        pConsumer->onWhisper(msg, out);
        return pConsumer->pipe().get();
    }

    auto pWorker = worker(self);
    if (pWorker) {
        SPD_TRACE("::onWhisper() WORKER [{}] from [{}] has msg", self, msg->uuid());
        pWorker->onWhisper(msg, out);
        return pWorker->pipe().get();
    }

    return nullptr;
}

bool NodeManager::sendWhisper(Socket * /*s*/, std::string /*to*/, std::vector<std::string> & /*v*/)
{
    ///
    /// Sends message
    ///

    return true;
}

void NodeManager::addTask(TaskInfo * pTaskInfo, std::string cuuid, std::string fuuid, Salsa::Job::EQueueType type)
{
    ///
    /// Adds task
    ///

    Salsa::Job * pJob   = nullptr;
    auto         search = mJobs.find(pTaskInfo->jobid());
    if (search != mJobs.end()) {
        // if pJob was found
        pJob = search->second;
    }
    else {
        // if pJob was not found
        pJob = new Salsa::Job(pTaskInfo->jobid());
        pJob->consumer(cuuid);
        pJob->feeder(fuuid);
        mJobs.insert(std::make_pair(pTaskInfo->jobid(), pJob));
        mActiveJobs.push_back(pTaskInfo->jobid());
        // TODO : now we need to tell all feeders that theat they should subscribe to workers
        SPD_TRACE("Looping feeders");
        for (auto feeder : mFeeders) {
            /// subscribe to all clients
            SPD_TRACE("Subscribe to feeder [{}]", feeder.first);
            feeder.second->subscribe(pTaskInfo->jobid());
        }
    }

    SPD_TRACE("::addTask from [{}] with task id [{}]", pTaskInfo->jobid(), pTaskInfo->taskid());
    pJob->addTask(pTaskInfo->taskid(), pTaskInfo, type);
}

TaskInfo * NodeManager::getNextTask()
{
    ///
    /// Return Next task from job
    ///
    TaskInfo * pTaskInfo = nullptr;

    SPD_TRACE("mActiveJobs.size() [{}]", mActiveJobs.size());
    while (mActiveJobs.size() > 0 && pTaskInfo == nullptr) {
        size_t      index  = static_cast<size_t>(rand()) % mActiveJobs.size();
        std::string jobstr = mActiveJobs[index];
        auto        iJob   = mJobs.find(jobstr);
        if (iJob != mJobs.end()) {
            pTaskInfo = iJob->second->nextTask();
            if (pTaskInfo) {
                SPD_TRACE("getNextTask FEEDER [{}] JOB [{}:{}]", iJob->first, pTaskInfo->jobid(), pTaskInfo->taskid());
                return pTaskInfo;
            }
        }

        // removing jobstring from mActiveJobs
        mActiveJobs.erase(std::remove(begin(mActiveJobs), end(mActiveJobs), jobstr), end(mActiveJobs));
    }

    SPD_TRACE("::getNextTask No pTaskInfo found");
    return nullptr;
}

void NodeManager::resultTask(TaskInfo * pTask)
{
    ///
    /// Handle result of pTask
    ///

    Job * pJob = job(pTask->jobid());
    if (pJob == nullptr) {
        delete pTask;
        return;
    }

    SPD_TRACE("TASK ENDED JOB [{}:{}]", pTask->jobid(), pTask->taskid());
    // search->second->moveTask(pTask->taskid(), pTask, Salsa::Job::running, Salsa::Job::done);

    // If job has no consumer we end (assuming that it is SUBMITTER)
    if (pJob->consumer().empty()) {
        // TODO : Fix done and failed
        auto sourceQueue = (pJob->isTaskInQueue(pTask->taskid(), Salsa::Job::assigned)) ? Job::assigned : Job::running;
        if (pTask->returncode() == 0) {
            pJob->moveTask(pTask->taskid(), sourceQueue, Salsa::Job::done);
        }
        else {
            pJob->moveTask(pTask->taskid(), sourceQueue, Salsa::Job::failed);
        }

        if (pJob->isFinished()) {
            /// Moving to finished jobs. They will be removed after some inactivity time
            mFinishedJobs.push_back(pJob->uuid());
        }

        resultTaskToExternal(pJob, pTask);

        print();
        // TODO we need to think what to do with TaskInfo object in highest level
        delete pTask;
        return;
    }

    if (pJob->isTaskInQueue(pTask->taskid(), Salsa::Job::assigned)) {
        SPD_WARN("Task [{}] duplicate found in [assigned] queue!", pTask->taskid());
        pJob->removeTask(pTask->taskid(), Salsa::Job::assigned);
    }
    else {
        SPD_WARN("Task [{}] duplicate found in [running] queue!", pTask->taskid());
        pJob->removeTask(pTask->taskid(), Salsa::Job::running);
    }

    std::shared_ptr<Consumer> pConsumer = consumer(pJob->consumer());
    std::vector<std::string>  out       = {"TASK_RESULT"};
    // out.push_back("TASK_RESULT");
    std::string payload;
    pTask->SerializeToString(&payload);
    out.push_back(payload);
    uint32_t slots = nSlots();

    // delete pTask;

    // TODO only for testing, REMOVE IT later
    // - Well, what about #ifdef DEBUG ?
    if (getenv("SALSA_FAKE")) slots *= 10;

    if (pJob->size(Job::pending) < slots) {
        if (pJob->haveMoreTasks()) {
            SPD_TRACE("We are requesting new tasks [{}] haveMoreTasks [{}]", slots, pJob->haveMoreTasks());
            out.push_back("&");
            out.push_back("SENDTASKS");
            out.push_back(fmt::format("{}", slots));
        }
    }

    sendWhisper(pConsumer->pipe().get(), pJob->feeder(), out);
}

void NodeManager::terminateJob(std::string uuid)
{
    ///
    /// Cleans job
    ///

    SPD_TRACE("Terminating job from client [{}]", uuid);

    auto iJob = mJobs.find(uuid);
    if (iJob != mJobs.end()) {
        if (mpTaskPool) {
            mpTaskPool->terminateJob(iJob->second);
        }

        for (auto f : mFeeders) {
            f.second->terminateJob(uuid);
        }

        mFinishedJobs.erase(std::remove(begin(mFinishedJobs), end(mFinishedJobs), uuid), end(mFinishedJobs));

        SPD_TRACE("Removing job [{}]", uuid);
        delete iJob->second;
        iJob->second = nullptr;
        mJobs.erase(iJob);
    }
    SPD_TRACE("NodeManager::terminateJob print()");
    print();
}

bool NodeManager::terminateFinishedJobs()
{
    ///
    /// Clear finished jobs
    ///

    if (mFinishedJobs.size() == 0) return false;

    SPD_DEBUG("Checking finished jobs [{}] to be removed ...", mFinishedJobs.size());

    std::chrono::time_point<std::chrono::system_clock> curTime = std::chrono::system_clock::now();
    uint64_t curTimeEpoch = std::chrono::duration_cast<std::chrono::seconds>(curTime.time_since_epoch()).count();
    std::vector<std::string> cleanUUID;
    for (auto js : mFinishedJobs) {
        auto j = job(js);
        if (j == nullptr) continue;
        if (curTimeEpoch - j->timeFinished() > mFinishedJobTimeout) {
            SPD_DEBUG("Terminating finished job. Time : diff[{}] timeout[{}]", curTimeEpoch - j->timeFinished(),
                      mFinishedJobTimeout);
            cleanUUID.push_back(js);
        }
    }
    if (!cleanUUID.size()) return false;

    for (auto u : cleanUUID) {
        terminateJob(u);
    }

    return true;
}

void NodeManager::terminateAllJobs(bool finishedonly)
{
    ///
    /// Terminate all jobs
    ///
    if (mJobs.size() == 0) return;

    std::vector<std::string> cleanUUID;
    if (finishedonly)
        for (auto job : mFinishedJobs) cleanUUID.push_back(job);

    else {
        for (auto job : mJobs) cleanUUID.push_back(job.first);
    }

    for (auto u : cleanUUID) {
        SPD_DEBUG("Terminating [{}]", u);
        terminateJob(u);
    }
}

std::shared_ptr<Feeder> NodeManager::feeder(std::string uuid) const
{
    ///
    /// Returns uuid is feeder
    /// /param uuid UUID
    ///
    auto search = mFeeders.find(uuid);
    if (search != mFeeders.end()) {
        return search->second;
    }
    else {
        return nullptr;
    }
}
std::shared_ptr<Consumer> NodeManager::consumer(std::string uuid) const
{
    ///
    /// Returns consumer
    /// /param uuid UUID
    ///
    auto search = mConsumers.find(uuid);
    if (search != mConsumers.end()) {
        return search->second;
    }
    return nullptr;
}
std::shared_ptr<Worker> NodeManager::worker(std::string uuid) const
{
    ///
    /// Returns worker
    /// /param uuid UUID
    ///
    auto search = mWorkers.find(uuid);
    if (search != mWorkers.end()) {
        return search->second;
    }
    return nullptr;
}

Job * NodeManager::job(std::string uuid)
{
    ///
    /// Returns job
    /// /param uuid UUID
    ///
    auto search = mJobs.find(uuid);
    if (search != mJobs.end()) {
        return search->second;
    }
    return nullptr;
}

void NodeManager::addTaskSlot()
{
    ///
    /// Reserve task slot
    ///
    if (mpTaskPool == nullptr) {
        mpTaskPool = new TaskPool(this);
    }
}

bool NodeManager::handleTaskPool(void * /*p*/)
{
    ///
    /// Handle task pool
    ///
    return false;
}

TaskPool * NodeManager::taskPool()
{
    ///
    /// Returns task pool
    ///
    return mpTaskPool;
}

bool NodeManager::hasJobs() const
{
    ///
    /// Returns if jobs are active
    ///
    return mActiveJobs.size() > 0;
}

void NodeManager::jobs(std::string client_uuid, std::vector<std::string> & jobs) const
{
    ///
    /// Returns list of jobs
    ///

    for (auto search : mJobs) {
        if (search.second != nullptr && search.second->feeder() == client_uuid) {
            jobs.push_back(search.first);
        }
    }
}
int32_t NodeManager::nSlots(double mult) const
{
    ///
    /// Returns numer of slots
    ///

    int32_t num = 0;
    for (auto feeder : mFeeders) {
        num += feeder.second->nodeInfo()->slots();
    }
    return num * mult;
}

void NodeManager::noMoreTasks(std::string /*jobUUID*/)
{
    ///
    /// Sets no more tasks to job with jobuuid
    ///

    (void)(0);
    // auto search = mJobs.find(jobUUID);
    // if (search != mJobs.end()) {
    //    return search->second->haveMoreTasks();
    //}
}

bool NodeManager::haveMoreTasks()
{
    ///
    /// Sets no more tasks to any job
    ///

    bool rc = false;
    for (auto job : mJobs) {
        if (job.second->Job::size(Job::pending)) {
            // job.second->haveMoreTasks(true);
            bool isActiveJob = false;
            for (auto pActiveJobUUID : mActiveJobs) {
                if (pActiveJobUUID == job.first) {
                    isActiveJob = true;
                    break;
                }
            }

            if (!isActiveJob) {
                mActiveJobs.push_back(job.first);
            }

            rc = true;
        }
    }
    return rc;
}

bool NodeManager::haveMoreTasks(std::string jobUUID)
{
    ///
    /// Sets no more tasks to job with jobuuid
    ///
    auto found = mJobs.find(jobUUID);
    if (found != mJobs.end()) {
        return found->second->haveMoreTasks();
    }
    else {
        return false;
    }
}

void NodeManager::publisher(Publisher * pPublisher)
{
    ///
    /// Sets publisher
    ///
    mpPublisher = pPublisher;
}

Publisher * NodeManager::publisher() const
{
    ///
    /// Returns publisher
    ///
    return mpPublisher;
}

bool NodeManager::publish(std::string id, bool force) const
{
    ///
    /// Publishes node manager state
    ///
    if (!mpPublisher) return false;

    bool          changed         = false;
    bool          jobJustFinished = false;
    Json::Value   json;
    Json::Value & json_jobs = json["jobs"];
    json_jobs               = Json::arrayValue;
    json["version"] =
        fmt::format("v{}.{}.{}-{}", salsa_VERSION_MAJOR(salsa_VERSION), salsa_VERSION_MINOR(salsa_VERSION),
                    salsa_VERSION_PATCH(salsa_VERSION), salsa_VERSION_TWEAK);
    // json_node               = Json::arrayValue;
    if (mJobs.size() > 0) {
        for (auto job : mJobs) {
            if (job.second->changed()) changed = true;
            if (job.second->isJustFinished()) jobJustFinished = true;
        }

        SPD_DEBUG("force=[{}] changed=[{}] jobJustFinished=[{}]", force, changed, jobJustFinished);

        if (!force && !changed && !jobJustFinished) return false;

        for (auto job : mJobs) {
            job.second->json(json_jobs);
        }
    }

    // Fill node info
    // mClusterAlias = "mycluster";
    std::string name = fmt::format("{}:{}", id, mClusterAlias);
    auto        f    = feeder(id);
    if (f) {
        json["node"] = f->jsonValueNodeInfo();
        if (!f->nodeInfo()->name().empty()) name = f->nodeInfo()->name();
    }

    Json::StreamWriterBuilder wBuilder;
    wBuilder["indentation"] = "";
    std::string data        = Json::writeString(wBuilder, json);

    SPD_DEBUG("Publish sub [salsa:{}] id [{}] data [{}] ", name, name, data);
    mpPublisher->publish(id, name, data, jobJustFinished);

    for (auto job : mJobs) {
        job.second->changed(false);
    }

    return true;
}

} // namespace Salsa
