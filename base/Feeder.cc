#include "Feeder.hh"
#include "Job.hh"
#include "NodeManager.hh"

using namespace fmt::literals;

namespace Salsa {
Feeder::Feeder(std::string uuid, std::shared_ptr<Socket> pipe, NodeManager * pNM) : Distributor(uuid, pipe, pNM)
{
    ///
    /// Constructor
    ///
    mpNodeInfo->set_uuid(mUUID);
}

Feeder::~Feeder()
{
    ///
    /// Destructor
    ///
}

void Feeder::onEnter(Message * /*pInMsg*/, std::vector<std::string> & out, std::string type)
{
    ///
    /// onEnter
    ///

    if (type == "WORKER" && mpNodeManager->hasJobs()) {
        out.push_back("SUB");
    }
}

void Feeder::onExit(Message * pInMsg, std::vector<std::string> & /*out*/)
{
    ///
    /// onExit
    ///

    SPD_TRACE("::onExit inMSG [{}]", pInMsg->uuid());

    uint32_t slots = 0;
    uint32_t iPos  = 0;
    bool     found = false;
    for (auto & node : mpNodeInfo->hosts()) {
        if (pInMsg->uuid() == node.uuid()) {
            found = true;
        }
        else {
            slots += node.slots();
            if (!found) {
                iPos++;
            }
        }
    }

    // TODO: Continue if worker (Better check is needed)
    if (found) { // if node is found in local reg
        mpNodeInfo->mutable_hosts()->DeleteSubrange(iPos, 1);
        mpNodeInfo->set_slots(slots);

        for (auto pTask : mWorkerTasks[pInMsg->uuid()]) {
            SPD_WARN("WORKER [{}] exit. Moving task [{}:{}] to Job::pending", pInMsg->uuid(), pTask->jobid(),
                     pTask->taskid());
            Job * pJob = mpNodeManager->job(pTask->jobid());
            if (pJob) {
                if (pJob->isTaskInQueue(pTask->taskid(), Salsa::Job::running)) {
                    pJob->moveTask(pTask->taskid(), Salsa::Job::running, Salsa::Job::pending);
                }
                else if (pJob->isTaskInQueue(pTask->taskid(), Salsa::Job::assigned)) {
                    pJob->moveTask(pTask->taskid(), Salsa::Job::assigned, Salsa::Job::pending);
                }

                if (!getenv("SALSA_FAST")) {
                    if (!pJob->consumer().empty()) {
                        std::shared_ptr<Consumer> pConsumer = mpNodeManager->consumer(pJob->consumer());
                        std::vector<std::string>  outData;
                        outData.push_back("JOBRESUBMITED");
                        std::string payload;
                        pTask->SerializeToString(&payload);
                        outData.push_back(payload);
                        mpNodeManager->sendWhisper(pConsumer->pipe().get(), pJob->feeder(), outData);
                    }
                }
            }
        }

        mWorkerTasks[pInMsg->uuid()].clear();
        mWorkerTasks.erase(pInMsg->uuid());
        mClients.erase(pInMsg->uuid());

        if (mpNodeManager->haveMoreTasks()) {
            subscribe(pInMsg->uuid());
        }
        upadateJsonValueNodeInfo();
        mpNodeManager->print();
        SPD_INFO("Workers [{}] slots [{}]", mpNodeInfo->hosts_size(), mpNodeInfo->slots());
    }
}
void Feeder::onWhisper(Message * pInMsg, std::vector<std::string> & out)
{
    ///
    /// onWhisper
    ///

    std::vector<std::string> inContent = pInMsg->content();

    SPD_TRACE("::onWhisper inMSG [{}]", inContent[0]);
    if (inContent[0] == "FREESLOT") { // from consumer
        // Signal arrives from consumer. Indicates that consumer has free exec slot.
        SPD_TRACE("Searching for task in one of jobs");
        TaskInfo * pTask = mpNodeManager->getNextTask();
        // If there's task available, send it out
        if (pTask == nullptr) {
            SPD_TRACE("Sending back NOMORETASKS");
            out.push_back("NOMORETASKS");
            out.push_back(inContent[1]);
        }
        else {
            out.push_back("TASK");
            std::string payload;
            pTask->SerializeToString(&payload);
            out.push_back(payload);
            out.push_back(inContent[1]);
            mWorkerTasks[pInMsg->uuid()].push_back(pTask);
            SPD_TRACE("mWorkerTasks[{}] vector size [{}]", pInMsg->uuid(), mWorkerTasks[pInMsg->uuid()].size());
        }
    }
    else if (inContent[0] == "TASK_IS_RUNNING") { // from consumer
        SPD_TRACE("TASK_IS_RUNNING");

        std::string payload = inContent[1];
        TaskInfo *  pTask   = new TaskInfo();
        {
            if (!pTask->ParseFromString(payload)) {
                SPD_ERROR("Message does not contain ProtoBuf message!");
                for (auto s : inContent) {
                    SPD_ERROR("::onWhisper inMSG [{}]", s);
                }
                return;
            }
        }

        Job * job = mpNodeManager->job(pTask->jobid());
        if (job) {
            if (job->isTaskInQueue(pTask->taskid(), Salsa::Job::assigned)) {
                job->moveTask(pTask->taskid(), Salsa::Job::assigned, Salsa::Job::running);
            }

            if (!job->consumer().empty()) {
                std::shared_ptr<Consumer> pConsumer = mpNodeManager->consumer(job->consumer());
                std::vector<std::string>  output;
                output.push_back(inContent[0]);
                output.push_back(inContent[1]);
                mpNodeManager->sendWhisper(pConsumer->pipe().get(), job->feeder(), output);
            }
            else {
                delete pTask;
            }
        }
        mpNodeManager->print();
    }
    else if (inContent[0] == "TASK_RESULT") { // from consumer
        std::string payload = inContent[1];
        TaskInfo *  pTask   = new TaskInfo();
        {
            if (!pTask->ParseFromString(payload)) {
                SPD_ERROR("Message does not contain ProtoBuf message!");
                for (auto line : inContent) {
                    SPD_ERROR("::onWhisper inMSG [{}]", line);
                }
                return;
            }
        }

        removeWorkerTask(pTask, pInMsg->uuid());

        // task is deleted in next line
        mpNodeManager->resultTask(pTask);
        // TODO possible memleak? @mvala
    }
    else if (inContent[0] == "NODEINFO") { // from consumer
        std::string       payload = inContent[1];
        Salsa::NodeInfo * pNI     = mpNodeInfo->add_hosts();
        if (!pNI->ParseFromString(payload)) {
            SPD_ERROR("[NodeInfo] Message does not contain ProtoBuf message!");
        }
        uint32_t slots = 0;

        for (auto nodeInfo : mpNodeInfo->hosts()) {
            slots += nodeInfo.slots();
        }
        mpNodeInfo->set_slots(slots);
        upadateJsonValueNodeInfo();

        SPD_INFO("Workers [{}] slots [{}]", mpNodeInfo->hosts_size(), mpNodeInfo->slots());
    }
}

void Feeder::subscribe(std::string uuid)
{
    ///
    /// subscribe
    ///

    SPD_INFO("Client [{}] started", uuid);
    SPD_TRACE("Feeders -> [{}]", mClients.size());
    for (auto client : mClients) {
        std::vector<std::string> out;
        out.push_back("SUB");
        mpNodeManager->sendWhisper(pipe().get(), client.first, out);
    }
}

void Feeder::removeWorkerTask(TaskInfo * pTaskInfo)
{
    ///
    /// Remove task from mWorkerTasks (all workers)
    ///

    for (auto wkTask : mWorkerTasks) {
        removeWorkerTask(pTaskInfo, wkTask.first);
    }
}

void Feeder::removeWorkerTask(TaskInfo * pTaskInfo, std::string uuid)
{
    ///
    /// Remove task from mWorkerTasks (wotker with uuid)
    ///
    SPD_TRACE("mWorkerTasks[{}].size() [{}]", uuid, mWorkerTasks[uuid].size());
    if (mWorkerTasks[uuid].size() == 0) return;

    int iPos = 0;
    for (auto pTask : mWorkerTasks[uuid]) {
        if (pTask->taskid() == pTaskInfo->taskid()) {
            mWorkerTasks[uuid].erase(mWorkerTasks[uuid].begin() + iPos);
            return;
        }
        iPos++;
    }
}

void Feeder::terminateJob(std::string uuid)
{
    ///
    /// Terminate job
    ///

    auto job = mpNodeManager->job(uuid);

    std::vector<Salsa::TaskInfo *> tasks;
    job->tasks(tasks, Job::pending, false);
    job->tasks(tasks, Job::assigned, false);
    job->tasks(tasks, Job::running, false);
    for (auto pTask : tasks) {
        SPD_TRACE("removeWorkerTask [{}]", pTask->taskid());
        removeWorkerTask(pTask);
    }

    for (auto client : mClients) {
        std::vector<std::string> out;
        out.push_back("TERMINATEJOB");
        out.push_back(uuid);
        mpNodeManager->sendWhisper(pipe().get(), client.first, out);
    }
    SPD_INFO("JOB [{}] has finished", uuid);
}

} // namespace Salsa
