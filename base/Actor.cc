#include "Actor.hh"
namespace Salsa {

std::sig_atomic_t Actor::msInterrupted = 0;

Actor::Actor() : Object()
{
    ///
    /// Constructor
    ///
}
Actor::~Actor()
{
    ///
    /// Destructor
    ///
}

void Actor::signalHandler(int signalNumber)
{
    ///
    /// Function for handling signals
    ///

    interrupted(signalNumber);
    SPD_TRACE("Interrupted with signal [{}]", msInterrupted);
    SPD_TRACE("Shutting down...");
}

} // namespace Salsa
