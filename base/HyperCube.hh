#pragma once
#include <algorithm>
#include <bitset>
#include <map>
#include <sstream>
#include "Object.hh"

namespace Salsa {
///
/// \class HyperCube
///
/// \brief HyperCube algorithm class
/// \author Andrey Bulatov <andrey.bulatov20@gmail.com>
///

class HyperCube : public Object {
public:
    /// Create HyperCube
    HyperCube(int power = 3, int start = 1);
    virtual ~HyperCube();

    /// Printing Hyper cube paths
    void print() const;

    /// create matrix adjacency
    void createAdjMatrix();
    /// add new node in HC
    void addNode(std::string nodeName);
    /// remove node from HC
    void removeNode(std::string nodeName);
    /// Creat outPut vectors
    void createPaths();
    // TODO WARN - variables _shall not_ begin with underscore!
    // Also, all members should be private (or protected, if so required)!
    std::map<int, std::string> _nodeMap; ///< avalible nodes and their numbers

private:
    int                           mPower;       ///< Power
    int                           mStart;       ///< Starting point
    std::vector<int>              mPassedNodes; ///< Passed nodes
    std::vector<std::vector<int>> mAdjMatrix;   ///< Matrix adjacency
    std::vector<std::vector<int>> mPaths;       ///< Output paths
};
} // namespace Salsa
