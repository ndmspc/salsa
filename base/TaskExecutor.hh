#pragma once

#include "TaskState.hh"

namespace Salsa {
///
/// \class TaskExecutor
///
/// \brief Base TaskExecutor class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///
class TaskExecutor : public Object {
public:
    TaskExecutor();
    virtual ~TaskExecutor();

    /// Run task
    // TODO
    virtual bool run(std::string, std::string) = 0;
    /// Handle pipe
    virtual bool   handlePipe(std::vector<std::string> &) = 0;
    virtual void * pipe() const;

    // TODO annotate task states
    void        taskState(TaskState * pTS);
    TaskState * taskState() const;

protected:
    TaskState * mpTaskState = nullptr; ///< Task state
};
} // namespace Salsa
