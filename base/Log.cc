#include <unistd.h>
#include <fstream>
#include "Log.hh"

namespace Salsa {
uint64_t Log::msID = 0;

Log::Log() : mName(std::to_string(msID++)) {}

Log::~Log() {}

int Log::add(std::string where)
{
    if (where == "console" || where == "") {
        // Console (STD out/err)
        mSinks.push_back(std::make_shared<spdlog::sinks::stdout_color_sink_st>());
    }
    else if (where.find("file://") == 0) {
        // Simple file
        // auto logger = spdlog::basic_logger_mt("mylogger", "log.txt");
        // where.substr(7).c_str()
        std::string p = where.substr(7);
        SPD_TRACE("Testing file [{}] for write ...", p);
        std::ofstream output(p.c_str());
        if (output.is_open()) {
            output.close();
            SPD_TRACE("Testing file [{}] for write is OK ...", p);
            mSinks.push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>(p.c_str(), true));
        }
        else {
            SPD_WARN("Problem creating log file [{}]!!!", p);
            output.close();
        }
    }
    else if (where.find("zmq://") == 0) {
        // TODO implement ZMQ to SPDLog
    }
    else {
        // throw std::runtime_error("Specified sink not found! sink: [" + where + "]");
        return 1;
    }
    return 0;
}

int Log::create()
{
    // Sanity check
    // We could remove this to "reset" the logger, but there may be some risks to this
    if (mpTarget == nullptr) {
        // Create shared logger
        // TODO find a way to drop sink?
        if (mName == "") {
            mName = fmt::format("salsa-runlog-{}", msID);
        }
        mpTarget = std::make_shared<spdlog::logger>(mName.c_str(), mSinks.begin(), mSinks.end());
        // mpTarget->set_pattern("%v[[--ENDL--]]");
        mpTarget->set_pattern("%v");
        // mpTarget->set_formatter(std::make_shared<spdlog::pattern_formatter>(
        //    logFormat, spdlog::pattern_time_type::local, ""));
    }
    return 0;
}

int Log::write(char const * pContent)
{
    // VERY self-explanatory
    mpTarget->info("{}", pContent);
    return 0;
}
} // namespace Salsa
