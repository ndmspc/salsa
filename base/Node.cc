#include <iostream>
#include "Actor.hh"
#include "Node.hh"
namespace Salsa {
Node::Node(std::string newName, std::string uuid)
{
    ///
    /// Constructor
    ///

    mpNodeInfo->set_name(newName);
    mpNodeInfo->set_uuid(uuid);

    SPD_TRACE("Constructing node name [{}] UUID [{}]", mpNodeInfo->name(), mpNodeInfo->uuid());
}

Node::~Node()
{
    ///
    /// Destructor
    ///

    SPD_TRACE("### Destroy Node [{}] ###", mpNodeInfo->name());

    // for (auto p : mPublishers)
    // {
    //     // p->publish("","", ""); // Eh, why?
    // }
    mPublishers.clear();

    delete mpNodeInfo;
}

void Node::json(Json::Value & root)
{
    ///
    /// Returns Node in json format
    ///
    // return;

    // Alright people. This is the time I learned to pay attention the hard way. tl;dr I
    // accidentally purged the whole base folder thinking it was build.
    //
    // And that, kids, is how I made a function meaningless with mere 4 chars...

    for (auto n : mChildNodes) {
        spdlog::get("console")->debug("Node::json() : name={} uuid={}", n->name(), n->uuid());

        // if (!n->uuid().empty()) {
        //     Json::Value jsonnode;
        //     jsonnode["label"] = n->name();
        //     jsonnode["id"]    = n->uuid();
        //     if (n->parent()) jsonnode["net"] = n->parent()->name();
        //     bool found = false;
        //     for (const Json::Value & node : root["nodes"]) {
        //         if (node["label"].asString() == n->name()) found = true;
        //     }
        //     if (!found) root["nodes"].append(jsonnode);

        //     auto p = n->parent();
        //     if (p) {
        //         for (auto pn : p->nodes()) {
        //             if (!n->uuid().compare(pn->uuid())) continue;
        //             Json::Value jsonlink;
        //             jsonlink["source"] = n->uuid();
        //             jsonlink["target"] = pn->uuid();
        //             // jsonlink["sourcename"] = n->name();
        //             // jsonlink["targetname"] = pn->name();
        //             root["links"].append(jsonlink);
        //         }
        //     }
        // }

        n->json(root);
    }

    // if (spdlog::get("console")->level() < 2 && !mpParent)
    //   std::cout << root << std::endl;
}

void Node::print() const
{
    ///
    /// Prints node info
    ///
    std::shared_ptr<Node> pParent = nullptr;

    try {
        pParent = static_cast<std::shared_ptr<Node>>(mpParent);
    }
    catch (std::bad_weak_ptr &) {
    }

    SPD_TRACE("Node::print() : name [{}] nodes [{}] publishers [{}] this [{}] parent [{}]", mpNodeInfo->name(),
              mChildNodes.size(), mPublishers.size(), reinterpret_cast<void const *>(this),
              static_cast<void *>(pParent.get()));

    for (auto const & node : mChildNodes) {
        node->print();
    }
}

std::shared_ptr<Node> Node::find(std::string whatName) const
{
    ///
    /// Returns node by name
    ///

    // We should start using algos...
    // But that is really not necessary for like 10-20 nodes
    for (auto node : mChildNodes) {
        if (name() == whatName) {
            return node;
        }
    }

    return nullptr;
}

void Node::removeByUUID(std::string whatUUID)
{
    ///
    /// Removes node by uuid
    ///

    // I'm sure there's a more elegant way
    // I don't have internet rn tho, so
    // TODO
    int iNode = 0;
    for (auto node : mChildNodes) {
        if (whatUUID == node->uuid()) {
            // delete n; // Unnecessary. Smart pointers FTW
            // Also, one very important concept: Delete only what you create
            mChildNodes.erase(mChildNodes.begin() + iNode);
        }
        iNode++;
    }
}

void Node::publish()
{
    ///
    /// Publish network status
    ///

    // Get to highest node in stack?
    // TODO @mvala explanation?

    // We ignore it for now
    return;

    // Node * pNode = this;
    // while (true) {
    //     try {

    //         std::shared_ptr<Node> pParent = //
    //             static_cast<std::shared_ptr<Node>>(pNode->parent());

    //         pNode = pParent.get();
    //     }
    //     catch (std::bad_weak_ptr &) {
    //         break;
    //     }
    // }

    // if (pNode->publishers().empty()) {
    //     SPD_TRACE("Node::publish() No publisher defined! Aborting publish()");
    //     return;
    // }

    // SPD_TRACE("Node::publish() Publishing from [{}] nPublishers [{}]", pNode->name(), mPublishers.size());

    // Json::Value jsonData;
    // pNode->json(jsonData);
    // Json::StreamWriterBuilder builder;
    // builder.settings_["indentation"] = "";

    // std::string outStr = Json::writeString(builder, jsonData);

    // for (auto const & pub : pNode->publishers()) {
    //     SPD_TRACE("Node::publish() name [{}] data [{}]", pNode->name(), outStr);
    //     pub->publish(pNode->name(), outStr);
    // }
}

} // namespace Salsa
