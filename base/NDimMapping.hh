#pragma once
#include <map>
#include "Object.hh"

namespace Salsa {
///
/// \class NDimMapping
///
/// \brief NDimMapping algorithm class
/// \author Yuri Butenko <gohas94@gmail.com>
///

class NDimMapping : public Object {
public:
    NDimMapping(int nBin = 8, int lBorder = -4, int rBorder = 3);
    virtual ~NDimMapping();

    /// Printing
    void print() const;

private:
    int                                        mNBin;    ///< number of bin's
    int                                        mLBorder; ///< left border
    int                                        mRBorder; ///< right border
    std::map<std::pair<int, std::string>, int> mBinMap;  ///< bin mapping
};
} // namespace Salsa
