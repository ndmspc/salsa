#pragma once
#include "Object.hh"

namespace Salsa {
///
/// \class Poller
///
/// \brief Base Poller class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Poller : public Object {
public:
    Poller();
    virtual ~Poller();

    /// Waiting for socket
    virtual void * wait(int timeout = -1) = 0;
};
} // namespace Salsa
