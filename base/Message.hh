#pragma once

#include "Object.hh"

namespace Salsa {
///
/// \class Message
///
/// \brief Base Message class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Message : public Object {
public:
    /// Node event type
    enum EEventType { UNKNOWN = 0, ENTER, EXIT, EVASIVE, WHISPER };

    Message();
    virtual ~Message();

    /// Printing info
    virtual void print() const = 0;

    /// Returns node event type
    virtual EEventType event() const = 0;

    /// Returns node uuid
    virtual std::string uuid() const = 0;

    /// Returns node uuid
    virtual std::string name() const = 0;

    /// Retursn vector of partial messages as strings
    virtual std::vector<std::string> & content() = 0;
};
} // namespace Salsa
