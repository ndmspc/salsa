#include "Consumer.hh"
#include "NodeManager.hh"
namespace Salsa {
Consumer::Consumer(std::string uuid, std::shared_ptr<Socket> pPipe, NodeManager * pNM) : Distributor(uuid, pPipe, pNM)
{
    ///
    /// Constructor
    ///
}
Consumer::~Consumer()
{
    ///
    /// Destructor
    ///
}

void Consumer::onEnter(Message * pInMsg, std::vector<std::string> & out, std::string /*type*/)
{
    ///
    /// onEnter
    ///

    std::vector<std::string> & content = pInMsg->content();
    for (auto data : content) {
        SPD_TRACE("::onEnter IN [{}]", data.c_str());
    }

    out.push_back("AUTHOK");
    SPD_TRACE("AUTHOK");
}

void Consumer::onExit(Message * pInMsg, std::vector<std::string> & /*out*/)
{
    ///
    /// onExit
    ///
    for (auto data : pInMsg->content()) {
        SPD_TRACE("::onExit IN [{}]", data.c_str());
    }

    SPD_TRACE("Handling EXIT from [{}]", pInMsg->uuid());
    std::vector<std::string> jobs;
    mpNodeManager->jobs(pInMsg->uuid(), jobs);
    for (auto job : jobs) {
        SPD_TRACE("Terminating job [{}] from upstream [{}]", job, pInMsg->uuid());
        mpNodeManager->terminateJob(job);
    }
}
void Consumer::onWhisper(Message * pInMsg, std::vector<std::string> & out)
{
    ///
    /// onWhisper
    ///

    std::vector<std::string> & content = pInMsg->content();
    for (auto data : content) {
        SPD_TRACE("::onWhisper IN [{}]", data.c_str());
    }

    if (content[0] == "START") {
        int32_t nTasks = 1;
        nTasks         = mpNodeManager->nSlots(1.5);
        if (getenv("SALSA_FAKE")) {
            nTasks *= 10;
        }
        if (nTasks != 0) {
            out.push_back("SENDTASKS");
            out.push_back(fmt::format("{}", nTasks));
            SPD_TRACE("SENDTASKS");
        }
    }
    else if (content[0] == "TASK") {
        SPD_TRACE("TASK");
        TaskInfo * ti = new TaskInfo();
        {
            if (!ti->ParseFromString(content[1].c_str())) {
                SPD_ERROR("Message does not contain ProtoBuf message!");
                return;
            }
        }
        // TODO : now we need to tell all feeders that that they should subscribe to workers
        // (probably in addTask when creating new Job)
        mpNodeManager->addTask(ti, mUUID, pInMsg->uuid());
    }
    else if (content[0] == "NOMORETASKS") {
        mpNodeManager->noMoreTasks(content[1]);
    }
    else {
        out.push_back("START");
        SPD_TRACE("START");
    }
}
} // namespace Salsa
