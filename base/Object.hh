#pragma once

#include "salsa.hh"

namespace Salsa {
///
/// \class Object
///
/// \brief Base Salsa Object class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Object {
public:
    Object();
    virtual ~Object();

    /// Get console output
    static std::shared_ptr<spdlog::logger> getConsoleOutput()
    {
        // This method is inline, so compiler gets hinted very strongly to inline it.
        // Aaaand it does not. (Clang 8.0.0 makes binary with method... So sad...)
        return mspConsoleLogger;
    }

    /// Sets console log level
    static void setConsoleLevel(spdlog::level::level_enum level) { mspConsoleLogger->set_level(level); }

private:
    static std::shared_ptr<spdlog::logger> mspConsoleLogger; ///< Pointer to spd logger
};
} // namespace Salsa
