#include <bitset>
#include <math.h>
#include <string>
#include "NDimMapping.hh"

namespace Salsa {
NDimMapping::NDimMapping(int nBin, int lBorder, int rBorder)
    : Object(), mNBin(nBin), mLBorder(lBorder), mRBorder(rBorder)
{
    /// Constructor

    SPD_INFO("disregard - {} {}", mLBorder, mRBorder); // Clang actually marks these two as unused...

    const int p    = ceil(log2(mNBin));
    int       size = pow(2, p);
    for (auto i = 1; i <= size; i++) {
        std::bitset<64> b(i - 1);
        std::string     bits = b.to_string<char, std::char_traits<char>, std::allocator<char>>();
        bits.erase(0, 64 - p);
        mBinMap.insert(std::make_pair(std::make_pair(i, bits), i));
    }
}
NDimMapping::~NDimMapping()
{
    /// Destructor
}

void NDimMapping::print() const
{

    SPD_INFO("nBin [{}] pow [{}]", mNBin, ceil(log2(mNBin)));
    for (auto it = mBinMap.begin(); it != mBinMap.end(); ++it) SPD_INFO("[{}] [{}]", it->first.first, it->first.second);
}
} // namespace Salsa
