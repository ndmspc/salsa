#include "Distributor.hh"
namespace Salsa {
Distributor::Distributor(std::string uuid, std::shared_ptr<Socket> pipe, NodeManager * pNodeMan)
    : Object(), mUUID(uuid), mpPipe(pipe), mpNodeManager(pNodeMan)
{
    ///
    /// Constructor
    ///
}
Distributor::~Distributor()
{
    ///
    /// Destructor
    ///
    delete mpNodeInfo;
}

void Distributor::addClient(std::string uuid, std::string type)
{
    ///
    /// Adds client
    ///
    mClients.emplace(uuid, type);
}

void Distributor::removeClient(std::string uuid)
{
    ///
    /// Remove client
    ///
    mClients.erase(uuid);
}

void Distributor::addOther(std::string uuid, std::string type)
{
    ///
    /// Adds other
    ///
    mOthers.emplace(uuid, type);
}

void Distributor::removeOther(std::string uuid)
{
    ///
    /// Remove other
    ///
    mOthers.erase(uuid);
}

void Distributor::print() const
{
    ///
    /// Prints info
    ///
    SPD_DEBUG("clients [{}] others [{}] pipe [{}]", mClients.size(), mOthers.size(), static_cast<void *>(mpPipe.get()));
}

void Distributor::onEnter(Message * /*inMsg*/, std::vector<std::string> & /*out*/, std::string /*type*/)
{
    ///
    /// onEnter
    ///
}
void Distributor::onExit(Message * /*inMsg*/, std::vector<std::string> & /*out*/)
{
    ///
    /// onExit
    ///
}
void Distributor::onWhisper(Message * /*inMsg*/, std::vector<std::string> & /*out*/)
{
    ///
    /// onWhisper
    ///
}

std::shared_ptr<Socket> Distributor::pipe() const
{
    ///
    /// Returns pipe socket
    ///
    return mpPipe;
}
std::string Distributor::uuid() const
{
    ///
    /// Returns uuid
    ///
    return std::move(mUUID);
}

NodeInfo * Distributor::nodeInfo() const
{
    ///
    /// Returns node info
    ///
    return mpNodeInfo;
}

void Distributor::upadateJsonValueNodeInfo()
{
    ///
    /// Updates json value with node info
    ///

    if (mpNodeInfo == nullptr) return;

    mJsonValue.clear();
    mJsonValue["uuid"] = mpNodeInfo->uuid();
    if (!mpNodeInfo->name().empty()) mJsonValue["name"] = mpNodeInfo->name();
    if (!mpNodeInfo->name().empty()) mJsonValue["hostname"] = mpNodeInfo->hostname();
    if (!mpNodeInfo->submiturl().empty()) mJsonValue["submitUrl"] = mpNodeInfo->submiturl();
    mJsonValue["slots"] = mpNodeInfo->slots();
    if (mpNodeInfo->hosts_size() > 0) {
        for (auto hh : mpNodeInfo->hosts()) {
            Json::Value h;
            h["uuid"]     = hh.uuid();
            h["hostname"] = hh.hostname();
            h["slots"]    = hh.slots();
            mJsonValue["hosts"].append(h);
        }
    }
}

} // namespace Salsa
