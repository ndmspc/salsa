#pragma once

#include "Object.hh"
#include "TaskInfo.pb.h"

namespace Salsa {
///
/// \class TaskState
///
/// \brief Base salsa TaskState class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///
class TaskExecutor;
class TaskState : public Object {
public:
    /// Status of task
    enum EState { idle, assigned, running, killed, all };

    TaskState(TaskExecutor * te = nullptr);
    virtual ~TaskState();

    void     id(uint32_t id);
    uint32_t id() const;

    void   state(EState s);
    EState state() const;

    TaskInfo * task() const;
    void       task(TaskInfo * t);

    void print(bool verbose = false) const;

    void     pid(uint32_t pid);
    uint32_t pid() const;

    void           killTask();
    TaskExecutor * executor();

protected:
    uint32_t       mId            = 0;       ///< ID of task state
    EState         mState         = idle;    ///< Status of actor
    TaskInfo *     mpTask         = nullptr; ///< TaskInfo held by said actor
    uint32_t       mPID           = 0;       ///< Task PID
    TaskExecutor * mpTaskExecutor = nullptr; ///< Task Executor
};

} // namespace Salsa
