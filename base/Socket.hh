#pragma once

#include "Message.hh"

namespace Salsa {
///
/// \class Socket
///
/// \brief Base Socket class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Socket : public Object {
public:
    Socket();
    virtual ~Socket();

    /// Connect function
    virtual int connect() = 0;

    /// Disconnect function
    virtual int disconnect() = 0;

    /// Pull message
    virtual Message * pull() = 0;

    /// Push message
    virtual int push(Message *) = 0;
};
} // namespace Salsa
