#pragma once
#include "Object.hh"

namespace Salsa {
///
/// \class Publisher
///
/// \brief Base Publisher class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Publisher : public Object {
public:
    Publisher(std::string url = "");
    virtual ~Publisher();

    /// Publish TODO publish what?
    virtual void publish(std::string id, std::string name, std::string data, bool force = true) = 0;

    /// Returns url
    std::string url() const { return mURL; }

protected:
    std::string mURL; ///< Url to publish
};
} // namespace Salsa
