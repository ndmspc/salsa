#pragma once
#include <map>
#include <json/json.h>

#include "NodeInfo.pb.h"
#include "TaskInfo.pb.h"
#include "Socket.hh"
namespace Salsa {
///
/// \class Distributor
///
/// \brief Base Distributor class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///
class NodeManager;

class Distributor : public Object {
public:
    Distributor(std::string uuid, std::shared_ptr<Socket> pPipe, NodeManager * pNM);
    virtual ~Distributor();

    /// Returns distributor's UUID
    std::string uuid() const;

    /// TODO Returns distributor's pipe?
    std::shared_ptr<Socket> pipe() const;

    /// TODO Prints distributor's state
    void print() const;

    // TODO Client ops
    void addClient(std::string uuid, std::string type);
    void removeClient(std::string uuid);

    // TODO Other ops
    void addOther(std::string uuid, std::string type);
    void removeOther(std::string uuid);

    // TODO Get node info
    NodeInfo * nodeInfo() const;

    /// TODO Three horsemen of apocalypse
    virtual void onEnter(Message * pInMsg, std::vector<std::string> & out, std::string type);
    virtual void onExit(Message * pInMsg, std::vector<std::string> & out);
    virtual void onWhisper(Message * pInMsg, std::vector<std::string> & out);

    /// Returns json value
    Json::Value  jsonValueNodeInfo() const { return mJsonValue; }
    virtual void upadateJsonValueNodeInfo();

protected:
    std::string                        mUUID{};                    ///< Self UUID
    std::shared_ptr<Socket>            mpPipe = nullptr;           ///< Pipe for messages (net connector)
    std::map<std::string, std::string> mClients{};                 ///< List of clients
    std::map<std::string, std::string> mOthers{};                  ///< List of others
    NodeManager *                      mpNodeManager = nullptr;    ///< Node Manager
    NodeInfo *                         mpNodeInfo{new NodeInfo()}; ///< Node Info
    Json::Value                        mJsonValue;                 ///< Node Info as json value

    mutable TaskInfo mTaskInfoCache{}; ///< Task Info cache
};
} // namespace Salsa
