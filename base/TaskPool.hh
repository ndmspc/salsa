#pragma once

#include "TaskState.hh"

namespace Salsa {
///
/// \class TaskPool
///
/// \brief Base salsa TaskPool class
/// \author Matej Fedor <matej.fedor.mf@gmail.com>
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Job;
class NodeManager;

class TaskPool : public Object {
public:
    TaskPool(NodeManager * pNM);
    virtual ~TaskPool();

    // TODO __annotate__
    void        add(void * p, TaskState * t);
    TaskState * find(void * p) const;
    TaskState * findById(uint32_t id) const;
    TaskState * findFreeTask() const;

    void     changeState(uint32_t id, TaskState::EState state);
    uint32_t nSlotFree();

    bool terminateJob(Job * pJob);
    bool handlePipe(void * pPipe);
    void print(bool verbose = false) const;

protected:
    std::map<void *, TaskState *> mTasks{};                ///< List of task slots
    NodeManager *                 mpNodeManager = nullptr; ///< Node manager
};

} // namespace Salsa
