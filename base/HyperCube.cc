#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
// Next time please sanitize your damned code.

#include "HyperCube.hh"

namespace Salsa {

int nVertex; // number vertex

HyperCube::HyperCube(int power, int start) : Object(), mPower(power), mStart(start)
{
    nVertex = pow(2, power);
}

HyperCube::~HyperCube()
{
    ///
    /// Destructor
    ///
}

void HyperCube::createAdjMatrix()
{
    std::bitset<64>          b;
    std::vector<std::string> bitsVector(nVertex);

    for (int i = 0; i < nVertex; i++) {
        std::bitset<64> b(i);
        std::string     bits = b.to_string<char, std::char_traits<char>, std::allocator<char>>();
        bits.erase(0, 64 - (mPower + 1));

        bitsVector[i] = bits;
    }

    mAdjMatrix.resize(nVertex);

    for (std::size_t i = 0; i < bitsVector.size(); i++) {
        mAdjMatrix[i].resize(nVertex);
        for (std::size_t j = 0; j < bitsVector.size(); j++) {
            int diff = 0;
            for (std::size_t k = 0; k < bitsVector[i].length(); k++) {
                if (bitsVector[i][k] != bitsVector[j][k]) diff++;
            }
            if (diff == 1) mAdjMatrix[i][j] = 1;
        }
    }
}

void HyperCube::addNode(std::string nodeName)
{
    auto it = _nodeMap.begin();

    while (it != _nodeMap.end()) {
        if (it->second == nodeName) {
            SPD_INFO("a node with that name has already been added");
            return;
        }

        it++;
    }

    int i = _nodeMap.size() + 1;
    _nodeMap.insert(std::pair<int, std::string>(i, nodeName));
}

void HyperCube::removeNode(std::string nodeName)
{
    auto it = _nodeMap.begin();

    while (it != _nodeMap.end()) {
        if (it->second == nodeName) {
            _nodeMap.erase(it);

            std::map<int, std::string> newMap;
            int                        i = 1;

            for (std::map<int, std::string>::iterator mi = _nodeMap.begin(); mi != _nodeMap.end(); ++mi) {
                newMap[i] = mi->second;
                i++;
            }
            _nodeMap = newMap;
            it--;
        }

        it++;
    }
}

void HyperCube::createPaths()
{
    bool flag = false;

    std::vector<int> initVector = {mStart};
    mPaths.push_back(initVector);
    mPassedNodes.push_back(mStart);

    do {
        std::vector<int> tmpVector;
        for (std::size_t i = 0; i < mPaths[mPaths.size() - 1].size(); i++) {

            std::size_t pos = mPaths[mPaths.size() - 1][i] - 1;
            for (std::size_t j = 0; j < mAdjMatrix[pos].size(); j++) {

                bool passed = std::find(mPassedNodes.begin(), mPassedNodes.end(), j + 1) != mPassedNodes.end();

                if (mAdjMatrix[pos][j] == 1 && !passed) {
                    tmpVector.push_back(j + 1);
                    mPassedNodes.push_back(j + 1);
                }
            }
        }

        if (tmpVector.size() > 0) {
            mPaths.push_back(tmpVector);
            flag = true;
        }
        else
            flag = false;

    } while (flag);
}

void HyperCube::print() const
{
    for (std::size_t i = 0; i < mAdjMatrix.size(); ++i) {
        std::string s;
        for (std::size_t j = 0; j < mAdjMatrix[i].size(); ++j) {
            s.append(std::to_string(mAdjMatrix[i][j]));
        }
        SPD_INFO("{}", s);
    }

    for (std::size_t i = 0; i < mPaths.size(); ++i) {
        std::string s;
        for (std::size_t j = 0; j < mPaths[i].size(); ++j) {
            if (_nodeMap.find(mPaths[i][j]) != _nodeMap.end()) {
                auto position = _nodeMap.find(mPaths[i][j]);

                s.append(std::to_string(mPaths[i][j]) + "(" + position->second + ") ");
            }
            else
                s.append(std::to_string(mPaths[i][j]) + " ");
        }
        SPD_INFO("{}", s);
    }
}
} // namespace Salsa

#pragma clang diagnostic pop
