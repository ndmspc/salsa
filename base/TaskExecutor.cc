#include "TaskExecutor.hh"
namespace Salsa {
TaskExecutor::TaskExecutor() : Object()
{
    ///
    /// Constructor
    ///
}
TaskExecutor::~TaskExecutor()
{
    ///
    /// Destructor
    ///
}
void * TaskExecutor::pipe() const
{
    ///
    /// Returns pointer to pipe
    ///
    return nullptr;
}
void TaskExecutor::taskState(TaskState * pTaskState)
{

    ///
    /// Sets task state
    ///
    mpTaskState = pTaskState;
}
TaskState * TaskExecutor::taskState() const
{
    ///
    /// Returns task state
    ///
    return mpTaskState;
}
} // namespace Salsa
