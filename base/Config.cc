#include <iostream>
#include "Config.hh"
namespace Salsa {
Config::Config() : Object()
{
    ///
    /// Constructor
    ///
}
Config::~Config()
{
    ///
    /// Destructor
    ///
}

bool Config::load(std::string file)
{
    ///
    /// Load config file
    ///

    mConfig = YAML::LoadFile(file);
    return true;
}

void Config::filter(std::string const & filter)
{
    ///
    /// Set Filter List
    ///
    std::stringstream filterSS{filter};
    std::string       netName;

    while (std::getline(filterSS, netName, '+')) {
        if (!netName.empty()) {

            size_t      pos  = netName.find('{');      // Find semicolon, if any
            std::string name = netName.substr(0, pos); // Substring name
            std::string opt;
            if (pos != std::string::npos) {
                opt = netName.substr(pos, netName.size()); // Substring opt

                // replase ":" with ": "
                findAndReplaceAll(opt, ":", ": ");
            }

            if (opt.empty()) {
                YAML::Node n;
                mFilter.insert(std::pair<std::string, YAML::Node>(std::move(name), n));
            }
            else {
                mFilter.insert(std::pair<std::string, YAML::Node>(std::move(name), YAML::Load(opt)));
            }

            // mFilter.push_back(std::move(s));
        }
    }
}

void Config::print() const
{
    ///
    /// Prints config file
    ///
    std::cout << mConfig << std::endl;
}

void Config::findAndReplaceAll(std::string & data, std::string toSearch, std::string replaceStr)
{
    ///
    /// Replase all
    ///

    // Get the first occurrence
    size_t pos = data.find(toSearch);

    // Repeat till end is reached
    while (pos != std::string::npos) {
        // Replace this occurrence of Sub String
        data.replace(pos, toSearch.size(), replaceStr);
        // Get the next occurrence from the current position
        pos = data.find(toSearch, pos + replaceStr.size());
    }
}

} // namespace Salsa
