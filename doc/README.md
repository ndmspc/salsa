# SALSA development setup via Fedora toolbox

## Init

```bash
[user@localhost ~]$ toolbox create -c sls
Created container: sls
Enter with: toolbox enter --container sls
```

## Installing development packages for SALSA

Entering salsa toolbox

```bash
[user@localhost ~]$ toolbox enter -c sls # Entering toolbox
⬢[user@toolbox ~]$ su - # Change to super user
⬢[root@toolbox ansible] dnf install -y -q ansible # Install ansible
```

Clone obl/site project

```bash
⬢[root@toolbox ~]# git clone  https://gitlab.openbrain.sk/obl/site.git # Clone repo
⬢[root@toolbox ~]# cd site/ansible/ # Enter site/ansible directory

```

Install depencency for ansible

```bash
⬢[root@toolbox ansible]# ansible-galaxy install -r requirements.yml
```

Run `ansible-playbook` to install all salsa-development packages

```bash
⬢[root@toolbox ansible]# ansible-playbook -e ansible_python_interpreter=/usr/bin/python3 main.yml --tags="salsa-dev"
```

Play recap should look similar to this

```bash
PLAY RECAP ********************************************************************************************************************************************************************************************************************************************************************************************************************
localhost                  : ok=14   changed=8    unreachable=0    failed=0    skipped=10   rescued=0    ignored=0
```

Installation is done. One can exit toolbox via `Ctrl-D` (Two times. One from `root` and one from toolbox itself)

```bash
⬢[root@toolbox ~]# logout
⬢[user@toolbox ~]$ logout
[user@localhost ~]$
```

## Entering toolbox and compliling SALSA

```bash
[user@localhost ~]$ toolbox enter -c sls # Entering SALSA toolbox
⬢[user@toolbox ~]$ git clone https://gitlab.openbrain.sk/salsa/salsa.git # Cloning SALSA repo
⬢[user@toolbox ~]$ cd salsa/ # Entering to SALSA directory
⬢[user@toolbox salsa]$ scripts/make.sh # Building project
```

Starting salsa cluster

```bash
⬢[user@toolbox salsa]$ build/src/salsa
[2019-07-26 11:24:00.399] [salsa] [info] Starting salsa v0.3.0-0.1 ...
[2019-07-26 11:24:00.399] [salsa] [info] Using config file [/home/user/git/openbrain.sk/salsa/salsa/etc/default.yaml] ...
[2019-07-26 11:24:00.412] [salsa] [info] Submit : url [tcp://*:41000]
[2019-07-26 11:24:00.414] [salsa] [info] Node : type [rdr] name [rdr:toolbox:13813:0] discovery type [udp] url [] port [40000] endpoint []
I: 19-07-26 11:24:00 zyre_node: dump state
I: 19-07-26 11:24:00  - name=rdr:toolbox:13813:0 uuid=2ED74E870BBD4E698C23CCA3CDFBE97B
I: 19-07-26 11:24:00  - endpoint=tcp://158.197.44.188:49152
I: 19-07-26 11:24:00  - discovery=beacon port=40000 interval=0
I: 19-07-26 11:24:00  - headers=1:
I: 19-07-26 11:24:00    - X-SALSA-NODE-TYPE: FEEDER
I: 19-07-26 11:24:00  - peers=0:
I: 19-07-26 11:24:00  - own groups=0:
I: 19-07-26 11:24:00  - peer groups=0:
[2019-07-26 11:24:00.417] [salsa] [info] Node : type [wk] name [wk:toolbox:13813:2] discovery type [udp] url [] port [40000] endpoint []
I: 19-07-26 11:24:00 zyre_node: dump state
I: 19-07-26 11:24:00  - name=wk:toolbox:13813:2 uuid=8984DB62596B40ED85337C04327863CD
I: 19-07-26 11:24:00  - endpoint=tcp://158.197.44.188:49153
I: 19-07-26 11:24:00  - discovery=beacon port=40000 interval=0
I: 19-07-26 11:24:00  - headers=1:
I: 19-07-26 11:24:00    - X-SALSA-NODE-TYPE: WORKER
I: 19-07-26 11:24:00  - peers=0:
I: 19-07-26 11:24:00  - own groups=0:
I: 19-07-26 11:24:00  - peer groups=0:
[2019-07-26 11:24:00.417] [salsa] [info] WORKER [8984DB62596B40ED85337C04327863CD] has 4 cores
[2019-07-26 11:24:00.418] [salsa] [info] WORKER [8984DB62596B40ED85337C04327863CD] <= [2ED74E870BBD4E698C23CCA3CDFBE97B] [FEEDER]
[2019-07-26 11:24:00.418] [salsa] [info] FEEDER [2ED74E870BBD4E698C23CCA3CDFBE97B] <= [8984DB62596B40ED85337C04327863CD] [WORKER]
[2019-07-26 11:24:00.418] [salsa] [info] Workers [1] slots [4]
```
